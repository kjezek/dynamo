import java.util.*;
// note that wrap uses a covariant return type
public class WrapperGrandchild extends WrapperChild {
    @Override public List wrap(Object o) {
        return (List) (super.wrap(o));
    }
}