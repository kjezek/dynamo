This case study shows how the use of the dynamo compiler can avid the issue described here:
http://www.artima.com/weblogs/viewpost.jsp?thread=354443


Prerequisites:
-----------------------------

* Must use Java 1.7
  -> Java < 1.7 does not support invokedynamic
  -> dynamo-compiler.jar and dynamo-rt.jar
  -> ant must be installed in in the PATH, verify this with ANT

Test with ant build script:
----------------------------
1. ant
   -> runs everything, see

2. ant compile
   -> compiles everything, incl two versions of the code in src1-v1 / src-v2 ,
      and compiles sources from src2 with javac into bin2-classic and with dynamo into bin2-dynamo

3. ant run-classic-compiled
   -> compiles everything
   -> then runs the client app compiled with classic javac against the upgraded code -
      this fails with a StackOverflowError as described in
      http://www.artima.com/weblogs/viewpost.jsp?thread=354443

4. ant run-dynamo-compiled
    -> compiles everything
    -> then runs the client app compiled with dynamo against the upgraded code
       this should now succeed
