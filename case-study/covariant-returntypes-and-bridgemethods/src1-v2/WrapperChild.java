import java.util.*;
// in this version, the return type of wrapper is changed from Object to List
public class WrapperChild extends Wrapper {
    @Override public List wrap(Object o) {
        return (List)super.wrap(o);
    }
}