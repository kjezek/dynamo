package org.dynamo.casestudy.jasper;

/*
 * JasperReports - Free Java Reporting Library.
 * Copyright (C) 2001 - 2014 TIBCO Software Inc. All rights reserved.
 * http://www.jaspersoft.com
 *
 * Unless you have purchased a commercial license agreement from Jaspersoft,
 * the following license terms apply:
 *
 * This program is part of JasperReports.
 *
 * JasperReports is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JasperReports is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JasperReports. If not, see <http://www.gnu.org/licenses/>.
 */

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.data.JRMapArrayDataSource;
import net.sf.jasperreports.view.JasperViewer;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class JFreeChartApp
{

    public static JasperViewer jasperViewer;

    /**
     *
     */
    public static void main(String[] args) throws JRException {

        File dir = new File("build");
        dir.mkdir();
        new File(dir, "reports").mkdir();

        JasperCompileManager.compileReportToFile("src/reports/timeseries.jrxml", "build/reports/timeseries.jasper");
        JasperFillManager.fillReportToFile("build/reports/timeseries.jasper", null, createData());
//        JasperExportManager.exportReportToPdfFile("out/reports/days.jrprint");
//        JasperViewer.viewReport("build/reports/timeseries.jrprint", false);
        jasperViewer = new JasperViewer("build/reports/timeseries.jrprint", false, true);
        jasperViewer.setVisible(true);
    }


    private static JRDataSource createData() {
        int size = 10;

        Object[] data = new Object[size];

        String title = "Day by day";
        final TimeSeries series = new TimeSeries(title);
        Day current = new Day(8,2,1983);

        for (int i = 0; i < size; i++) {

            series.add(current, new Double(i));
            current = (Day) current.next();

            Map<String, Object> row = new HashMap<String, Object>();
            row.put("label", "Label");
            row.put("data", new Date());
            row.put("value", i);

            data[i] = row;
        }

        return new JRMapArrayDataSource(data);
    }

}
