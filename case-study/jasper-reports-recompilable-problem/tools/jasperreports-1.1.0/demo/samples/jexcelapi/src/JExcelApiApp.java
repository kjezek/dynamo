/*
 * ============================================================================
 * GNU Lesser General Public License
 * ============================================================================
 *
 * JasperReports - Free Java report-generating library.
 * Copyright (C) 2001-2005 JasperSoft Corporation http://www.jaspersoft.com
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 * 
 * JasperSoft Corporation
 * 185, Berry Street, Suite 6200
 * San Francisco CA 94107
 * http://www.jaspersoft.com
 */
import java.io.File;

import java.util.Map;
import java.util.HashMap;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JExcelApiExporterParameter;

/**
 * @author Teodor Danciu (teodord@users.sourceforge.net)
 * @version $Id: JExcelApiApp.java,v 1.1 2005/10/19 08:45:00 teodord Exp $
 */
public class JExcelApiApp {
	
	/**
	 * 
	 */
	private static final String TASK_FILL = "fill";
	private static final String TASK_XLS = "xls";
	private static final String TASK_PDF = "pdf";
	
	private static final String[] reportNames = {
		"AreaChartReport",
		"FontsReport",
		"HyperlinkReport",
		"ImagesReport",
		"QueryReport",
		"ShapesReport",
		"StyledTextReport",
		"UnicodeReport"
	};
	
	/**
	 * 
	 */
	public static void main(String[] args) {
		String taskName = null;
		
		if(args.length == 0) {
			usage();
			return;
		}
		
		int k = 0;
		while (args.length > k) {
			if (args[k].startsWith("-T")) {
				taskName = args[k].substring(2);
			}			
			k++;
		}
		
		
		try {
			if(TASK_FILL.equals(taskName)) {
				
				long start = System.currentTimeMillis();
			
				Map chartParameters = new HashMap();
				// charts sample 
				chartParameters.put("MaxOrderID", new Integer(12500));
				JasperFillManager.fillReportToFile("./build/reports/AreaChartReport.jasper", chartParameters, getConnection());
				System.err.println("AreaChartReport filling time : " + (System.currentTimeMillis() - start));
				
				
				// fonts sample
				start = System.currentTimeMillis();
				JasperFillManager.fillReportToFile("./build/reports/FontsReport.jasper", null, new JREmptyDataSource());
				System.err.println("FontsReport filling time : " + (System.currentTimeMillis() - start));
				
				// hyperlink sample
				start = System.currentTimeMillis();
				JasperFillManager.fillReportToFile("./build/reports/HyperlinkReport.jasper", null, new JREmptyDataSource());
				System.err.println("HyperlinkReport filling time : " + (System.currentTimeMillis() - start));
				
				// images sample
				start = System.currentTimeMillis();
				JasperFillManager.fillReportToFile("./build/reports/ImagesReport.jasper", null, new JREmptyDataSource());
				System.err.println("ImagesReport filling time : " + (System.currentTimeMillis() - start));
				
				// query sample
				start = System.currentTimeMillis();
				Map queryParameters = new HashMap();
				queryParameters.put("ReportTitle", "Address Report");
				queryParameters.put("FilterClause", "'Boston', 'Chicago', 'Oslo'");
				queryParameters.put("OrderClause", "City");
				JasperFillManager.fillReportToFile("./build/reports/QueryReport.jasper", queryParameters, getConnection());
				System.err.println("QueryReport filling time : " + (System.currentTimeMillis() - start));
				
				// shapes sample
				start = System.currentTimeMillis();
				JasperFillManager.fillReportToFile("./build/reports/ShapesReport.jasper", null, (JRDataSource)null);
				System.err.println("ShapesReport filling time : " + (System.currentTimeMillis() - start));
				
				// styled text sample
				start = System.currentTimeMillis();
				JasperFillManager.fillReportToFile("./build/reports/StyledTextReport.jasper", null, new JREmptyDataSource());
				System.err.println("StyledTextReport filling time : " + (System.currentTimeMillis() - start));
				
				// unicode sample
				start = System.currentTimeMillis();
				Map unicodeParameters = new HashMap();
				unicodeParameters.put("GreekText", "\u0393 \u0394 \u0398 \u039B \u039E \u03A0 \u03A3 \u03A6 \u03A8 \u03A9");
				unicodeParameters.put("CyrillicText", "\u0402 \u040B \u040F \u0414 \u0416 \u0418 \u041B \u0426 \u0429 \u042E");
				unicodeParameters.put("ArabicText", "\u0647\u0630\u0627 \u0639\u0631\u0636 \u0644\u0645\u062C\u0645\u0648\u0639\u0629 TextLayout");
				unicodeParameters.put("HebrewText", "\u05D0\u05E0\u05D9 \u05DC\u05D0 \u05DE\u05D1\u05D9\u05DF \u05E2\u05D1\u05E8\u05D9\u05EA");

				JasperFillManager.fillReportToFile("./build/reports/UnicodeReport.jasper", unicodeParameters, new JREmptyDataSource());
				System.err.println("UnicodeReport filling time : " + (System.currentTimeMillis() - start));
				
				System.exit(0);
			}
			if (TASK_XLS.equals(taskName)) {
				for(int i = 0; i < reportNames.length; i++) {
					long start = System.currentTimeMillis();
					File sourceFile = new File("./build/reports/" +reportNames[i] + ".jrprint");
					File destFile = new File("./build/reports/" + reportNames[i] + ".xls");
					
					JasperPrint jasperPrint = (JasperPrint)JRLoader.loadObject(sourceFile);
					
					JExcelApiExporter exporter = new JExcelApiExporter();	
					
					exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, destFile.toString());
					exporter.setParameter(JExcelApiExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
					
					exporter.exportReport();
					
					sourceFile = null;
					destFile = null;
					
					System.err.println("Report : " + reportNames[i] + ".XLS export time : " + (System.currentTimeMillis() - start));
				}
				
				System.exit(0);
			}
			else if (TASK_PDF.equals(taskName))
			{
				for(int i = 0; i < reportNames.length; i++)
				{
					long start = System.currentTimeMillis();
					JasperExportManager.exportReportToPdfFile("./build/reports/" + reportNames[i] + ".jrprint");
					System.err.println("Report : " + reportNames[i] + ". PDF export time : " + (System.currentTimeMillis() - start));
				}

				System.exit(0);
			}
			
		}
		catch(JRException ex) {
			ex.printStackTrace();
			System.exit(1);
		}
		catch(Exception ex) {
			ex.printStackTrace();
			System.exit(1);
		}
		
		
	}
	
	
	private static void usage() {
		System.out.println("JExcelApiApp usage:");
		System.out.println("\tjava JExcelApiApp -Ttask");
		System.out.println("\tTasks: fill | pdf | xls");
	}
	
	private static Connection getConnection() throws ClassNotFoundException, SQLException {
		String driver = "org.hsqldb.jdbcDriver";
		String url = "jdbc:hsqldb:hsql://localhost";
		String username = "sa";
		String password = "";
		
		// get database connection
		Class.forName(driver);	
		return DriverManager.getConnection(url, username, password);
	}
	
	
	
	
	
}