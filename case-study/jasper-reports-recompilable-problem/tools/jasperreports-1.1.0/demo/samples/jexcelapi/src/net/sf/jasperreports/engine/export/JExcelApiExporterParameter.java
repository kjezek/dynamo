/*
 * ============================================================================
 *                   GNU Lesser General Public License
 * ============================================================================
 *
 * JasperReports - Free Java report-generating library.
 * Copyright (C) 2001-2003 Teodor Danciu teodord@hotmail.com
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 * 
 * JasperSoft Corporation
 * 185, Berry Street, Suite 6200
 * San Francisco CA 94107
 * http://www.jaspersoft.com
 */

/*
 * Contributor: Manuel Paul <mpaul@ratundtat.com>, 
 *				Rat & Tat Beratungsgesellschaft mbH, 
 *				Muehlenkamp 6c,
 *				22303 Hamburg,
 *				Germany.
 */
package net.sf.jasperreports.engine.export;

import net.sf.jasperreports.engine.JRExporterParameter;


/**
 * @author Manuel Paul (mpaul@ratundtat.com)
 * @version $Id: JExcelApiExporterParameter.java,v 1.2 2005/10/21 13:24:27 teodord Exp $
 */
public class JExcelApiExporterParameter extends JRExporterParameter {

	protected JExcelApiExporterParameter(String name) {
		super(name);
	}

	public static final JExcelApiExporterParameter IS_ONE_PAGE_PER_SHEET = new JExcelApiExporterParameter("Is One Page per Sheet");
	public static final JExcelApiExporterParameter IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS = new JExcelApiExporterParameter("Is Remove Empty Space Between Rows");
	public static final JExcelApiExporterParameter IS_WHITE_PAGE_BACKGROUND = new JExcelApiExporterParameter("Is White Page Background");
	public static final JExcelApiExporterParameter IS_FONT_SIZE_FIX_ENABLED = new JExcelApiExporterParameter("Is Font Size Fix Enabled");
	
}
