/*
 * ============================================================================
 * GNU Lesser General Public License
 * ============================================================================
 *
 * JasperReports - Free Java report-generating library.
 * Copyright (C) 2001-2005 JasperSoft Corporation http://www.jaspersoft.com
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 * 
 * JasperSoft Corporation
 * 185, Berry Street, Suite 6200
 * San Francisco CA 94107
 * http://www.jaspersoft.com
 */
package net.sf.jasperreports.engine;

import java.util.Arrays;
import java.util.List;


/**
 * @author Teodor Danciu (teodord@users.sourceforge.net)
 * @version $Id: JRPrintElementIndex.java,v 1.1 2005/10/07 09:27:31 teodord Exp $
 */
public class JRPrintElementIndex
{


	/**
	 *
	 */
	private int reportIndex = 0;
	private int pageIndex = 0;
	private List elementIndexes = null;


	/**
	 *
	 */
	public JRPrintElementIndex(
		int reportIndex,
		int pageIndex,
		Integer[] elementIndexes
		)
	{
		this.reportIndex = reportIndex;
		this.pageIndex = pageIndex;
		this.elementIndexes = Arrays.asList(elementIndexes);
	}


	/**
	 *
	 */
	public int getReportIndex()
	{
		return this.reportIndex;
	}
		

	/**
	 *
	 */
	public int getPageIndex()
	{
		return this.pageIndex;
	}
		

	/**
	 *
	 */
	public Integer[] getElementIndexes()
	{
		return (Integer[]) elementIndexes.toArray(new Integer[elementIndexes.size()]);
	}

	
}
