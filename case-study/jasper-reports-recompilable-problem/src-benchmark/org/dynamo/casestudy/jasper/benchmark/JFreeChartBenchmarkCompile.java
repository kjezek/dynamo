package org.dynamo.casestudy.jasper.benchmark;

import org.apache.commons.io.FileUtils;
import org.dynamo.compiler.Compiler;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.ChainedOptionsBuilder;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.io.File;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

/**
 * Benchmark of JasperReports
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
@Measurement(iterations = 30, timeUnit = TimeUnit.MILLISECONDS)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 15)
@BenchmarkMode(Mode.AverageTime)
@State(Scope.Thread)
public class JFreeChartBenchmarkCompile {

    private static final File BIN = new File("build/benchmark/bin");

    private static final File SRC = new File("tools/jasperreports-1.1.0/src");

    private static final File CLASSPATH_ROOT = new File("tools/jasperreports-1.1.0/");

    private String classPath = "";

    @Setup(Level.Trial)
    public void beforeTrial() throws Exception {
        Collection<File> files = FileUtils.listFiles(CLASSPATH_ROOT, new String[] {"jar"}, true);

        for (File file : files) {
            classPath += file + File.pathSeparator;
        }
    }

    @TearDown(Level.Trial)
    public void afterTrial() throws Exception {
    }


    /**
     * Compile a single source code file into a folder using the dynamo compiler.
     *
     * @param bin    the bin folder to store classes and jars
     * @param src    the source code file
     * @param cp    classpath
     * @param params additional runtimeparameters
     * @return the folder containing the compiled class
     */
    public static File compile(File bin, File src, String cp, String... params) throws Exception {
        bin.delete();
        bin.mkdirs();
        assert bin.exists();

        String[] args = {
                "-" + Compiler.CLASSPATH, cp,
                "-" + Compiler.DESTINATION, bin.getAbsolutePath(),
                "-" + Compiler.SOURCE_PATH, src.getParentFile().getAbsolutePath()
        };

        String[] args2 = new String[args.length + params.length];
        System.arraycopy(args, 0, args2, 0, args.length);
        System.arraycopy(params, 0, args2, args.length, params.length);

        Compiler.main(args2);

        return bin;
    }


    @Benchmark
    public File compileJasperClassic() throws Exception {
        return compile(BIN, SRC, classPath, "-encoding", "ISO-8859-13", "-classic");
    }

    @Benchmark
    public File compileJasperDynamo() throws Exception {
        return compile(BIN, SRC, classPath, "-encoding", "ISO-8859-13");
    }


    public static void main(String[] args) throws RunnerException {
        ChainedOptionsBuilder opt = new OptionsBuilder()
                .include(JFreeChartBenchmarkCompile.class.getSimpleName())
                .jvmArgs("-Xmx2g", "-Xms2g", "-noverify")
                .forks(2)
                .shouldDoGC(true);

        if (args.length > 0) {
            opt = opt.output(args[0]);
        }

        new Runner(opt.build()).run();
    }

}
