package org.dynamo.casestudy.jasper.benchmark;

import net.sf.jasperreports.view.JasperViewer;
import org.dynamo.casestudy.jasper.JFreeChartApp;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.ChainedOptionsBuilder;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

/**
 * Benchmark of JasperReports
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
@Measurement(iterations = 30, timeUnit = TimeUnit.MILLISECONDS)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 15)
@BenchmarkMode(Mode.AverageTime)
@State(Scope.Thread)
public class JFreeChartBenchmarkRun {

    private JasperViewer jasperViewer;

    @TearDown(Level.Invocation)
    public void afterTrial() throws Exception {
        jasperViewer.dispose();
    }

    @Benchmark
    public JasperViewer renderReport() throws Exception {
        JFreeChartApp.main(new String[] {});
        jasperViewer = JFreeChartApp.jasperViewer;
        return jasperViewer;
    }


    public static void main(String[] args) throws RunnerException {
        ChainedOptionsBuilder opt = new OptionsBuilder()
                .include(JFreeChartBenchmarkRun.class.getSimpleName())
                .jvmArgs("-Xmx2g", "-Xms2g", "-noverify")
                .forks(2)
                .shouldDoGC(true);

        if (args.length > 0) {
            opt = opt.output(args[0]);
        }

        new Runner(opt.build()).run();
    }

}
