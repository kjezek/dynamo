This mini-project demonstrates how invokedynamic instruction can help
with making a project compatible.


Prerequisites:
-----------------------------

* Must use Java 1.7
  -> Java < 1.7 does not support invokedynamic
  -> jasperreports 1.0.1 does not work in Java 1.8 (do not know exact reason)

Test with ant build script:
----------------------------
1. ant run.original
   -> invokes jasperreports 1.0.1 with jfreechart 1.0.1
   -> works well

2. ant run.update
   -> invokes jasperreports 1.0.1 with jfreechart 1.0.12
   -> fails with
        java.lang.NoSuchMethodError: org.jfree.data.time.TimeSeries.<init>(Ljava/lang/String;)V

3. ant run.dynamic
   -> generates code with invokedynamic for all libraries
   -> recompiles Client applications and JasperReports with Dynamo Compiler
   -> libraries  in "build/lib-dynamic"
   -> report printing runs well


Directory structure:
---------------------------

lib-original/
 -> contains all libraries of jasperreports 1.0.1
lib-update/
 -> contains updated jfreechart libraries
build/lib-dynamic/
 -> contains 3rd lib enhanced or compiled with Dynamo


Obstacles:
* It is not possible to use invokedynamic instruction in byte code version < 1.7
  and just run (even in Java 1.7) - ClassFileFormat exception is thrown
  -> I had to recompile jasperreports to 1.7 byte-code first, then the byte-code manipulation could be performed
  -> Better approach is to update the byte-code (partly implemented, ongoging work)

* Two missing methods detected -- cannot be fixed by invokedynamic (automatically)
 -> removed manually
