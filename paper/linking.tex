
%Jens: I changed this to just linking, Method Resolution for Linking sounded a bit odd
% perhaps we could consider "Runtime Method Resolution" as an alternative ?
\section{Linking}
\label{sec:linking}

In this section, we describe the bootstrap process. We first discuss the strategy we are using 
to locate and select the target method. In a nutshell, we use an algorithm that aims at aligning linking behaviour with compile time behaviour to facilitate program comprehension by developers. We then describe the complexity of the algorithm, and some implementation issues.

\subsection{Resolution}
\label{ss:resolution}

At runtime, we need to resolve the reference in the bootstrap method and locate an actual target method. This method must be \textit{adaptable} to the original method. We only look for methods that are non-abstract, visible from the call site and have the same name and arity, but may have a different descriptor or are defined in a super type.  This is similar to the problem the compiler has to solve when it selects the most specific method \cite[sec 15.12.2.5]{JL8Spec}. 
However, we also have to take the return type into account. Not only can the return type change as we allow specialisation or narrowing of the return type, but we potentially also have to deal with return type overloading, a situation that can only arise in bytecode. In particular, the standard Java compiler uses synthetic \cite[sec 4.7.8]{JVM8Spec} bridge methods to deal with co-variant return types. This means that any runtime method resolution has to be able to deal with a situation where there are multiple methods with the same name, the same parameter types but different return types within one class. Since return type overloading is not supported by the Java language, we can make the assumption that if this situation arises, only one of these methods is non-synthetic.

%\footnote{The use of bridge methods in conjunction with separating the life cycle of application and library code can create complex runtime errors, and example is given by Robertson \cite{HazardOfCovarReturnTypes}. }. 
 

We refer to the selection of an adaptable method as \textit{runtime resolution}, or in this context just \textit{resolution} for short. Resolution is defined with respect to adaptations that can be performed by method handles \cite{rose2009bytecodes}. Different resolution strategies are possible: (1) a \textit{greedy strategy} that locates any adaptable method and selects it, (2) an \textit{optimal strategy} that tries to find the ``best match'' according to some metric and (3) an \textit{unambiguous best match strategy} that tries to find a method that is not only the best match in the sense of being optimal with respect to some order, but must also be strictly better than other candidates with respect to this order. 

As one of our objectives is to address inconsistencies between source and binary compatibility, we decided to use strategy (3) to mimic the compiler behaviour of choosing the most specific method \cite[sect 15.12.2.5]{JL8Spec}. While the overall aim is somehow similar to how the compiler processes method invocation expressions \cite[sect 15.12]{JL8Spec}, there are important differences caused by the differences in representation of language features such as generic types, static imports and varargs in source code and bytecode.

For a concise formulation of the algorithm, we propose a simple model, the \textit{type conversion graph (TCG)}. This graph captures subtype relationships between classes and interfaces, boxing and unboxing relationships between wrapper types and their respective primitive types, and widening conversion relationships between primitive types \cite[sect 5.1.2]{JVM8Spec}. We build the $TCG$ for a program by applying the following rules:

\begin{enumerate}
	\item All (primitive and reference) types except annotation types but including array types that occur in the program are added as vertices to $TCG$.
	\item If $T_1$ and $T_2$ are (non-generic) class, interface or array types and $T_1$ is a direct subtype of $T_2$ as defined in \cite[sects 4.10.2, 4.10.3]{JL8Spec}
	then an edge $T_1 \rightarrow T_2$ with a label $r$ is added to $TCG$.
	\item If $T_1$ and $T_2$ are primitive types and there is a widening conversion from $T_1$ to $T_2$ as defined in \cite[sect 5.1.2]{JL8Spec}
	then an edge $T_1 \rightarrow T_2$ with a label $p$ is added to $TCG$.
	\item If $T$ is a primitive type, and $Cl$ is the wrapper type of $T$, then an edge $T \rightarrow Cl$ with a label $b$ (for boxing) and an edge $Cl \rightarrow T$ with a label $u$ (for unboxing)  are added to $TCG$.
\end{enumerate}


%\todo[inline]{Kamil >> Jens:  I beleive that the grammar should be \texttt{p*|r*|br*|up*} as you may have more transitions on ``p'' edges:  short -> int -> long}
% Jens: No, if you have a look at https://docs.oracle.com/javase/specs/jls/se8/html/jls-5.html#jls-5.1.2  (which we cite), it already contains the transitive closure, for instance:

%The TCG encodes all type generalisation information plus conversions the compiler as well as method handles can perform via auto-boxing and - unboxing. 
Figure \ref{fig:graph-example} shows an example TCG. The intention behind the TCG is that paths between types represent valid (potentially composite) conversions. However, the compiler imposes additional constraints. For instance, a widening primitive conversion followed by a boxing conversion is not permitted in assignment and invocation contexts \cite[sects 5.2, 5.3]{JL8Spec}. As our motivation is to align compilation and linking behaviour, we impose the same restrictions. We do so by defining a \textit{valid path} between two types considered as vertices in the TCG as a path such that the labels of the edges in this graph spell a word defined by the following simple regular grammar: \texttt{p|r*|br*|up}. This grammar is derived from the rules used in \cite[sects 5.2, 5.3]{JL8Spec}\footnote{The identity conversion is represented by a path of length 0. The primitive widening conversion rules in \cite[sect 5.1.2]{JL8Spec} use the transitive closure for primitive widening conversions, we can therefore use \texttt{p} instead of \texttt{p*} in the grammar}.

% Jens: I have replaced this with the paragraph above
%The virtual edges (dashed in the example) are not physically added to the graph and they represent special treatment of boxing in Java. For instance a method with \texttt{double} parameter can be invoked as \texttt{int} due to widening conversion, but a method with \texttt{Double} cannot be invoked with \texttt{int}. For this reason, the graph does not contain physical path \texttt{int} $\rightarrow$ \texttt{double} $\rightarrow$ \texttt{Double}. In contrary, a method return type \texttt{Integer} can be assigned to \texttt{double} because \texttt{Integer} is unboxed to \texttt{int} and widened to \texttt{double} and thus the graph contains a respective path. This is consistent with the fact that the \texttt{Integer} return type cannot be assigned to \texttt{Double}. 
%
%This sometimes counter intuitive behaviour of Java must be treated by special branching of the algorithm for primitive and wrapper types to follow the rules defined in \cite[15.12.2]{JL8Spec}: (1) first the type resolution is performed as if boxing and unboxing were disabled, then (2) it is enabled only if a conversion has not been found. 


\begin{figure}
	\centering
	\includegraphics[width=10cm]{graph-example}
	\caption{Example Type Conversion Graph}
	\label{fig:graph-example}
\end{figure}

% Kamil: I removed that methods must be non-abstract, visible, and have the same name and arity as it was said in introduction
When we try to locate an adaptable method, we only consider methods that are non-synthetic\footnote{This will remove ambiguity if several methods with the same name and parameter type but different return types are present. This is discussed in more details in section \ref{ss:hazards}}. Given the TCG, the adaptability relationship between types $\sqsubseteq_{type} \subseteq T \times T$ can be easily defined as $t_1 \sqsubseteq_{type} t_2$ iff there is a valid path from $t_1$ to $t_2$ in the TCG. In order to identify, compare and select adaptable  methods, we need to analyse their (1) defining type (2) parameter types and (3) return types. We define an order between methods based on their \textit{extended descriptor} (\textit{XD}) comprising the type where the method is defined, followed by the parameter types and the return type. We use the syntax \texttt{<defining\_type>(parameter\_type*)return\_type} for XDs. 

The relation $\sqsubseteq_{type}$ can be easily promoted to a relationship between XDs of the same arity: $T^1(A_1^1,..,A_n^1)R^1 \sqsubseteq_{desc} T^2(A_1^2,..,A_n^2)R^2$ iff $R^2 \sqsubseteq_{type} R^1$ and $T^1 \sqsubseteq_{type} T^2$ and $A_i^1 \sqsubseteq_{type} A_i^2$ for each $i$. Note that the direction of $\sqsubseteq_{type}$ is reversed (``contravariant'') for return types. By treating the defining type as a virtual first parameter, we include possible target methods defined in super types. For instance, this allows adaptations in cases where methods have been pushed up the type hierarchy by refactoring\footnote{The standard JVM runtime method resolution already includes superclass lookup \cite[sect 5.4.3.3]{JVM8Spec}}. 

We can then define a simple disambiguation algorithm as follows: given an extended descriptor $\Delta = T(A_1,..,A_n)R$ and a set of adaptable descriptors $\{\Delta^i\}$, where $\Delta \sqsubseteq_{desc} \Delta^i$, we chose a descriptor $\Delta^k$ from $\{\Delta^i\}$ if $\Delta^k \sqsubseteq_{desc} \Delta^i$ for all $i \neq k$. This captures the intention of selecting the \textit{most specific method} that is adaptable. In case there is more than one minimal XD with respect to $\sqsubseteq_{desc}$, and the respective methods have the same parameter types, we chose the method with the more specific return type following the compiler \cite[sect 15.12.2.5]{JL8Spec}. If resolution fails to detect a unique XD, the process will result in a linkage error (instance of \texttt{java.lang.NoSuchMethodError}). 

An example where this occurs is when a method \texttt{Object foo(java.util.ArrayList)} is replaced by two methods
\texttt{String foo(java.util.AbstractList)} and \texttt{Object foo(java\-.io\-.Serializable)}\footnote{The class \texttt{ArrayList} extends \texttt{AbstractList} and implements the interface \texttt{Serializable}.} within the same class. Both methods are suitable targets, but disambiguation fails to detect a best method that is strictly better than the other one.

A side effect of the resolution algorithm just described is that the only possible targets for replaced constructor invocations are constructors defined within the same class. This follows from the fact that we only look for targets with the same name as the original target (i.e., \texttt{<init>}). While we also consider constructors from super classes as possible targets, these methods have a more general return type, and therefore do not qualify. It follows further that the type returned by the \texttt{invokedynamic} invocation is the same as the type of the original (\texttt{invokespecial (new)}) invocation. 
 
\subsection{Algorithm Complexity}

Adaptability is defined with respect to reachability in the TCG. Reachability is a potentially expensive operation. The worst case complexity of standard shortest path algorithms used to query adaptability between two types is quadratic \cite{dijkstra1959note}, while the pre-computation of a reachability index that enables constant time queries is super-quadratic but sub-cubic \cite{coppersmith1987matrix}. However, the paths that need to be traversed by the algorithm are generally short because of the flat hierarchies found in most Java programs \cite{tempero2008java}, and the complexity of standard shortest path and many reachability algorithms is near linear for sparse graphs. 

%\todo[inline]{Kamil >> Jens: is this true? I think we provide just numbers how fast we are, not how scalable we are }
% Jens: changed scalability > performance
We provide further evidence of the performance of our approach in the evaluation section. 


\subsection{Implementation Issues}

The runtime component is implemented as a small library that must be included in the class path of applications compiled or enhanced with dynamo. 
The method lookup algorithm is implemented in \texttt{org.\-dy\-namo.\-rt.\-Dynamo\-Boot\-strap}. This class has five static bootstrap methods corresponding to the different instructions (\texttt{bootstrapVirtual}, \texttt{bootstrapStatic}, \texttt{bootstrapInterface}) and the two variants of \texttt{invokespecial} supported (\texttt{bootstrap\-Special\-New}, \texttt{bootstrapSpecialSuper}). These methods differ slightly in terms of decoding and interpreting method types according to Table \ref{tab:desc}. They all invoke a common method which finds the most suitable method as described in section \ref{ss:resolution}.

We use reflection to gather information about types, their relationships and members, and to reason about this information. Although reflection is not ideal as it may trigger some unnecessary class loading if classes are analysed for members that are not used at the end, it facilitates the implementation of bootstrap methods. The actual target finder algorithm produces an instance of \texttt{java.lang.reflect.Method} which is easily converted to a method handle using the \texttt{MethodHandles.Lookup.unreflect()} protocol and its variants for constructors and special methods. The actual conversion of types is then easily performed by  \texttt{MethodHandle.asType()}. At the end of this process, a constant call site wrapping this handle is instantiated. 

The method \texttt{MethodHandle.asType()} supports all of the type conversions we use and therefore effectively performs the transformation of both return and parameter types. Since we replace classic invocations with \texttt{invokedynamic}, we automatically support cases where interfaces are converted to classes or vice-versa. Finally, changes where non-static target methods have evolved to static methods are handled by ignoring the first parameter when the method is invoked. This way, the descriptor described in Table \ref{tab:desc}, row 2 is produced. The transformation is achieved by using the \texttt{MethodHandles.dropArguments} API. The (simplified) implementation of a bootstrap method is shown in Listing \ref{ls:resolution}.

\begin{figure}
	\begin{lstlisting}[caption=Bootstrap method implementation example (simplified),frame=tlrb,columns=flexible,label=ls:resolution]{ls:resolution}
import java.lang.invoke.*;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

...
CallSite bootstrapVirtual(Lookup caller, String name, MethodType type) {

   Class<?> owner = type.parameterType(0);
   Method method = find(owner, name, type); // use resolution with TGC
   MethodHandle handle = lookup.unreflect(method);
   if (Modifier.isStatic(method.getModifiers())) {
      handle = MethodHandles.dropArguments(handle, 0, method.getDeclaringClass());
   }
   return new ConstantCallSite(handle.asType(type));
}
...
\end{lstlisting}
\end{figure}



% Jens: see sentence above 
%\begin{lstlisting}[label=static,caption=Non-static to Static Update,language=java]
%MethodHandle finalHandle = handle;
%if (Modifier.isStatic(method.getModifiers())) {
%  finalHandle = MethodHandles.dropArguments(handle, 0, method.getDeclaringClass());
%}
%\end{lstlisting}


