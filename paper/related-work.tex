\section{Related Work}
\label{sec:rel-wor}

Our work addresses issues related to binary compatibility. The study of binary compatibility goes back to the work by Forman et al. \cite{Forman:1995:RBC:217838.217880}, in the context of IBM's SOM object model. For Java, binary compatibility is formally defined in the language specification \cite[sect 13]{JL8Spec}. Drossopoulou et al. have proposed a formal model of binary compatibility in \cite{drossopoulou1998java}. 

Our work is motivated by issues that result from inconsistencies between source, binary and to some extent behavioural compatibility. These issues have been catalogued and studied by several authors, including des Rivi\`{e}res \cite{EvolvingJavaAPIs:2007}, Dietrich et al. \cite{brokenpromises,WhatDevelopersKnow} and Raemakers et al. \cite{raemaekers2012measuring}. In particular, they include empirical studies \cite{brokenpromises,raemaekers2012measuring} showing that binary compatibility issues occur in practice when programs and libraries used by these programs evolve independently.   Dig and
Johnson \cite{DigJohnson:06} have conducted an API evolution case study on
five real world systems (\textit{struts}, \textit{eclipse}, \textit{jhotdraw}, \textit{log4j} and a commercial application). They found that the majority of API-breaking changes were caused by refactoring, but did not distinguish between different types of compatibility. 
Mens et al. \cite{Mens:08} have studied the evolution of Eclipse (from version 1.0 to version 3.3). The focus of this study was to investigate the applicability of Lehmann's laws of software evolution \cite{Lehman:1985}. They found significant changes. Cosette and Walker have studied the evolution of APIs on a set of five Java open source programs \cite{cossette2012seeking}. They focused on generating change recommendation techniques that could then be used to give developers advice on how to refactor client code in order to adapt to API changes.

Several authors looked into how binary compatibility problems in Java programs can be avoided. Our work is somehow similar to binary component adaptation (BCA) by Keller et al. \cite{KellerHolzle98} as bytecode is modified in order to overcome certain compatibility problems. However, there are important differences: (1) BCA does not support changes to method descriptors, (2)  changes must be specified by the user in the form of delta files while our approach is completely automated (3) BCA modifies libraries (components) whereas we transform the program itself (4) BCA can alter types and their relationships and is therefore rather invasive as it changes the semantics of client programs using reflection (for instance, when interfaces are added to classes, and the client program uses \texttt{instanceof} guards)\footnote{In a trivial sense, the dynamo compiler also changes the semantics of programs as method invocations that used to result in errors succeed after compilation with dynamo. But this effects are intended and local as they only impact the objects aliased at the call site, whereas changing types will affect other, unrelated parts of the program as well.}


%
%Changes (4) following feedback from Alex Buckley. 
%(4) BCA can change the semantics of programs using reflection (for instance, when interfaces are added to classes, and the client program uses \texttt{instanceof} guards). , while the dynamo compiler is  completely transparent.
% 



Other related works include Dmitriev's proposal to use binary compatibility checks in order to optimise build systems \cite{Dmitriev:2002:LMT:582419.582453}, Barr and Eisenbach's rule-based tool to compare library versions in order to detect
changes that cause binary incompatibility \cite{BarrEisenbach:2003}, and refactoring-based approaches by  Dig et al.
\cite{Dig:2008} and Savga and Rudolf \cite{Savga:2007}, aiming at generating a compatibility layer that ensures binary
compatibility when used libraries evolve. Corwin et al. \cite{corwin03mj} have proposed a modular framework that uses a higher
level API on top of the Java classpath architecture. This is somehow similar to how the OSGi framework \cite{OSGi} operates.


Our work relies on the use of the \texttt{invokedynamic} instruction. One of the key features of \texttt{invokedynamic} is that it adds reflection-like features to the language without incurring the performance overhead imposed by the use of traditional reflection. 
The low performance overhead has been confirmed by several authors including Kaewkasi \cite{kaewkasi2010towards} and Ortin and Conde \cite{ortin2014runtime,Conde2O14jindy}. 


Within the Java standardisation process, JEP 276: Dynamic Linking of Language-Defined Object Models \cite{JEP276} is related to this research. The focus of JEP 276 is on supporting the compilation of object expressions often used in applications that require templating, while we suggest different compilation for standard Java call sites. The JEP 276 proposal does explicitly state that it does ``not wish to provide linking semantics for operations for any single programming language or execution environment for any such language''. 

There is one open source project we are aware of with a similar aim -- Kohsuke Kawaguchi's Bridge Method Injector \footnote{\url{http://bridge-method-injector.infradna.com/}}. The idea behind this project is to address one particular evolution problem we also consider - source-compatible changes of the return type. This is achieved by generating additional bridge methods in a post compilation step, a technique also used by the compiler when encountering co-variant return types. This will be discussed in more detail in section \ref{ss:hazards}. The bridge method injector relies on an annotation that has to be added by the author of the library that evolves. This requires that the author understands the effects this change has on client code, and this might not always be the case \cite{WhatDevelopersKnow}. On the other hand, our approach is completely automated and covers a wider range of evolution patterns.

This work is part of a wider trend towards self-adaptive software \cite{Salehie:2009:SSL:1516533.1516538,Salvaneschi2013}. 



