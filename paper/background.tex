\section{Background}
\label{sec:background}


\subsection{Binary vs Source Compatibility}  

The compiler and the linker use sets of rules to establish whether a method invocation can be resolved. These rule sets define \textit{source compatibility} (in the case of the compiler) and \textit{binary compatibility} (in the case of the linker). As far as Java (version 8) is concerned, binary compatibility is generally stricter than source compatibility as the compiler can reason about subtype relationships, the associations between primitive types and their respective wrapper types via auto-boxing and unboxing conversions \cite[sects 5.1.7, 5.1.8]{JL8Spec} and invocations of static methods from non-static contexts \footnote{There are some exceptions to this rule -- situations where a program is binary but not source compatible with a library. For instance, the use of  erasure can cause such scenarios.}.
%Jens: I have rephrased this a bit to soften this statement.
 
For instance, consider Listing \ref{exampleRC1}\footnote{Package declarations and imports are omitted.}. This example has a client program with a class \texttt{Main} invoking \texttt{Foo.get()} defined in a library that has two versions \texttt{lib-v1.jar} and \texttt{lib-v2.jar}. 
\texttt{Main} can be compiled against both versions of the library. However, when \texttt{Main} is compiled against \texttt{lib-v1.jar} and then executed with \texttt{lib-v2.jar}, a linkage error (\texttt{NoSuchMethodError}) is thrown as the descriptor \texttt{get()Ljava/util/Collection;} found at the call site does not match the new descriptor \texttt{get()Ljava/util/List;} and can therefore not be resolved.


\begin{lstlisting}[label=exampleRC1,caption=Specialising the return type of a method,language=java]
 // lib-v1.jar
 public class Foo {
   public static java.util.Collection get() {return new java.util.ArrayList();}
 }
 // lib-v2.jar
 public class Foo {
   public static java.util.List get() {return new java.util.ArrayList();}
 }
 // client program
 public class Main {
   public static void main(String[] args) {
      java.util.Collection coll = Foo.get();
      System.out.println(coll);
   }
 }
\end{lstlisting}

Listing \ref{exampleRC2} is similar, but this time the sole parameter type of \texttt{bar()} is changed from \texttt{int} to \texttt{java.\-lang.\-Integer}. The compiler deals with this situation by applying auto-boxing. But since the descriptor changes from \texttt{bar(I)V} to \texttt{bar(Ljava/\-lang/\-Integer;)V}, this change is not binary compatible. 


\begin{lstlisting}[label=exampleRC2,caption=Boxing of a parameter type,language=java]
 // lib-v1.jar
 public class Foo {
   public static void bar(int i) {System.out.println(i);}
 }
 // lib-v2.jar
 public class Foo {
   public static void bar(Integer i) {System.out.println(i);}
 }
 // client program
 public class Main {
   public static void main(String[] args) {new Foo().bar(42);}
 }
\end{lstlisting}

Finally, consider Listing \ref{exampleRC3}. \texttt{Main} still compiles against \texttt{Foo} in \texttt{lib-v2.jar}, but this time the compiler has to apply two adaptation rules: auto-boxing and type generalisation. The type of the invocation must also be changed as the \texttt{static} modifier has been added to \texttt{bar}. 


\begin{lstlisting}[label=exampleRC3,caption=Complex (but still compatible) evolution,language=java]
 // lib-v1.jar
 public class Foo {
   public void bar(int i) {System.out.println(i);}
 }
 // lib-v2.jar
 public class Foo {
   public static void bar(Object i) {System.out.println(i);}
 }
 // client program
 public class Main {
   public static void main(String[] args) {new Foo().bar(42);}
 }
\end{lstlisting}

% Jens: removed this -- sounds a bit odd and does not contribute much
%Above mentioned examples raise the question whether it is possible to provide rules used by the compiler also for linking. This is desirable as it will make applications more resilient to change. 

Evolution patterns and their impact on binary compatibility have been catalogued by des Rivières \cite{EvolvingJavaAPIs:2007}. Some of these problems could be avoided if linking was more consistent with compilation. In particular, we are interested in the following evolution patterns that are source compatible, but not binary compatible: 

\begin{enumerate}
	\item the specialisation of a reference return type of a method
	\item the narrowing of a primitive return type of a method
	\item the generalisation of a reference parameter type of a method
	\item the widening of a primitive parameter type of a method
	\item replacing the primitive return or parameter type of a method by the respective wrapper type
	\item replacing the wrapper return or parameter type of a method by the respective primitive type
	\item changing a non-static method to a static method
	\item changing a class to an interface or vice-versa
	\item some combinations of any number of evolution patterns from this list\footnote{See also section \ref{sec:linking} for a discussion on which of those combinations are supported.}
\end{enumerate}


\subsection{The invokedynamic Instruction}  

Prior to version 7, Java used four different bytecode instructions for method invocation. First, \texttt{invokestatic} is used to call static methods. Static methods are resolved at compile time. Next, \texttt{invokespecial} is used
for special cases including constructor and private method invocations and invocations via \texttt{super}. 
Finally, \texttt{invokevirtual} and \texttt{invokeinterface} are used to call non-static, non-private methods. Dynamic dispatch is used here, i.e. the method which is invoked is only computed at runtime based on the actual type of the receiver. 

Starting with Java 1.7, \texttt{invokedynamic}  was added to the instruction set \cite{rose2009bytecodes}. The motivation was to give programmers more control over the dispatch process, in particular to facilitate the implementation of 
dynamic languages like Ruby on the JVM \cite{JRubyAndInvokeDynamic}. The Java 7 JVM supports the \texttt{invokedynamic} instruction, it is not emitted by the Java 7 compiler. The Java 8 compiler uses \texttt{invokedynamic} to compile lambdas.

With \texttt{invokedynamic}, the method reference is resolved by means of a \textit{bootstrap method}. 
This user-implemented method is then used to locate the actual method being invoked.
This is fast as the bootstrap method is only invoked  by the linker during the resolution phase \cite[5.4.3.6]{JVM8Spec} and following method invocations skip the bootstrap process. At this point it is possible to implement adapters.  

The bootstrap method represents the target method as an instance of a \texttt{java.\-lang.\-invoke.\-Method\-Handle}. The method handles support transformations that can be used to achieve a linking behaviour that is similar to the behaviour of the compiler. This mapping behaviour is exposed in the API of \texttt{MethodHandle} by the \texttt{invoke} method (as opposed to the \texttt{invokeExact} method). 
The overall goal of this work is to use this API to perform appropriate type conversions to match the call site with the method found in a library that may have evolved. The difficult part is to lookup the best method which is suitable for re-typing.
