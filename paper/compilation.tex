\section{Compilation}

% Kamil: I commented this out. The para is itself the overview
%\subsection{Overview}

The mechanism we are proposing is based on the idea of swapping invoke instructions in bytecode. Our aim is to do this at compile time, so that the bytecode produced by the compiler can be transparently used at runtime with minimal additional configuration needed. However, there is another use case: to retrofit existing code only available in compiled form. We address this use case first and introduce the \textit{dynamo enhancer} - the tool that performs the bytecode transformations.
We can then use the enhancer to design the actual \textit{dynamo compiler}, implemented as a post compiler /  wrapper around the enhancer and the standard Java compiler. This design gives the dynamo compiler almost instantly the quality attributes needed for real world application. In particular, most Java compiler optimisations are available to us. An additional benefit of this design is that the enhancer can be used as a post compiler for other compilers emitting JVM bytecode, such as \texttt{scalac}. In the final part of this section, we discuss \textit{Dynamo DSL}, a lightweight domain-specific language that can be used to customise the dynamo-specific part of the compilation via a simple command line argument.  

\subsection{The Dynamo Bytecode Enhancer}


The bytecode enhancer is used to transform the bytecode emitted by a standard Java compiler, and replaces selective \texttt{invokestatic}, \texttt{invokevirtual}, \texttt{invokeinterface} and \texttt{invokespecial} instructions (from hereto referred to as \textit{classical invocations}) by \texttt{invoke\-dynamic}. The intention of the selection is to only replace invocations where the target is located in a library that may have a separate update cycle. This is achieved through \textit{filters}, described in more detail below.

Any classical invocation  can be expressed by a tuple $t = (opcode, C, m, desc)$
where $opcode$ represents one of the classical invocations,
$C$ is an owner class containing the method, $m$ is the method's name and $desc$ is the methods descriptor consisting of the formal parameter types and the return type of the method. To convert a method invocation to  \texttt{invokedynamic}, the original instruction from the tuple $t$ must be changed to fit the \texttt{invokedynamic} call site specifier 
\cite[sect 4.7.23]{JVM8Spec}. In particular, a reference to a bootstrap method must be provided that is then used at link time to locate the actual target of the invocation. 

Given a tuple $t$ containing call site information, a dynamic call site can be created as follows. An \texttt{invoke\-dynamic} instruction is created that has an index pointing to a constant pool entry of the type \texttt{CONSTANT\_InvokeDynamic}. This entry defines a bootstrap method with the following three parameters: (1) the \texttt{MethodHandles.Lookup} factory used to locate and check access to methods, (2) the method name, (3) and a \texttt{MethodType} representing the descriptor. Any number of additional user parameters may follow.

The JVM Specification provides information about descriptors used by the classical invocations and the descriptors required by method handles \cite[sect 5.4.3.5]{JVM8Spec}. By combining these two parts of the specification, we infer the transformation rules listed in Table \ref{tab:desc}.

\begin{table}
	\centering
	\begin{tabular}{|l|c|c|c|}
		\hline
		opcode          & source C,m,desc & target desc & parameters \\ 
		\hline
		\texttt{invokevirtual}   & \texttt{C,m,(A*)T}       & \texttt{(C,A*)T} &      \\ 
		\texttt{invokestatic}    & \texttt{C,m,(A*)T}       & \texttt{(A*)T}   &  \texttt{C }  \\ 
		\texttt{invokeinterface} & \texttt{C,m,(A*)T }      & \texttt{(C,A*)T} &      \\
		\texttt{invokespecial} (\texttt{new})   & \texttt{C,<init>,(A*)V}  & \texttt{(A*)C}   &   \\ 
		\texttt{invokespecial} (\texttt{super})   & \texttt{C,m,(A*)T}  &\texttt{ (C,A*)T}  &   \\ 
		\hline
	\end{tabular} 
	\caption{Translation from method descriptors to method handles}
	\label{tab:desc}
\end{table}




As discussed earlier, $C$ and $m$ represent the method owner type and its name, respectively. Furthermore, $A*$ is a set of input parameter types, $T$ represents the return type, and $V$ represents the \texttt{void} type (used in the descriptors of constructors). For virtual and interface invocations, the owner type is prepended to the list of argument types in the target descriptor. This is necessary to check the type of the \texttt{this} reference passed as the first argument to the respective method handle. For constructors, $C$ becomes the return type. The  descriptors of method handles for static invocations do not contain owner types at all. For this reason, we pass the owner type as an additional, user defined, parameter to the bootstrap method.  

The translation of the first three instructions in Table \ref{tab:desc}, \texttt{invokevirtual}, \texttt{invokestatic} and \texttt{invokeinterface} is straightforward as they all have similar semantics. For this reason the transformation only requires to swap instructions and customise descriptor according to the table. See Listings \ref{ls:transform1_before} and \ref{ls:transform1_after} for an example presenting simplified byte-code instructions. These instructions may all be used in cross-component invocations and thus each occurrence is a candidate for replacement by the bytecode enhancer.


\begin{figure}
\begin{minipage}{.45\textwidth}
\begin{lstlisting}[caption=Bytecode  for \texttt{new Foo().setValue(10)}  before enhancement,frame=tlrb,label=ls:transform1_before,firstnumber=1,keepspaces=true]{Name}
bipush	10
istore_1
...	// new lib/Foo()
iload_1
invokevirtual lib/Foo.setValue:(I)V
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.45\textwidth}
\begin{lstlisting}[caption=Enhanced bytecode for code in listing \ref{ls:transform1_before},frame=tlrb,label=ls:transform1_after,firstnumber=1,keepspaces=true]{Name}
bipush	10
istore_1
...	// new lib/Foo()
iload_1
invokedynamic setValue:(Llib/Foo;I)V
\end{lstlisting}
\end{minipage}
\end{figure}

The situation is less obvious for \texttt{invokespecial}. In this case, the instruction has several usages, including the invocation of private methods, the invocation of methods via the \texttt{super} keyword, and the invocation of constructor using the \texttt{this} keyword \cite[sect 6.5]{JVM8Spec}. We support the transformation of \texttt{invokespecial} used with \texttt{super} and constructors \footnote{Invocations of private methods are out of scope as they can not be used for cross-component invocations.}.  For \texttt{invokespecial (super)},
the transformation is equivalent to the transformations applied to \texttt{invokevirtual}  and \texttt{invokeinterface}. 

The transformation of \texttt{invokespecial} used in conjunction with the \texttt{new} keyword at object allocation sites is less straightforward. In bytecode, the respective methods invoked all have a special name \texttt{<init>}. 
A correct transformation requires the detection of a usage pattern that consists of a certain sequence of instructions, the so-called \textit{bytecode behaviour}. The pattern for constructors consists of the following three instructions  (1) \texttt{new C} (2) \texttt{dup} (3) \texttt{invokespecial C.<init>:(A*)V} \cite[sect 5.4.3.5]{JVM8Spec}, possibly with some intermediate instructions. The semantics of this sequence is: (1) create a new object of type $C$ and push it onto the stack, (2) duplicate the object on the top of the stack and (3) invoke the constructor. The object on the top of the stack is duplicated because one element is consumed (popped) by the constructor invocation and the object must remain on top of the stack so that the value can be assigned to a variable (usually using an \texttt{astore} instruction). To replace this sequence, it must be changed to a standard method invocation, which means that the instructions \texttt{new C} and \texttt{dup} must be dropped and \texttt{invoke\-special} must be replaced by \texttt{invoke\-dynamic}. Note that \texttt{invoke\-dynamic} returns a value, this value is pushed onto the stack and effectively replaces the two dropped instructions. If the constructor contains parameters, the transformed sequence will retain the instructions to push these parameters onto the stack before the \texttt{invoke\-dynamic} instruction is executed. An example transformation is shown in Listings \ref{ls:transform2_before} and \ref{ls:transform2_after}.

\begin{figure}
\begin{minipage}{.45\textwidth}
\begin{lstlisting}[caption=Bytecode for \texttt{new lib.Foo(10)},frame=tlrb,label=ls:transform2_before,firstnumber=1]{Name}
new           lib/Foo
dup
bipush        10
invokespecial lib/Foo."<init>":(I)V
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.45\textwidth}
\begin{lstlisting}[caption=Enhanced bytecode for code in listing \ref{ls:transform2_before},frame=tlrb,label=ls:transform2_after,firstnumber=1]{Name}
bipush        10
invokedynamic C$D:(I)Llib/Foo;
\end{lstlisting}
\end{minipage}
\end{figure}

Since \texttt{invokedynamic} cannot use \texttt{<init>} as the name parameter for the bootstrap method \cite[sect 4.10]{JVM8Spec}, an artificial name set to \texttt{C\$D}, meaning ``constructor dynamic'', is used instead. The use of the \$ sign in method names is legal, but according to the language specification ``The \$ sign should be used only in mechanically generated source code or, rarely, to access pre-existing names on legacy systems'' \cite[sect 3.8]{JL8Spec}.
By complying with this rule it is very unlikely that this name choice will create conflicts with user code. 

The actual bootstrap methods referenced in the modified bytecode are defined as static methods in the class \texttt{com.dynamo.rt.DynamoBootstrap}. There is one method for each instruction type (\texttt{bootstrapInterface}, \texttt{bootstrapStatic}, etc). The semantics of these methods is described in section \ref{sec:linking}.


\subsection{The Dynamo Compiler}

The dynamo compiler is implemented as a post compiler. It is a wrapper around the dynamo enhancer, combined with the standard Java compiler accessed via 
the compiler API (JSR-199) \cite{JSR199}. More explicitly, the dynamo compiler first uses the standard Java compiler through JSR-199 to compile compilation units in memory. The resulting bytecode is represented using a map that associates fully qualified class names with byte arrays. The compiler then uses the enhancer to apply bytecode transformations using information from (1) the compiler runtime parameters specifying the classpath, (2) compiler runtime parameters specifying filters. 

The classpath information is used to determine component boundaries. The filters are described in the next section, the options of the compiler command line interface (CLI) are described in appendix \ref{appendix:cli}.

Figure \ref{fig:compiler-design} shows the design of the dynamo compiler; Listing \ref{exampleCompiler} shows the basic usage of the compiler via its CLI. 

\begin{figure}
	\centering
	\includegraphics[width=12cm]{compiler-design}
	\caption{Compiler Design}
	\label{fig:compiler-design}
\end{figure}


\begin{lstlisting}[label=exampleCompiler,caption=Basic use of dynamo to compile all Java source code files in the \texttt{src} folder and store the .class files in the \texttt{bin} folder,float,floatplacement=H]
java -cp dynamo<..>.jar org.dynamo.compiler.Compiler -sourcepath ./src -d ./bin  
\end{lstlisting}

\subsection{The Dynamo Filter DSL}
\label{ss:dsl}

It is often desirable for users to retain fine-grained control over the compilation process. One of the more common use cases is 
not to use \texttt{invokedynamic} when invoking functionality from Java platform libraries, due to the strong emphasis on 
backward compatibility in these libraries \cite{KindsOfCompatibility,JDKCompatibilityRegions}.

% Jens: I removed this as it contradicts the results in benchmarking
%Another example may be invocation of a library where performance penalty of \texttt{invokedynamic} is still too high and a user prefers faster classical invokes, even if loosing flexibility.

For this reason, we have developed a lightweight domain-specific language that can be used to filter the methods and call sites where \texttt{invokedynamic} is used. These filters are used to select \textit{invocation records} consisting of two methods, the method containing the call site, and the target method invoked before enhancement takes place.

The method filter is composed of individual filters that all have the following properties: 

\begin{enumerate}
	\item a \textit{kind} (\texttt{+}, \texttt{-}) defining whether a filter is an include or an exclude filter
	\item a \textit{role} (\texttt{callsite}, \texttt{target}) defining whether a filter applies to the client method that has the call site, or to the target method to be invoked
	\item a \textit{class pattern} defining the classes to which the filter applies. Class names are fully qualified, with a dot used as package separator
	\item an optional \textit{method name pattern} defining the methods to which the filter applies
	\item an optional \textit{descriptor pattern} defining the descriptors to which the filter applies. This allows for the discrimination of overloaded methods. Descriptor patterns use the syntax defined in the JVM specification \cite[sect 4.3.3]{JVM8Spec} plus optional wild card characters.
\end{enumerate}

Filters can be defined using a simple domain specific language; the grammar of this language is given in appendix \ref{appendix:dsl}.
The \texttt{*} and \texttt{?} wild cards can be used in class names, method names and descriptor patterns. The \textit{default filter} is defined using the patterns listed in Table \ref{tab:default-filter}. 

\begin{table}[ht]
	\centering
	\caption{The default filter}
	\label{tab:default-filter}
	\begin{tabular}{p{4cm}p{8cm}}
		\\ \hline
		Filter definition                       & Description                                                                       		 	\\ \hline
		\texttt{+callsite *,+target *}          & include all invocation records         														\\
		\texttt{-target java.* }                & \multirow{5}{*}{\begin{minipage}{8cm}then exclude all packages included in the Java development kit.\end{minipage}}        		\\
		\texttt{-target javax.*}     			& \\
		\texttt{-target com.sun.*} 				& \\
		\texttt{-target sun.*} 					& \\
		\texttt{-target com.oracle.*} 			& \\ 
		\texttt{-target org.ietf.*} 			& \\ 
		\texttt{-target org.omg.*} 				& \\ 
		\texttt{-target org.w3c.*} 				& \\ 
		\texttt{-target org.xml.*} 				& \\ \hline
		
	\end{tabular}
\end{table}


The intention of this filter is to exclude all invocations of target methods defined in the Java Development Kit. A user-defined filter can modify the default filter by using additional exclude and include patterns. The main use case for include patterns 
is to selectively permit the enhancement of JDK method invocations. With additional exclude patterns, users can manually specify that enhancements for certain targets 
are not required since the libraries in which the respective methods are defined are known to be API stable. In case additional include and exclude filters conflict, we resolve this as follows: invocation records are accepted if they are accepted by at least one of the include filter and not accepted by any of the exclude filters. This resolution is consistent with how include and exclude patterns are handled in popular build tools, and we assume that developers are familiar with the practice.
Some examples of filters are given in Table \ref{tab:sample-filter}. 

\begin{table}[htb]
	\centering
	\caption{Filter examples}
	\label{tab:sample-filter}
	\begin{tabular}{p{6cm}p{6cm}}
		 \\ \hline
		Filter definition                       		& Description                                                                       		\\ \hline
		\texttt{-callsite com.foo.Bar}                   & exclude invocations from call sites within \texttt{com.foo.Bar  }                           \\
		\texttt{-target com.foo.* }                      & exclude invocation of targets in methods in packages starting with \texttt{com.foo}      \\
		\texttt{+target java.lang.String\#substring}     & include invocations of the \texttt{substring} methods defined in \texttt{String}                   		\\
		\texttt{+target java.lang.String\#substring(I)*} & include invocations of the \texttt{substring(int)} method defined in \texttt{String} 		\\ \hline
	\end{tabular}
\end{table}

The definition of the default filter is relatively coarse. For instance, the filter would also include a package with the prefix \texttt{com.oracle} that is 
not part of the Java developer kit, such as certain JDBC drivers. In order to override this behaviour, users must use custom filters.