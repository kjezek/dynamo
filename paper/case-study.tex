
\section{Benchmarks}
\label{sec:benchmark}


In this section we discuss sets of benchmarks used to test various dynamo components, and to assess the performance overhead induced by dynamo. 

\subsection{Compiler Benchmarks}
\label{ss:performance:compiler}

The compiler benchmarks are based on a comprehensive set of unit tests we have developed to quality assure dynamo. Tests are based on scenarios, each scenario consists of three classes:

\begin{enumerate}
	\item \texttt{version1/lib/Foo.java} - the source code of version 1 of a class \texttt{lib.Foo} providing a method
	\item \texttt{version2/lib/Foo.java} - the source code of version 2 of a class \texttt{lib.Foo} providing a modified method
	\item \texttt{client/Main.java} - the source code of the client program using the method provided by \texttt{Foo}
\end{enumerate}

During testing, the following sequence is executed for each scenario:

\begin{itemize}
	\item Compile both versions of \texttt{Foo} and package them in different libraries (\texttt{lib-v1} and \texttt{lib-v2}). This should succeed.
	\item Compile \texttt{Main} with javac against \texttt{lib-v1}. This should succeed.
	\item Run the compiled class \texttt{Main} with \texttt{lib-v1}. This should succeed.
	\item Run the compiled class \texttt{Main} with \texttt{lib-v2}. This should fail.
	\item Recompile \texttt{Main} with dynamo against \texttt{lib-v1}. This should succeed. 
	\item Run the recompiled class \texttt{Main} with \texttt{lib-v2}. This should succeed, except for the last two ``ambiguous'' benchmarks designed to produce linkage errors.
\end{itemize}


Scenarios are identified by self-descriptive unique names. For instance \texttt{invoke\-interface\-\_narrow\_ret} is a scenario that contains an \texttt{invokeinterface} instruction with a reference to a method that evolves by narrowing the return type. Table \ref{tab:benchmarks} shows an overview of the scenarios used for testing, the first column describes the change pattern, the other columns show the type of invocation that is being converted. These scenarios were designed to obtain full coverage of all possible combinations for common scenarios, and good partial coverage for more exotic cases such as chained conversions (like boxing and then generalising a parameter type). A check mark in the row-column intersection means that there is such a scenario, \texttt{n/a} indicates that such a scenario is impossible (example: return type conversions for constructor invocations) or does not make sense (example: converting an invocation of a certain type to an invocation of the same type without adapting parameter or return types).


\begin{table}[hbt]
	\centering
	\caption{Benchmark overview}
	\label{tab:benchmarks}
	\begin{tabular}{lp{1cm}p{1cm}p{1cm}p{1cm}p{1cm}}
		\hline 
		%		& \multicolumn{1}{l}{}             & \multicolumn{1}{l}{}              & \multicolumn{1}{l}{}                & \multicolumn{1}{l}{}   & \multicolumn{1}{l}{}                                                              \\
		%		& \multicolumn{1}{l}{\texttt{invoke\-static}} & \multicolumn{1}{l}{\texttt{invoke\-virtual}} & \multicolumn{1}{l}{\texttt{invoke\-interface}} & \multicolumn{1}{l}{\begin{tabular}[c]{@{}l@{}}\texttt{invoke\-special}\\ (\texttt{super})\end{tabular}} & \multicolumn{1}{l}{\begin{tabular}[c]{@{}l@{}}\texttt{invoke\-special}\\ (\texttt{new})\end{tabular}} \\ \hline
		
		& \texttt{invoke\-static} & \texttt{invoke\-virtual} & \texttt{invoke\-inter\-face} & \texttt{invoke\-special (\texttt{super})} & \texttt{invoke\-special (\texttt{new})} \\ \hline
		
		narrow return type             						& $\checkmark$	& $\checkmark$	 & $\checkmark$ 	& $\checkmark$ 				& n/a			\\
		specialised return type         					& $\checkmark$	& $\checkmark$	& $\checkmark$	& $\checkmark$	& n/a			\\
		% removed from final version
		% specialised return type form void					&  				& $\checkmark$	&  				&  				& n/a			\\
		specialised array return type 						& $\checkmark$	& $\checkmark$ 	& $\checkmark$	& $\checkmark$  & n/a			\\
		box return type                						& $\checkmark$	& $\checkmark$	& $\checkmark$	& $\checkmark$	& n/a			\\
		unbox return type              						& $\checkmark$	& $\checkmark$	& $\checkmark$	& $\checkmark$	& n/a			\\ \hline 
		
		widen parameter type           						& $\checkmark$	& $\checkmark$	& $\checkmark$	& $\checkmark$				& $\checkmark$	\\
		generalise parameter type      						& $\checkmark$	& $\checkmark$	& $\checkmark$	& $\checkmark$	& $\checkmark$	\\
		generalise array parameter type						& $\checkmark$	& $\checkmark$	& $\checkmark$	&  				&  				\\
		generalise array parameter to \texttt{Object}		&  				&  				& $\checkmark$	&  				&  				\\
		generalise array parameter to \texttt{Serializable}	&  				&  				& $\checkmark$	&  				&  				\\
		generalise array parameter to \texttt{Cloneable}	&  				&  				& $\checkmark$	&  				&  				\\
		box parameter type             						& $\checkmark$	&  $\checkmark$ & $\checkmark$	& $\checkmark$	& $\checkmark$	\\
		unbox parameter type           						& $\checkmark$	& $\checkmark$	& $\checkmark$	& $\checkmark$	& $\checkmark$	\\ 
		multiple methods with generalised parameters	    & $\checkmark$	&  				&  				&   			&        		\\ \hline 
		
		convert to \texttt{invokestatic}   					& n/a			& $\checkmark$	& $\checkmark$	& n/a				&	n/a			\\
		convert to \texttt{invokeinterface}					& n/a			& $\checkmark$	& n/a			& n/a				&	n/a			\\
		convert to \texttt{invokevirtual}					& n/a	& n/a			& $\checkmark$	& n/a			&	n/a			\\ \hline 
		
		unbox and widen parameter type 						& $\checkmark$	& $\checkmark$	& $\checkmark$	&				& $\checkmark$	\\
		box and narrow return type     						& $\checkmark$	& $\checkmark$	& $\checkmark$	&				&   n/a    		\\                
		box and generalised parameter type  				& $\checkmark$	& $\checkmark$	& $\checkmark$	& $\checkmark$ 	& $\checkmark$ 	\\ \hline 
		
		method ambiguous parameter     						& 				& $\checkmark$ 	&  				&   			&        		\\ 
		method ambiguous two parameters						& 				& $\checkmark$ 	&  				&   			&        		\\ \hline  
		
	\end{tabular}
\end{table}


We use the test scenarios as compiler micro-benchmarks. However, as we do not test linking at this stage, for some scenarios the code compiled with dynamo (\texttt{Main.java}) is identical. For instance, this is the case for the various ``generalise array parameter .. '' scenarios.  We remove these scenarios from the set of 65 scenarios listed in Table \ref{tab:benchmarks} in order to avoid double counting, the result is a set of 49 scenarios we can use for benchmarking. 


Performance measurement in Java is not straightforward as Java uses runtime optimisation (JIT, HotSpot).  To account for this, repeated invocations and JVM warm-up runs are necessary to produce statistically meaningful results \cite{georges2007statistically}. For this reason, we used JMH,\footnote{\url{http://openjdk.java.net/projects/code-tools/jmh/}} a tool that provides the respective features. For each experiment, we executed 15 warm-up and 30 trial runs. For the experiments, we used the Java(TM) SE Runtime Environment (build 1.8.0\_20-b26)
with a Java HotSpot(TM) 64-Bit Server VM (build 25.20-b23, mixed mode) on a MacBook with OSX 10.5.5, an 2.8 GHz Intel Core i7 processor, 16 GB 1600 MHz DDR3 and SSD disk.

Table \ref{tab:byte-code-enhancer-performance} summarises the performance results. We compare the dynamo compiler with the standard Java compiler accessed through JSR199. To achieve a fair comparison, we compile in memory in both cases and try to measure the net effect of the post compilation step when bytecode is manipulated. We report average, standard deviation and confidence intervals, all in ms. The fourth column contains the number of benchmarks tested, and column 5 is the average divided by this number, i.e., the typical time a single compilation takes. The results clearly show that the compile time overhead imposed by dynamo is very small. 

\begin{table}[hbt]
	\centering
	\caption{Dynamo vs classic compiler performance}
	\label{tab:byte-code-enhancer-performance}
	\begin{tabular}{llllll}
		\hline
		benchmarks                   & \begin{tabular}[c]{@{}l@{}}average \\ runtime\\ (ms)\end{tabular} & stdev (ms)    & \begin{tabular}[c]{@{}l@{}}bench-\\ mark\\ count\end{tabular} & \begin{tabular}[c]{@{}l@{}}average\\ runtime single\\ benchm. (ms) \end{tabular} & \begin{tabular}[c]{@{}l@{}}confidence interval \\ (ms) (99.9\%)\end{tabular} \\ \hline
		
		classic & 1923  	& 65 		& 49		& 	39.24		& {[}1857, 1988{]}  	\\  
		dynamo 	& 2142  	& 91		& 49		& 	43.71		& {[}2051, 2233{]}  	\\ \hline
	\end{tabular}
\end{table}



\subsection{Runtime Benchmarks}
\label{ss:performance:runtime}

We have also created a benchmark for runtime resolution. As the low performance overhead of \texttt{invokedynamic} is known and has been reported in previous research \cite{ortin2014runtime,Conde2O14jindy}, the benchmark is based on unit tests that only compute the target methods using the resolution algorithm described in \ref{ss:resolution} from a set of candidate methods. Therefore, the design of the benchmarks is driven by  the following considerations: (1) a benchmark has a (significant) number of candidate target methods scattered across several classes within the type hierarchy, (2) candidate methods are detected via conversions represented by non-trivial paths within the TCG including combinations of different edge types.

The individual benchmark scenarios are implemented as standard JUnit tests, and used for quality assurance as well as to assess performance. 
%The data are distributed together with the \textit{dynamimo-rt} module as a set of Java classes in packages categorising them plus one JUnit test file invoking 14 test cases in total. 
Each test case has a self-explanatory name to express its purpose.
% Jens: removed this, this is not intuitive and confusing without further discussion
% for instance \texttt{testUnboxWideRetType} tests un-boxing and widening of a return type.
The packages and respective test cases are:

\begin{itemize}
\item \texttt{specstaticreturntype} -- a simple scenario with a test to locate a static target method with a specialised return type
 \item \texttt{boxunbox} -- a scenario with tests that require the following conversions to locate the target method: (1) boxing, (2) un-boxing, (3) boxing and widening of parameter types and (4) un-boxing and narrowing of the return type 
 \item \texttt{methodoverloading} -- a scenario with multiple 
 d potential target methods, and tests checking different resolution strategies to select target methods
 \item \texttt{overloadinghierarchy} -- a scenario with multiple overloaded potential target methods scattered across a class and its direct and indirect super classes
 \item \texttt{disambiguatebyreturntype} -- a scenario to test situations where the target method is selected based on the return type, simulating a scenario similar to what will be further discussed in section \ref{ss:hazards}
\end{itemize}


\begin{table}[hbt]
	\centering
	\caption{Method resolution performance}
	\label{tab:method-resolution-performance}
	\begin{tabular}{llllll}
		\hline
		benchmarks                   & \begin{tabular}[c]{@{}l@{}}average \\ runtime\\ (ms)\end{tabular} & stdev (ms)    & \begin{tabular}[c]{@{}l@{}}bench-\\ mark\\ count\end{tabular} & \begin{tabular}[c]{@{}l@{}}average\\ runtime single\\ benchm. (ms) \end{tabular} & \begin{tabular}[c]{@{}l@{}}confidence interval \\ (ms) (99.9\%)\end{tabular} \\ \hline
		all	 & 0.180 	& 0.001 	& 14	& 	0.013	& {[}0.179, 0.181{]}  	\\  \hline
	\end{tabular}
\end{table}

Table \ref{tab:method-resolution-performance} summarises the performance results, using the same format as table \ref{tab:byte-code-enhancer-performance} described above. This result confirms that the performance overhead of runtime resolution is negligible. 


\section{Case Studies}
\label{sec:case-study}

To demonstrate the applicability of our approach, we present two case studies sourced from real world scenarios.

\subsection{Solving Compatibility Issues between Jasperreports and Jfreechart}
\label{ss:casestudy:jasperreports}

The first case study is sourced from a real world scenario using the popular Java reporting library \textit{jasperreports}\footnote{\url{http://community.jaspersoft.com/project/jasperreports-library}}. \textit{Jasperreports} uses several other libraries including \textit{jfreechart}\footnote{\url{http://www.jfree.org/jfreechart/}}, which evolve independently, and this can lead to incompatibilities. In particular, a problem occurs when \textit{jfreechart} evolves from version 1.0.0 to 1.0.12. In this upgrade, the constructor \texttt{TimeSeries(String)} in the class \texttt{org.jfree.data.time.TimeSeries} is changed to \texttt{TimeSeries(Comparable)}. There are several overloaded variants of this constructor, but for all of them the type of the first parameter is changed from \texttt{String} to \texttt{Comparable}. 

This change is particularly dangerous for two reasons: (1) The change happens between two micro versions, such evolutions are widely regarded as being compatible in accordance with popular semantic versioning schemes  \cite{osgi10semanticver,SemVer}. These schemes are widely used to represent compatibility contracts between collaborating components, and tools and frameworks like Maven and OSGi support them. 
(2) This change only affects binary compatibility, and its implications therefore depend on the mode of deployment. The issue does not occur when \textit{jasperreports} is built, but only if a dynamic system upgrade mechanism like OSGi or Java WebStart is used. The key observation here is that \texttt{String} implements the \texttt{Comparable} interface, and that generalising a parameter type generally preserves source compatibility.\footnote{There are exceptions where generalising a parameter type can create ambiguities in the presence of overloading that lead to compilation errors.}

The system used in the case study is depicted in Figure \ref{fig:case-study}. The system is represented using a model resembling a UML class diagram. However, the arrows represent the invocation of a method or constructor in the target class, and the dashed box represents the older version of the library. Labels on edges show the Java code at the respective call site. The model contains an application \texttt{client.jar}, which is a simple client application producing a basic report containing a chart. The example illustrates both compile and runtime dependencies. While the client application is compiled and invoked against both libraries, \textit{jasperreports} runs only against \textit{jfreechart-1.0.0} and this dependency is not resolved at compile time. 

\begin{figure}
\centering
\includegraphics[width=13	cm]{case-study-jasper}
\caption{Case study 1 application design}
\label{fig:case-study}
\end{figure}


This setting allows for at least three scenarios: (1) the client is compiled and executed with \textit{jfreechart-1.0.0}, (2) the client is compiled against \textit{jfreechart-1.0.0} and executed with \textit{jfreechart-1.0.12}, (3) the client is compiled and executed with \textit{jfreechart-1.0.12}. Only the first scenario succeeds with the standard Java compiler. In the second scenario, both dependencies \textit{client} $\rightarrow$ \textit{jfreechart} and \textit{jasperreports} $\rightarrow$ \textit{jfreechart} will fail due to the incompatible change in \texttt{TimeSeries(..)}. In the third scenario, the problem will be solved for the client as it can now be compiled, although invocations of \texttt{TimeSeries(..)} from \textit{jasperreports} will still fail. However, the working client will remain prone to similar problems as \textit{jfreechart} may evolve further. 

%Two incompatible scenarios do not have to be intuitive for most of developers as the \texttt{String} type is assignable to \texttt{Comparable} type and for this reason would be expected as a compatible library update. The involvement of dynamic dispatch makes the situation more intuitive as it performs required type conversion and makes all three scenarios equally compatible and working.

To solve this problem, we compiled the client with the dynamo compiler. This produces a client program that is compatible with both versions of \textit{jfreechart} and resilient to any possible future change of a similar nature. We also processed the \textit{jasperreports} library with the dynamo enhancer, demonstrating the application to third party or legacy software where only bytecode is available. Finally, we invoked the program and verified by inspection that both versions produce the same report. Listings \ref{ls:client-before} and \ref{ls:client-after} show the bytecode of the original client compiled with the standard Java compiler followed by the enhanced bytecode. The output is provided in the format produced by the \texttt{javap} tool, with comments revealing values defined in the constant pool. The modifications follow the process described above: the instructions creating the object and invoking its constructor are replaced by \texttt{invokedynamic}, which refers to the respective bootstrap method.

\begin{figure}
\begin{lstlisting}[caption=Client code compiled with javac,frame=tlrb,columns=flexible,label=ls:client-before]{ls:client-before}
ldc            #10  // String 'Series'
astore_2      
new            #11  // class org/jfree/data/time/TimeSeries
dup           
aload_2       
invokespecial #12 // Method org/.../TimeSeries."<init>":(Ljava/lang/String;)V
\end{lstlisting}
\end{figure}

\begin{figure}
\begin{lstlisting}[caption=Client code compiled with dynamo,frame=tlrb,columns=flexible,label=ls:client-after]{ls:client-after}
ldc            #10      // String 'Series'
astore_2      
aload_2       
invokedynamic #130,  0 // InvokeDynamic #3:C$D:(Ljava/../String;)Lorg/../TimeSeries;

BootstrapMethods:
 3: #126 invokestatic org/dynamo/rt/DynamoBootstrap.bootstrapSpecialNew:(
           Ljava/lang/invoke/MethodHandles$Lookup;
           Ljava/lang/String;
           Ljava/lang/invoke/MethodType;)Ljava/lang/invoke/CallSite;
\end{lstlisting}
\end{figure}


% To summarise this case study, the enhanced application is less prone to incompatibility problems as it can function with wider range of library updates. 
% Jens: I removed the last paragraph, it sounds a bit odd and does not contribute any new information. 

We have also used this case study as a macro benchmark in order to measure the overhead of using dynamo on a real-world program.  Again, JMH was used to conduct these experiments, with 15 warm-up and 30 trial runs. Table \ref{tab:jasper-compiler-performance} summarises the compiler results, and confirms that the performance overhead of using dynamo is small.

\begin{table}[hbt]
	\centering
	\caption{Dynamo vs classic compiler performance for \textit{jasperreports}}
	\label{tab:jasper-compiler-performance}
	\begin{tabular}{llll}
		\hline
		benchmark   	& average runtime (ms) 	& stdev (ms)  	& confidence interval (ms) (99.9\%) \\ \hline
		classic 		& 1278  				& 52 			&  {[}1255,1302{]}  	\\
		dynamo 			& 1330  				& 56 			&  {[}1305,1355{]}  	\\  
		\hline
	\end{tabular}
\end{table}


We have also measured the runtime overhead. In this benchmark, a simple report is generated first by running a simple demo program that uses \textit{jasperreports} compiled with the classic compiler, and then by running it again with \textit{jasperreports} compiled by dynamo. We do use the same version of \textit{jfreechart} (1.0.0) in both cases in order to avoid a bias that would be caused by the different performance characteristics of different versions of this library. Therefore, the same classes are used in both cases, but the generated bytecode and the method used for linking differs. The respective results are reported in table \ref{tab:jasper-runtime-performance}. This again confirms that the overhead of dynamo runtime resolution is small.

\begin{table}[hbt]
	\centering
	\caption{Runtime performance of simple report generation with \textit{jasperreports}}
	\label{tab:jasper-runtime-performance}
	\begin{tabular}{llll}
		\hline
		benchmark   	& average runtime (ms) 		& stdev (ms)  	& confidence interval (ms) (99.9\%) \\ \hline
		classic 		& 168  						& 9 			&  {[}165,172{]}  					\\
		dynamo 			& 175  						& 12 			&  {[}154,218{]}  					\\  
		\hline
	\end{tabular}
\end{table}




\subsection{Avoiding the Hazards of Covariant Return Types and Bridge Methods}
\label{ss:hazards}

In a second case study, we demonstrate how compilation with dynamo can avoid an error that was encountered when the JDK was refactored to use more covariant return types when overriding \texttt{clone()} methods\footnote{\url{http://mail.openjdk.java.net/pipermail/core-libs-dev/2012-January/009119.html} [Accessed: October 20, 2015]}. We use the formulation of this problem presented in \cite{HazardOfCovarReturnTypes}.
The scenario consists of three simple classes, \texttt{Wrapper}, \texttt{WrapperChild} and \texttt{Wrapper\-Grand\-child}, the source code is shown in listing \ref{ls:cs2-src}. 
Both \texttt{Wrapper\-Child} and \texttt{Wrapper\-Grand\-child} override \texttt{wrap}, but \texttt{Wrapper\-Grand\-child} does so using a covariant return type. When a client calls \texttt{wrap} on an object that is an instance of \texttt{Wrapper\-Grand\-child} but declared as \texttt{Wrapper}, an \texttt{invokevirtual} instruction pointing to a \texttt{wrap} method with the descriptor \texttt{(LObject;)LCollection;}  (package names omitted)  is used. Therefore, a method with such a descriptor must exist in \texttt{Wrapper\-Grand\-child}. The compiler solves this problem by generating a synthetic bridge method with the descriptor \texttt{(LObject;)LCollection;} that then just delegates to the actual method with the descriptor \texttt{(LObject;)LList;}. This is therefore a case of return type overloading. The respective bytecode is shown in listing \ref{ls:cs2-bytecode}\footnote{The repeated \texttt{checkcast} instruction is a Java compiler bug reported in \url{http://bugs.java.com/bugdatabase/view\_bug.do?bug\_id=6246854}[Accessed: May 3, 2016]}.


\begin{figure}
	\begin{lstlisting}[caption=Case study 2 source code (imports omitted),frame=tlrb,columns=flexible,label=ls:cs2-src]{ls:cs2-src}
 public class Wrapper {
	 public Collection wrap(Object o) {
		   Collection c = new ArrayList();
		   c.add(o);
		   return c;
	 }
 }
	
 public class WrapperChild extends Wrapper {
	 @Override public Collection wrap(Object o) {
		   return super.wrap(o);
	 }
 }
	
 public class WrapperGrandchild extends WrapperChild {
	 @Override public List wrap(Object o) {
		   return (List) (super.wrap(o));
	 }
 }
	\end{lstlisting}
\end{figure}


 

\begin{figure}
	\begin{lstlisting}[caption=Case study 2 bytecode of \texttt{Wrapper\-Grand\-child},frame=tlrb,columns=flexible,label=ls:cs2-bytecode]{ls:cs2-bytecode}
  // access flags 0x1
  public wrap(Ljava/lang/Object;)Ljava/util/List;
  ALOAD 0
  ALOAD 1
  INVOKESPECIAL WrapperChild.wrap (Ljava/lang/Object;)Ljava/util/Collection;
  CHECKCAST java/util/List
  CHECKCAST java/util/List
  ARETURN
  
  // access flags 0x1041
  public synthetic bridge wrap(Ljava/lang/Object;)Ljava/util/Collection;
  ALOAD 0
  ALOAD 1
  INVOKEVIRTUAL WrapperGrandchild.wrap (Ljava/lang/Object;)Ljava/util/List;
  ARETURN
  \end{lstlisting}
\end{figure}


The problem occurs when \texttt{Wrapper} and \texttt{WrapperChild} on the one hand and \texttt{Wrapper\-Grand\-child} on the other hand are deployed in different libraries. When \texttt{WrapperChild} is refactored to also use a covariant return type, it too gets two \texttt{wrap} methods - the actual method with the descriptor \texttt{(LObject;)LList;} and the synthetic methods with the descriptor \texttt{(LObject;)LCollection;}. Now assume we execute the code in listing \ref{ls:cs2-app}. 


\begin{figure}
	\begin{lstlisting}[caption=Case study 2 client code,frame=tlrb,columns=flexible,label=ls:cs2-app]{ls:cs2-app}
	new WrapperGrandchild().wrap("x");
	\end{lstlisting}
\end{figure}


Executing this line invokes \texttt{WrapperGrandchild.wrap:(LObject;)LList;}. This method calls \texttt{WrapperChild.wrap:(LObject;)LCollection;} via \texttt{super} as this was the only method in \texttt{WrapperChild} that was available when \texttt{WrapperGrandchild} was compiled against the old version of \texttt{WrapperChild}. This method is now a bridge method, and therefore calls  \texttt{WrapperChild.wrap:(LObject;)LList} using an \texttt{invokevirtual} instruction. Since the actual type of the receiver object is \texttt{WrapperGrandchild}, this call is dispatched to \texttt{Wrapper\-Grand\-child.wrap:(LObject;)LList;}. This causes the program to loop infinitely and terminate with a \texttt{StackOverflowError}. The respective call graph is shown in Figure \ref{fig:casestudy2-callgraph-stackoverflow}.

\begin{figure}
\begin{lstlisting}[caption=Bytecode of  \texttt{Wrapper\-Grand\-child} generated by dynamo,frame=tlrb,columns=flexible,label=ls:cs2-enhanced]

public wrap(Ljava/lang/Object;)Ljava/util/List;
 aload_0
 aload_1
 invokedynamic #31 // wrap:(LWrapperChild;Ljava/lang/Object;)Ljava/util/Collection;
 checkcast java/util/List
 checkcast java/util/List
 areturn
  
public synthetic bridge wrap(Ljava/lang/Object;)Ljava/util/Collection;
 aload_0
 aload_1
 invokevirtual #4  // Method wrap:(Ljava/lang/Object;)Ljava/util/List;
 areturn

Constant pool:
 #4 = Methodref  // WrapperGrandchild.wrap:(Ljava/lang/Object;)Ljava/util/List;
#31 = InvokeDynamic// wrap:(LWrapperChild;Ljava/lang/Object;)Ljava/util/Collection;
 
\end{lstlisting}
\end{figure}


Using the dynamo compiler prevents this from happening not because of the signature adaptation, but due to the disambiguation strategy used during runtime resolution. Resolution only considers non-synthetic methods. Therefore, we have two adaptable candidate methods with the extended descriptors \texttt{Wrapper:(Object)Collection} and \texttt{WrapperChild:(Object)List}. Both are minimal (most specific), but because they both have the same parameter types, the method with the more specific return type \texttt{WrapperChild:(Object)List} is selected. The replacement pattern used is \texttt{invokespecial} (\texttt{super}) as the call via \texttt{super} is the cross-component call. The bytecode produced by \textit{dynamo} is shown in Listing \ref{ls:cs2-enhanced}, the respective call graph is shown in Figure \ref{fig:casestudy2-callgraph-dynamo}.


\begin{figure}[h!]
	\begin{minipage}{.48\textwidth}
	\centering
	\includegraphics[height=0.6\textwidth]{hazard-callgraph-stackoverflow}
	\caption{Call graph after refactoring, compiled with javac}
	\label{fig:casestudy2-callgraph-stackoverflow}
	\end{minipage}\hfill 
	\begin{minipage}{.48\textwidth}
	\centering
	\includegraphics[width=0.6\textwidth]{hazard-callgraph-dynamo}
	\caption{Call graph after refactoring, compiled with dynamo}
	\label{fig:casestudy2-callgraph-dynamo}
	\end{minipage}\hfill
\end{figure}
