% This is a template for producing artifact descriptions associated with ECOOP papers
% Adapted from the standard LIPIcs template by Camil Demetrescu
% See lipics-manual.pdf for further information.
% April 22, 2015

\documentclass[a4paper,UKenglish]{lipics-v2016}
% for A4 paper format use option "a4paper", for US-letter use option "letterpaper"
% for british hyphenation rules use option "UKenglish", for american hyphenation rules use option "USenglish"
% for section-numbered lemmas etc., use "numberwithinsect"
 
\usepackage{microtype}%if unwanted, comment out or use option "draft"

\usepackage{url}
%\graphicspath{{./graphics/}}%helpful if your graphic files are in another directory

\bibliographystyle{plainurl}% the recommended bibstyle


% ARTIFACT: Include the following input command here
\input{artifactcmds}

% Author macros::begin %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ARTIFACT: Please use the same title as the corresponding ECOOP paper and append the text "(Artifact)"
% ARTIFACT: Add as a footnote the reference to the corresponding ECOOP paper
\title{Magic with Dynamo -- Flexible Cross-Component Linking for Java with Invokedynamic (Artifact)
\footnote{This artifact is a companion of the paper:  Kamil Jezek and Jens Dietrich, ``Magic with Dynamo -- Flexible Cross-Component Linking for Java with Invokedynamic '', Proceedings of the 30th European Conference on Object-Oriented Programming (ECOOP 2016), Rome, Italy, July 2016. This work was partially supported by Oracle Labs, Australia.}
}
\titlerunning{Dynamo (Artifact)} %optional, in case that the title is too long; the running title should fit into the top page column

% ARTIFACT: Authors may not be exactly the same as the ECOOP paper, e.g., you may want to include authors who contributed to the preparation of the artifact, but not to the ECOOP companion paper
%% Please provide for each author the \author and \affil macro, even when authors have the same affiliation, i.e. for each author there needs to be the  \author and \affil macros
\author[1]{Kamil Jezek}
\author[2]{Jens Dietrich}
\affil[1]{NTIS – New Technologies for the Information Society \\ Faculty of Applied Sciences, University of West Bohemia\\ Pilsen, Czech Republic \\ \texttt{kjezek@kiv.zcu.cz}}
\affil[2]{School of Engineering and Advanced Technology, Massey University\\ Palmerston North, New Zealand 
	\\ \texttt{j.b.dietrich@massey.ac.nz}}

\authorrunning{
	 K. Jezek and J. Dietrich
} 

\Copyright{Kamil Jezek and Jens Dietrich}


\subjclass{
D.1.5 Object-oriented Programming
D.2.13 Reusable Software
D.3.4 Processors	
}

\keywords{Java,compilation,linking,binary compatibility,invokedynamic}
% Author macros::end %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Editor-only macros:: begin (do not touch as author)%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\EventEditors{John Q. Open and Joan R. Acces}
\EventNoEds{2}
\EventLongTitle{42nd Conference on Very Important Topics (CVIT 2016)}
\EventShortTitle{CVIT 2016}
\EventAcronym{CVIT}
\EventYear{2016}
\EventDate{December 24--27, 2016}
\EventLocation{Little Whinging, United Kingdom}
\EventLogo{}
\SeriesVolume{42}
\ArticleNo{23}
% Editor-only macros::end %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\maketitle

\begin{abstract}
Modern software systems are not built from scratch. They use functionality provided by libraries. These libraries evolve and often upgrades are deployed without the systems being recompiled. 
In Java, this process is particularly error-prone due to the mismatch between source and binary compatibility, 
and the lack of API stability in many popular libraries. 
We propose a novel approach to mitigate this problem based on the use of \texttt{invokedynamic} instructions for cross-component method invocations. The dispatch mechanism of \texttt{invokedynamic} is used to provide on-the-fly signature adaptation. We show how this idea can be used to construct a Java compiler that produces more resilient bytecode. 
We present the dynamo compiler, a proof-of-concept implemented as \texttt{javac} post compiler, and evaluate our approach using several benchmark examples and two case studies showing how the use of the dynamo compiler can prevent certain types of linkage and stack overflow errors that have been observed in real-world systems.
 \end{abstract}

% ARTIFACT: please stick to the structure of 7 sections provided below

% ARTIFACT: section on the scope of the artifact (what claims of the paper are intended to be backed by this artifact?)
\begin{scope}
  The artifact is designed to support repeatability of all the experiments of the 
  companion paper, allowing users to test \textit{dynamo} on a variety of benchmarks.
\end{scope}

% ARTIFACT: section on the contents of the artifact (code, data, etc.)
\begin{content}
The artifact is packaged as a VM image that can be opened with VirtualBox version 4.2.30 or better. VirtualBox can be obtained for free from \url{https://www.virtualbox.org/wiki/Downloads}. The VM uses Ubuntu 15.10, and has several tools (Java, Maven, Ant) pre-installed. It also contains the complete project source code, and pre-built binaries. 

Once the VM started and the user has logged in, the script \textit{run-evaluation.sh} located on the desktop (\textit{$\sim$/Desktop}) can be used to run a "guided" evaluation. It builds \textit{dynamo} and runs all experiments. Between steps a short description of the work done / to be done is displayed and the user is prompted for input to continue. The whole project is stored in \textit{$\sim$/dynamo}.

\end{content} 

% ARTIFACT: section containing links to sites holding the
% latest version of the code/data, if any
\begin{getting}
% leave empty if the artifact is only available on the DROPS server.
% otherwise, provide links to the latest version of the artifact (e.g., on github)
  The latest version of our code is also available from the bitbucket project web site: 
  \url{https://bitbucket.org/kjezek/dynamo}.
\end{getting} 

% ARTIFACT: section specifying the platforms on which the artifact is known to
% work, including requirements beyond the operating system such as large
% amounts of memory or many processor cores
\begin{platforms}
  The artifact is known to work on any platform running Oracle VirtualBox
  version~4 (\url{https://www.virtualbox.org/}). To get meaningful results from experiments,
  it is recommended to use at least 4GB of RAM and 2 CPU cores. Otherwise performance-related results may
  be compromised by swapping and Java garbage collector activity.
\end{platforms}

% ARTIFACT: section specifying the license under which the artifact is
% made available
\license{The MIT License (\url{https://opensource.org/licenses/MIT})}

% ARTIFACT: section specifying the md5 sum of the artifact master file
% uploaded to the Dagstuhl Research Online Publication Server, enabling 
% downloaders to check that the file is the expected version and suffered 
% no damage during download.
\mdsum{6f769a041059f704068b5a9e4b387c04}

% ARTIFACT: section specifying the size of the artifact master file uploaded
% to the Dagstuhl Research Online Publication Server
\artifactsize{2.4 GB}


% ARTIFACT: optional appendix
%\appendix

%\section{My Appendix}

% Add here any further material you would like to include. For instance, if the artifact is itself a PDF document, add it here.


% ARTIFACT: include here any additional references, if needed...

%% Either use bibtex (recommended), but commented out in this sample

%\bibliography{dummybib}

%% .. or use the thebibliography environment explicitely



\end{document}
