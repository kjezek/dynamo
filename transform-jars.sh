#!/bin/sh

ROOT_PATH=/home/kamilos/qualitas_corpus/QualitasCorpus-20120401f/Systems/
OUTPUT_PATH=/home/kamilos/tmp/transformed-jars/

PROGRAM=/home/kamilos/projects/jcs/verifiqua-dynamic-enhancer/target/verifiqua-dynamic-enhancer-1.0-SNAPSHOT.jar

for f in $(find "$ROOT_PATH" -name *.jar); do

    # Split path by the word 'System' into array.
    pathArray=(${f//Systems/ })
    # Second part of the path is a relative path within the corpus.
    outputPath=$OUTPUT_PATH""${pathArray[1]}

    # create output directory
    dir=$(dirname "${outputPath}")
    mkdir -p ${dir}

    # invoke transformation
    java -jar $PROGRAM $f $outputPath
done
