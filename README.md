# About #
Dynamo is a framework for flexible method dispatch in Java, the main use case is to avoid linkage errors resulting from the mismatch between source - and binary compatibility. The basic idea is to replace the invocation of methods defined in libraries from callsites in the main program by `invokedynamic` instructions. Dynamo consists of the following modules:

* **Dynamo Compiler** -- a replacement for `javac`, it generates Java compatible byte-code with flexible method dispatch.
* **Dynamo Enhancer** -- byte-code transformer usable for legacy or third-party binaries.
* **Dynamo Runtime** -- a runtime component that handles dynamic method dispatch.

# Overview #
The Java compiler generates method references at callsites using descriptors - combinations of parameter and return types. These descriptors must be matched exactly when the JVM links code. This  can result in unexpected results when the target methods have evolved in a way that would have been compatible from the compilers point of view. 

In other terms, Java distiguishes between `source` and `binary` compatibility. For instance, consider the following example:

![example-commons-io.png](https://bitbucket.org/repo/qG9rr6/images/2375384894-example-commons-io.png)

The client can be compiled with both the old and the new version of the library as the update is `source` compatible. However, the client cannot be invoked with the new version once it has been compiled with the old one, as the change is not `binary`compatible. Instead, this will result in a linkage error:


```
#!java
java.lang.NoSuchMethodError: org.apache.commons.io.LineIterator.next()Ljava/lang/String;
```


The problem goes away if **Dynamo** is used. The  **Dynamo Compiler** generates byte-code that is more resilient and can adapt to certain changes in the descriptors of target methods defined in libraries. Then, the **Dynamo Runtime** performs necessary type adaptations, converting the `()Ljava/lang/Object;` descriptor at the callsite to `()Ljava/lang/String;` in the example above.

# Usage #

Use **Dynamo Compiler** in the same way as standard `javac` to compile `.java` source files and it produces standard `.class` files.


```
#!java

java -jar dynamo-compiler-<version>.jar -cp <classpath> -sourcepath <dir> -d <destination>
```
example:

```
#!java

java -jar dynamo-compiler-<version>.jar -cp log4j.jar -sourcepath /src -d /bin
```
To run application with Dynamo enhanced classes, the **Dynamo runtime** component must be included on `classpath`. For instance, run your application normally, only add Dynamo to classpath:


```
#!java

java -cp dynamo-rt-<version>.jar:<original-class-path> -jar your-app.jar
```

If only byte-code of legacy or third-party applications is available, the **Dynamo Enhancer** can be used.  It gets byte-code in a JAR file on its input and produces the same byte-code that would be produced by the **Dynamo Compiler**.


```
#!java

java -jar dynamo-enhancer-<version>.jar original.jar modified.jar
```

There are additional parameters that can be used to customise the invocations that are being replaced by **Dynamo**, see the paper below for details. 


# Obtaining Dynamo #

Recommended downloads: latest release [Dynamo 1.3](http://relisa-dev.kiv.zcu.cz/dynamo/releases/dynamo-1.3-bin.zip)
and [sources](http://relisa-dev.kiv.zcu.cz/dynamo/releases/dynamo-1.3-src.zip).

For most recent features, [latest nightly builds](http://relisa-dev.kiv.zcu.cz/dynamo/nightly/) can be downloaded.  

Finally, Dynamo can be build from the latest version using `git`, `java` and `maven`

```
#!java
git clone git@bitbucket.org:kjezek/dynamo.git
cd dynamo
mvn clean install

```


# References #


1. [Kamil Jezek, Jens Dietrich: Magic with Dynamo – Flexible Cross-Component Linking for Java with Invokedynamic. ECOOP'16.](http://2016.ecoop.org/event/ecoop-2016-papers-magic-with-dynamo-flexible-cross-component-linking-for-java-with-invokedynamic) - paper describing **Dynamo** in detail, the paper is accompanied by an evaluation artifact packaged as VirtualBox VM. 

The following papers are empirical studies that show the scope of the problem addressed by **Dynamo**:

2. K Jezek, J Dietrich, P Brada: How Java APIs Break-An Empirical Study. Information and Software Technology, 2015. [DOI](http://dx.doi.org/10.1016/j.infsof.2015.02.014)
3. J Dietrich, L Jezek, P Brada: What Java Developers Know About Compatibility, And Why This Matters. Journal for Empirical Software Engineering, 2015. [DOI](http://dx.doi.org/10.1007/s10664-015-9389-1) [PREPRINT](http://arxiv.org/pdf/1408.2607v1.pdf)
4. J Dietrich, K Jezek, P Brada: Broken Promises - An Empirical Study into Evolution Problems in Java Programs Caused by Library Upgrades. Proceedings CSMR-WCRE'14. [DOI](http://dx.doi.org/10.1109/CSMR-WCRE.2014.6747226) [PREPRINT](https://sites.google.com/site/jensdietrich/publications/preprints)
5. S Raemaekers, A van Deursen, J Visser. Measuring software library stability
through historical version analysis. In Proceedings ICSM’12. [DOI](http://dx.doi.org/10.1109/ICSM.2012.6405296)
