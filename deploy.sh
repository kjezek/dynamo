#!/bin/sh

# Rebuild the project 
mvn install -Dmaven.test.skip

# Read current version
PROJECT_VERSION="$(mvn org.apache.maven.plugins:maven-help-plugin:2.1.1:evaluate -Dexpression=project.version | grep -v '\[')"

# Copy releases
cp dynamo-compiler/target/dynamo-compiler-$PROJECT_VERSION.jar case-study/jasper-reports-recompilable-problem/dynamo-compiler.jar
cp dynamo-compiler/target/dynamo-compiler-$PROJECT_VERSION.jar case-study/covariant-returntypes-and-bridgemethods/dynamo-compiler.jar
cp dynamo-enhancer/target/dynamo-enhancer-$PROJECT_VERSION.jar case-study/jasper-reports-recompilable-problem/dynamo-enhancer.jar 
cp dynamo-rt/target/dynamo-rt-$PROJECT_VERSION.jar case-study/jasper-reports-recompilable-problem/dynamo-rt.jar 
cp dynamo-rt/target/dynamo-rt-$PROJECT_VERSION.jar case-study/covariant-returntypes-and-bridgemethods/dynamo-rt.jar 
