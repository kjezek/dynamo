/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.enhancer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.dynamo.enhancer.filters.AbstractTargetFilter;
import org.dynamo.enhancer.filters.DefaultMethodFilter;
import org.dynamo.enhancer.filters.MethodFilter;
import org.dynamo.enhancer.filters.MethodFilterParser;
import org.dynamo.enhancer.testdata.SuperClass;
import org.dynamo.enhancer.testdata.TestMethodsInvocation;
import org.dynamo.enhancer.transformer.ClassInvocationTransformer;
import org.dynamo.enhancer.transformer.ClassTransformer;
import org.dynamo.tests.PublicClassLoader;
import org.dynamo.tests.TransformationTester;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.asm.Opcodes;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.dynamo.tests.TransformationTester.assertTransformation;
import static org.junit.Assert.assertEquals;

/**
 * Test transformation of invoke instruction.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class TransformationTest {

    /**
     * Path to the compiled classes.
     */
    private static final String INPUT_PATH = "target/test-classes/";

    /**
     * Path to the compiled classes.
     */
    private static final String OUTPUT_PATH = "target/generated-classes/";

    /**
     * Tested class package.
     */
    private static final String TESTED_CLASS_NAME = TestMethodsInvocation.class.getName().replace('.','/');

    /**
     * Tested class package.
     */
    private static final String TESTED_CLASS = TESTED_CLASS_NAME + ".class";

    /**
     * Tested transformer.
     */
    private ClassTransformer transformer = new ClassInvocationTransformer();

    /**
     * Output bytecode.
     */
    private byte[] output;

    /**
     * Input bytecode.
     */
    private byte[] input;

    /**
     * Init
     */
    @Before
    public void setUp() throws IOException {
        FileUtils.forceMkdir(new File(OUTPUT_PATH));
        FileUtils.forceMkdir(new File(INPUT_PATH));

        createDirs(OUTPUT_PATH);

        File inputFile = new File(INPUT_PATH, TESTED_CLASS);
        File outputFile = new File(OUTPUT_PATH, TESTED_CLASS);

        // transform
        input = IOUtils.toByteArray(inputFile.toURI());
        output = transformer.transform(TESTED_CLASS_NAME, input, MethodFilter.TRUE_FILTER);

        IOUtils.write(output, new FileOutputStream(outputFile));
    }

    /**
     * Test transformation works.
     */
    @Test
    public void testTransform() throws IOException {

        TransformationTester.Rules rules = new TransformationTester.Rules() {
            @Override
            public void rules(TransformationTester.RulesUpdater updater) {
                updater.addMethod(List.class.getName(), "add", Opcodes.INVOKEDYNAMIC);
                updater.addMethod(Integer.class.getName(), "parseInt", Opcodes.INVOKEDYNAMIC);
                updater.addMethod(ArrayList.class.getName(), "add", Opcodes.INVOKEDYNAMIC);
                updater.addMethod(IOUtils.class.getName(), "closeQuietly", Opcodes.INVOKEDYNAMIC);
                updater.addMethod(Integer.class.getName(), "parseInt", Opcodes.INVOKEDYNAMIC);
                updater.addMethod(LinkedList.class.getName(), "<init>", Opcodes.INVOKEDYNAMIC);
                updater.addMethod(ArrayList.class.getName(), "<init>", Opcodes.INVOKEDYNAMIC);
                updater.addMethod(LoggerFactory.class.getName(), "getLogger", Opcodes.INVOKEDYNAMIC);
                updater.addMethod(SuperClass.class.getName(), "superFoo", Opcodes.INVOKEDYNAMIC);
                updater.addMethod(TestMethodsInvocation.class.getName(), "superFoo", Opcodes.INVOKEDYNAMIC);
            }
        };

        assertTransformation(rules, output);
    }

    /**
     * Test transformation of only selected (filtered) methods.
     */
    @Test
    public void testTransformPart() {

        // this transformer includes only the ArrayList owner.
        ClassTransformer transformer = new ClassInvocationTransformer();

        MethodFilter filter = new AbstractTargetFilter() {
            @Override
            public boolean accepts(String className, String methodName, String descriptor) {
                return className.equals(ArrayList.class.getName().replace('.', '/'));
            }
        };

        byte[] output = transformer.transform(TESTED_CLASS_NAME, input, filter);

        // Only ArrayList is transformed.
        TransformationTester.Rules rules = new TransformationTester.Rules() {
            @Override
            public void rules(TransformationTester.RulesUpdater updater) {
                updater.addMethod(List.class.getName(), "add", Opcodes.INVOKEINTERFACE);
                updater.addMethod(Integer.class.getName(), "parseInt", Opcodes.INVOKESTATIC);
                updater.addMethod(LinkedList.class.getName(), "<init>", Opcodes.INVOKESPECIAL);
                updater.addMethod(IOUtils.class.getName(), "closeQuietly", Opcodes.INVOKESTATIC);
                updater.addMethod(LoggerFactory.class.getName(), "getLogger", Opcodes.INVOKESTATIC);
                updater.addMethod(SuperClass.class.getName(), "superFoo", Opcodes.INVOKESPECIAL);
                updater.addMethod(TestMethodsInvocation.class.getName(), "superFoo", Opcodes.INVOKEVIRTUAL);

                updater.addMethod(ArrayList.class.getName(), "add", Opcodes.INVOKEDYNAMIC);
                updater.addMethod(ArrayList.class.getName(), "<init>", Opcodes.INVOKEDYNAMIC);
            }
        };

        assertTransformation(rules, output);
    }

    /**
     * Test usage of expression filter.
     */
    @Test
    public void testTransformDefaultFilter() {
        MethodFilter filter = DefaultMethodFilter.INSTANCE;
        ClassTransformer transformer = new ClassInvocationTransformer();

        byte[] output = transformer.transform(TESTED_CLASS_NAME, input, filter);

        // Only 3rd calls are transformed.
        TransformationTester.Rules rules = new TransformationTester.Rules() {
            @Override
            public void rules(TransformationTester.RulesUpdater updater) {
                updater.addMethod(List.class.getName(), "add", Opcodes.INVOKEINTERFACE);
                updater.addMethod(ArrayList.class.getName(), "add", Opcodes.INVOKEVIRTUAL);
                updater.addMethod(Integer.class.getName(), "parseInt", Opcodes.INVOKESTATIC);
                updater.addMethod(LinkedList.class.getName(), "<init>", Opcodes.INVOKESPECIAL);
                updater.addMethod(ArrayList.class.getName(), "<init>", Opcodes.INVOKESPECIAL);

                updater.addMethod(IOUtils.class.getName(), "toByteArray", Opcodes.INVOKEDYNAMIC);
                updater.addMethod(IOUtils.class.getName(), "closeQuitely", Opcodes.INVOKEDYNAMIC);
                updater.addMethod(LoggerFactory.class.getName(), "getLogger", Opcodes.INVOKEDYNAMIC);
                updater.addMethod(SuperClass.class.getName(), "superFoo", Opcodes.INVOKEDYNAMIC);
                updater.addMethod(TestMethodsInvocation.class.getName(), "superFoo", Opcodes.INVOKEDYNAMIC);
            }
        };

        assertTransformation(rules, output);
    }

    /**
     * Test filtering of one method.
     */
    @Test
    public void testCallsiteFilter() {
        String className = TESTED_CLASS_NAME.replace('/', '.');
        MethodFilter filter = MethodFilterParser.parse(""
                        + "-callsite " + className + "#main*,"
                        + "+callsite *,"
                        + "+target *"  // TODO must explicitly say this, not convenient
        );

        ClassTransformer transformer = new ClassInvocationTransformer();

        byte[] output = transformer.transform(TESTED_CLASS_NAME, input, filter);

        // Only 3rd calls are transformed.
        TransformationTester.Rules rules = new TransformationTester.Rules() {
            @Override
            public void rules(TransformationTester.RulesUpdater updater) {
                updater.addMethod(IOUtils.class.getName(), "toByteArray", Opcodes.INVOKEDYNAMIC);
                updater.addMethod(IOUtils.class.getName(), "closeQuitely", Opcodes.INVOKESTATIC);
                updater.addMethod(LoggerFactory.class.getName(), "getLogger", Opcodes.INVOKESTATIC);
                updater.addMethod(SuperClass.class.getName(), "superFoo", Opcodes.INVOKEDYNAMIC);
                updater.addMethod(TestMethodsInvocation.class.getName(), "superFoo", Opcodes.INVOKEVIRTUAL);
            }
        };

        assertTransformation(rules, output);
    }

    /**
     * Test transformed class is correctly invoked.
     */
    @Test
    public void testInvokeTransformed() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        PublicClassLoader classLoader = new PublicClassLoader(getClass().getClassLoader());
        Class clazz = classLoader.defineClass(TESTED_CLASS_NAME.replace('/', '.'), output);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(stream));

        @SuppressWarnings("unchecked")
        Method method = clazz.getMethod("main", String[].class);
        method.invoke(clazz, new String[][]{new String[]{}});

        String actual = stream.toString().trim();
        assertEquals("[Hello from InvokeInterface]\n" +
                "[Hello from InvokeVirtual]\n" +
                "Hello from InvokeStatic: 666\n" +
                "[Hola]", actual);
    }

    /**
     * Create dirs.
     *
     * @param root root path
     */
    private void createDirs(String root) throws IOException {
        File rootFile = new File(root, TESTED_CLASS);

        String[] paths = rootFile.getPath().split("/");
        File file = new File(paths[0]);
        for (int i = 1; i < paths.length; i++) {
            FileUtils.forceMkdir(file);
            file = new File(file, paths[i]);
        }
    }

}
