/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.enhancer.testdata;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Contains invocation of methods.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class TestMethodsInvocation extends SuperClass {

    public static void main(String[] args) {

        IOUtils.closeQuietly((Reader) null);
        Logger logger = LoggerFactory.getLogger(TestMethodsInvocation.class);

        List<String> list = new LinkedList<>();
        list.add("Hello from InvokeInterface");
        System.out.println(list);


        ArrayList<String> arrayList = new ArrayList<>();
        // instruction invoke virtual will be here
        arrayList.add("Hello from InvokeVirtual");
        System.out.println(arrayList);

        int i = Integer.parseInt("666");
        System.out.println("Hello from InvokeStatic: " + i);

        new TestMethodsInvocation().superFoo(new ArrayList<>());
    }

    public static void another(String[] args) throws IOException {

        IOUtils.toByteArray((Reader) null);
    }

    @Override
    public void superFoo(List l) {
        l.add("Hola");
        super.superFoo(l);
    }
}
