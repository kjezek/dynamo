/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.enhancer.filter;

import org.dynamo.enhancer.filters.MethodFilter;
import org.dynamo.enhancer.filters.MethodFilterParser;
import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for method filter DSL/parser and the actual filter functionality.
 * @author jens dietrich
 */
public class MethodFilterTests {

    @Test public void test1() {
        String def = "+target *,-target java*";
        MethodFilter filter = MethodFilterParser.parse(def);

        // only excluded java packages, this should be ok
        assertTrue(filter.accepts(MethodFilter.Role.TARGET,"com.example.project","foo","()I"));
        assertFalse(filter.accepts(MethodFilter.Role.CALLSITE, "com.example.project", "foo", "()I"));
    }

    @Test public void test2() {
        String def = "+callsite *,-callsite java*";
        MethodFilter filter = MethodFilterParser.parse(def);

        // only excluded java packages, this should be ok
        assertFalse(filter.accepts(MethodFilter.Role.TARGET, "com.example.project", "foo", "()I"));
        assertTrue(filter.accepts(MethodFilter.Role.CALLSITE, "com.example.project", "foo", "()I"));
    }

    @Test public void test3() {
        String def = "+target com.example.*";
        MethodFilter filter = MethodFilterParser.parse(def);
        assertTrue(filter.accepts(MethodFilter.Role.TARGET, "com.example.project", "foo", "()I"));
    }

    @Test public void test4() {
        String def = "+target com.example.*#bar";
        MethodFilter filter = MethodFilterParser.parse(def);
        assertFalse(filter.accepts(MethodFilter.Role.TARGET, "com.example.project", "foo", "()I"));
    }

    @Test public void test5() {
        String def = "+target com.example.*#foo(I,J)LFoo;";
        MethodFilter filter = MethodFilterParser.parse(def);
        assertFalse(filter.accepts(MethodFilter.Role.TARGET, "com.example.project", "foo", "()I"));
    }

    @Test public void test6() {
        String def = "+target com.example.*#foo(I,J)LFoo;";
        MethodFilter filter = MethodFilterParser.parse(def);
        assertTrue(filter.accepts(MethodFilter.Role.TARGET, "com.example.project", "foo", "(I,J)LFoo;"));
    }


}