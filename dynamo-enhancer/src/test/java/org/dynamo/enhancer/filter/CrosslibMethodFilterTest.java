/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.enhancer.filter;

import org.dynamo.enhancer.filters.AndMethodFilter;
import org.dynamo.enhancer.filters.DefaultMethodFilter;
import org.dynamo.enhancer.filters.MethodFilter;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class CrosslibMethodFilterTest {

    @Test
    public void testJreFilter() throws IOException {
        MethodFilter filter = DefaultMethodFilter.INSTANCE;

        assertFalse(filter.accepts(MethodFilter.Role.TARGET, "java/lang/String", "", ""));
        assertTrue(filter.accepts(MethodFilter.Role.TARGET, "xxx/yyy/Foo", "", ""));
    }

    @Test
    public void testHierarchyFilters() throws IOException {
        MethodFilter parentFilter = new MethodFilter() {
            @Override
            public boolean accepts(Role role, String className, String methodName, String descriptor) {
                return !className.equals("xxx/yyy/Foo");
            }
        };

        MethodFilter filter = new AndMethodFilter(parentFilter, DefaultMethodFilter.INSTANCE);

        assertFalse(filter.accepts(MethodFilter.Role.TARGET, "java/lang/String", "", ""));
        assertFalse(filter.accepts(MethodFilter.Role.TARGET, "xxx/yyy/Foo", "", ""));
        assertTrue(filter.accepts(MethodFilter.Role.TARGET, "xxx/yyy/zz/Foo", "", ""));
    }
}
