grammar dynamo;

kind : '+'|'-';
role : 'callsite'|'target';
filters:filter(','filter)*;
className:NAME;
methodName:NAME;
descriptor:'(' NAMES ')' NAME ;
filter : kind role className('#'methodName(descriptor)?)?;

NAME  :	('a'..'z'|'A'..'Z'|'_'|'*'|'?') ('a'..'z'|'A'..'Z'|'0'..'9'|'_'|'.'|'*'|'?'|'/'|';')*;
NAMES  : (NAME (','NAME)*)+ ;
NL : '\r'? '\n' ;
WS    : [ \t\r\n]+ -> skip ;