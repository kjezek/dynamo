/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.enhancer.filters;


import java.util.Arrays;
import java.util.Collection;

/**
 * Simple AND method filter.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class AndMethodFilter implements MethodFilter {

    /** Filters to AND. */
    private Collection<MethodFilter> filters;

    /**
     * Filters to AND.
     * @param filters filters
     */
    public AndMethodFilter(final MethodFilter... filters) {
        this.filters = Arrays.asList(filters);
    }


    @Override
    public boolean accepts(final Role role, final String className, final String methodName, final String descriptor) {
        boolean r = true;

        for (MethodFilter filter : filters) {
            r &= filter.accepts(role, className, methodName, descriptor);
        }

        return r;
    }
}
