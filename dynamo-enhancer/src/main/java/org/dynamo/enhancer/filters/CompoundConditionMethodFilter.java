/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.enhancer.filters;

import java.util.ArrayList;
import java.util.List;

/**
 * Method filter composed from other simple conditions.
 * There might be conflicting includes and excludes, we resolved this as follows:
 * Records are accepted if they match at least one of the include patterns and don't match any of the exclude patterns.
 * This is consistent how other systems that have to deal with includes and excludes, like ANT, deal with this.
 *
 * @author jens dietrich
 */
public class CompoundConditionMethodFilter implements MethodFilter {

    /** Compound filters. */
    private List<SingleConditionMethodFilter> filters = new ArrayList<>(5);

    /**
     * Add a filter.
     * @param filter a filter
     */
    public void addFilter(final SingleConditionMethodFilter filter) {
        filters.add(filter);
    }

    @Override
    public boolean accepts(
            final Role role,
            final String className,
            final String methodName,
            final String descriptor) {

        boolean accept = false;

        // to be accepted, inputs must match at least one include pattern
        for (SingleConditionMethodFilter f:filters) {
            if (f.getRole() == role && f.getKind() == Kind.INCLUDE) {
                accept = accept || f.accepts(role, className, methodName, descriptor);
            }
        }
        // and not match any exclude pattern
        for (SingleConditionMethodFilter f:filters) {
            if (f.getRole() == role && f.getKind() == Kind.EXCLUDE) {
                accept = accept && !f.accepts(role, className, methodName, descriptor);
            }
        }
        return accept;

    }
}
