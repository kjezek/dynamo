/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.enhancer.filters;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.dynamo.enhancer.filters.antlrgenerated.dynamoLexer;
import org.dynamo.enhancer.filters.antlrgenerated.dynamoParser;


/**
 * Parser for the filter DSL.
 * @author jens dietrich
 */
public final class MethodFilterParser {

    /**
     * Parse an input filter.
     * @param s filter in the string form
     * @return a method filter.
     */
    public static MethodFilter parse(final String s) {

        CompoundConditionMethodFilter result = new CompoundConditionMethodFilter();

        ANTLRInputStream reader = new ANTLRInputStream(s);
        dynamoLexer lexer = new dynamoLexer(reader);
        dynamoParser parser = new dynamoParser(new CommonTokenStream(lexer));
        dynamoParser.FiltersContext _filters = parser.filters();

        for (dynamoParser.FilterContext _filter:_filters.filter()) {

            dynamoParser.KindContext _kind = _filter.kind();
            MethodFilter.Kind kind = "+".equals(_kind.getText()) ? MethodFilter.Kind.INCLUDE : MethodFilter.Kind.EXCLUDE;
//            System.out.println("kind: " + kind);

            dynamoParser.RoleContext _role = _filter.role();
            MethodFilter.Role role = "callsite".equals(_role.getText()) ? MethodFilter.Role.CALLSITE : MethodFilter.Role.TARGET;
//            System.out.println("role: " + role);

            dynamoParser.ClassNameContext _class = _filter.className();
            String classPattern =  _class == null ? null : _class.getText();
//            System.out.println("class pattern: " + classPattern);

            dynamoParser.MethodNameContext _method = _filter.methodName();
            String methodPattern =  _method == null ? null : _method.getText();
//            System.out.println("method pattern: " + methodPattern);

            dynamoParser.DescriptorContext _descriptor = _filter.descriptor();
            String descriptorPattern =  _descriptor==null?null:_descriptor.getText();
//            System.out.println("descriptor pattern: " + descriptorPattern);

            SingleConditionMethodFilter filter = new SingleConditionMethodFilter(
                    kind,
                    role,
                    classPattern,
                    methodPattern,
                    descriptorPattern
            );

            result.addFilter(filter);
        }

        return result;
    }

}
