/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.enhancer.filters;

import java.util.regex.Pattern;

/**
 * Method filter built from a single condition, instantiated using the filter DSL.
 * @author jens dietrich
 */
public class SingleConditionMethodFilter implements MethodFilter {

    private Kind kind = Kind.INCLUDE;
    private Role role = Role.TARGET;
    private String classNamePattern = null;
    private String methodNamePattern = null;
    private String descriptorPattern = null;

    private Pattern _classNamePattern = null;
    private Pattern _methodNamePattern = null;
    private Pattern _descriptorPattern = null;

    // don't use lambdas, we are still JDK 7 compatible
    private static final Pattern TRUE_FILTER = Pattern.compile(".*");

    public SingleConditionMethodFilter(Kind kind, Role role, String classNamePattern, String methodNamePattern, String descriptorPattern) {
        super();
        this.kind = kind;
        this.role = role;
        this.classNamePattern = classNamePattern;
        this.methodNamePattern = methodNamePattern;
        this.descriptorPattern = descriptorPattern;

        // init reg expr
        _classNamePattern = classNamePattern==null? TRUE_FILTER :Pattern.compile(wildcard2Regex(classNamePattern));
        _methodNamePattern = methodNamePattern==null? TRUE_FILTER :Pattern.compile(wildcard2Regex(methodNamePattern));
        _descriptorPattern = descriptorPattern==null? TRUE_FILTER :Pattern.compile(wildcard2Regex(descriptorPattern));
    }

    @Override
    public boolean accepts(Role role,String className, String methodName, String descriptor) {
        if (role!=this.role) return false;
        String classNameJavaPath = className.replace('/', '.');
        return _classNamePattern.matcher(classNameJavaPath).matches()
                && _methodNamePattern.matcher(methodName).matches()
                && _descriptorPattern.matcher(descriptor).matches();
    }

    public Kind getKind() {
        return kind;
    }

    public Role getRole() {
        return role;
    }

    public static String wildcard2Regex(String s){
        StringBuilder b = new StringBuilder(s.length());
        b.append('^');
        for (int i = 0, is = s.length(); i < is; i++) {
            char c = s.charAt(i);
            switch (c) {
                case '*':
                    b.append(".*");
                    break;
                case '?':
                    b.append(".");
                    break;
                // escape special chars
                case '(': case ')': case '[': case ']': case '$':
                case '^': case '.': case '{': case '}': case '|':
                case '\\':
                    b.append("\\");
                    b.append(c);
                    break;
                default:
                    b.append(c);
                    break;
            }
        }
        b.append('$');
        return b.toString();
    }

    @Override
    public String toString() {
        return "SingleConditionMethodFilter{" +
                "descriptorPattern='" + descriptorPattern + '\'' +
                ", methodNamePattern='" + methodNamePattern + '\'' +
                ", classNamePattern='" + classNamePattern + '\'' +
                ", role=" + role +
                ", kind=" + kind +
                '}';
    }
}
