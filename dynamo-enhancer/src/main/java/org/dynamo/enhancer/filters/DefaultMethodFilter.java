/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.enhancer.filters;

/**
 * Default filter - blocks all targets in the JDK classes.
 * Singleton.
 * @author jens dietrich
 */
public final class DefaultMethodFilter extends AbstractTargetFilter {

    /** Singleton instance.  */
    public static final MethodFilter INSTANCE = new DefaultMethodFilter();

    @Override
    public boolean accepts(final String className, final String methodName, final String descriptor) {

        return  !className.startsWith("java/")
                && !className.startsWith("javax/")
                && !className.startsWith("sun/")
                && !className.startsWith("com/sun/")
                && !className.startsWith("com.oracle.")
                && !className.startsWith("org/ietf/")
                && !className.startsWith("org/omg/")
                && !className.startsWith("org/w3c/")
                && !className.startsWith("org/xml/");
    }
}
