/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.enhancer.filters;

/**
 * Method filter interface.
 * @author Kamil Jezek [kjezek@kiv.zcu.cz] , jens dietrich
 */
public interface MethodFilter {

    /** Filter kind. */
    enum Kind {
        /** Filter interpreted as included one.  */
        INCLUDE,

        /** Filter interpreted as excluded one. */
        EXCLUDE
    }

    /** Filters role. */
    enum Role {
        /** Filter must be applied to the call site (callee). */
        CALLSITE,

        /** Filter must be applied to the target site (a method call). */
        TARGET
    }

    /** Always true filter. */
    MethodFilter TRUE_FILTER = new MethodFilter() {
        @Override
        public boolean accepts(
                final Role role,
                final String className,
                final String methodName,
                final String descriptor) {

            return true;
        }
    };

    /**
     * This method returns true if the input method should be included.
     * @param role the role.
     * @param className owner.
     * @param methodName name.
     * @param descriptor descriptor (as specified in the JVM spec).
     * @return true or false
     */
    boolean accepts(Role role, String className, String methodName, String descriptor);


}
