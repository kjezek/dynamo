/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.enhancer.filters;

/**
 * This filter proceeds with queries to the target calls.
 * Other queries will be automatically accepted.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public abstract class AbstractTargetFilter implements MethodFilter {


    @Override
    public final boolean accepts(
            final Role role,
            final String className,
            final String methodName,
            final String descriptor) {

        // crosscall checks are performed only for the target ROLE.
        // it is not meaningfull to check it for CALLSITE
        return role != Role.TARGET || accepts(className, methodName, descriptor);
    }


    /**
     * This method returns true if the input method should be included.
     * @param className owner.
     * @param methodName name.
     * @param descriptor descriptor (as specified in the JVM spec).
     * @return true or false
     */
    protected abstract boolean accepts(
            final String className,
            final String methodName,
            final String descriptor);
}
