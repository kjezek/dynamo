/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.enhancer.filters;

import org.dynamo.enhancer.readers.DataSourceReader;
import org.dynamo.enhancer.readers.JarFileReader;
import org.dynamo.enhancer.readers.ResourceStreamVisitor;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

/**
 * This filter includes only methods that come from
 * other libraries, i.e. cross library invocations.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class CrossJarMethodFilter extends AbstractTargetFilter {

    /**
     * List of existing classes.
     */
    private Set<String> existingClasses = new HashSet<>();

    /**
     * Load data from input JAR files.
     *
     * @param files input JAR files.
     */
    public CrossJarMethodFilter(final File... files)  {

        // collect classes stored in input JAR files.
        for (File file : files) {
            DataSourceReader reader = new JarFileReader(file);
            try {
                reader.loadResources(new ResourceStreamVisitor() {
                    @Override
                    public void visitClassStream(
                            final String fullClassName,
                            final String sourcePath,
                            final InputStream stream) throws IOException {

                        existingClasses.add(fullClassName);
                    }

                    @Override
                    public void visitAnotherResource(
                            final String name,
                            final InputStream stream) throws IOException {
                        // empty
                    }
                });
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public boolean accepts(
            final String className,
            final String methodName,
            final String descriptor) {

        return !existingClasses.contains(className);
    }

}
