/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.enhancer;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public final class BootstrapConstants {

    /** Private. */
    private BootstrapConstants() { }

    /** Name of bootstrap class. */
    public static final String BOOTSTRAP_CLASS = "org/dynamo/rt/DynamoBootstrap";

    /** Bootstrap methods. */
    public static final String BOOTSTRAP_VIRTUAL_METHOD = "bootstrapVirtual";

    /** Bootstrap methods. */
    public static final String BOOTSTRAP_INTERFACE_METHOD = "bootstrapInterface";

    /** Bootstrap methods. */
    public static final String BOOTSTRAP_STATIC_METHOD = "bootstrapStatic";

    /** Bootstrap methods. */
    public static final String BOOTSTRAP_SPECIAL_METHOD_NEW = "bootstrapSpecialNew";

    /** Bootstrap methods. */
    public static final String BOOTSTRAP_SPECIAL_METHOD_SUPER = "bootstrapSpecialSuper";

    /** Replacement for the constructor name.
     * Default <init> cannot be used as it does not pass by the byte-code verification. */
    public static final String CONSTRUCTOR_DYNAMIC_NAME = "C$D";

}
