/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.enhancer.writers;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public interface DataSourceWriter extends Closeable {

    /**
     * Add resource to the data source.
     *
     * @param name resource name including full path
     * @param stream stream with data.
     * @exception IOException IO Error
     */
    void addResource(String name, InputStream stream) throws IOException;

    /**
     * Finish writing.
     */
    void close();
}
