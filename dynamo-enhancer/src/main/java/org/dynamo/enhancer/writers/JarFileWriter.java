/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.enhancer.writers;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;

/**
 * Write data into JAR file.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class JarFileWriter implements DataSourceWriter {

    /** Output jarOutputStream. */
    private JarOutputStream jarOutputStream;

    /**
     * @param jarFile the jar to write data into.
     * @exception IOException io exception
     */
    public JarFileWriter(final File jarFile) throws IOException {
        /* A jar file to write into. */
        this.jarOutputStream = new JarOutputStream(new FileOutputStream(jarFile));
    }

    @Override
    public void addResource(final String name, final InputStream stream) throws IOException {

        String unixPath = FilenameUtils.separatorsToUnix(name);
        JarEntry newEntry = new JarEntry(unixPath);
        jarOutputStream.putNextEntry(newEntry);
        IOUtils.copy(stream, jarOutputStream);
    }

    @Override
    public void close() {
        IOUtils.closeQuietly(jarOutputStream);
    }
}
