/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.enhancer.transformer;

import org.dynamo.enhancer.filters.MethodFilter;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public interface ClassTransformer {

    /**
     * Transform bytes.
     *
     * @param className an original class name.
     * @param bytecode input bytes.
     * @param methodFilter a method filter
     * @return output bytes.
     */
    byte[] transform(String className, byte[] bytecode, MethodFilter methodFilter);

}
