/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.enhancer.transformer;

import org.dynamo.enhancer.BootstrapConstants;
import org.dynamo.enhancer.filters.MethodFilter;
import org.objectweb.asm.Handle;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.CallSite;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class AsmMethodVisitor extends MethodVisitor {

    /** Logger. */
    private Logger logger = LoggerFactory.getLogger(getClass());

    /** Filter of included methods. */
    private MethodFilter methodFilter;

    /**
     * A flag saying that the new constructor invocation will be replaced.
     *
     */
    private boolean replaceNextConstr = false;

    /**
     * A flag saying that the next dup instruction should be skipped.
     *
     */
    private boolean replaceNextDup = false;

    /** Empty classes. */
    private static final Class<?>[] EMPTY_CLASSES = new Class<?>[0];

    /** Empty data. */
    private static final Object[] EMPTY_DATA = new Object[0];

    /**
     * Init.
     * @param mv method visitor.
     * @param methodFilter method filter.
     */
    public AsmMethodVisitor(
            final MethodVisitor mv,
            final MethodFilter methodFilter) {
        super(Opcodes.ASM5, mv);

        this.methodFilter = methodFilter;
    }


    @Override
    public void visitTypeInsn(final int opcode, final String type) {

        // if a new object is to be created, check if its creation will be later replaced by method
        // dynamic invocation.
        if (Opcodes.NEW == opcode && methodFilter.accepts(MethodFilter.Role.TARGET, type, "", "")) {
            // skip this instruction
            replaceNextDup = true;
        } else {
            super.visitTypeInsn(opcode, type);
        }
    }

    @Override
    public void visitInsn(final int opcode) {

        // instruction NEW has been skipped. Skip also DUP here
        // They will be replaced by invokedynamic.
        if (Opcodes.DUP == opcode && replaceNextDup) {
            replaceNextConstr = true;
            replaceNextDup = false;
        } else {
            super.visitInsn(opcode);
        }
    }



    @Override
    public void visitMethodInsn(
            final int opcode,
            final String owner,
            final String name,
            final String desc,
            final boolean itf) {

        logger.trace("Method call instruction {}#{}", owner, name);

        if (methodFilter.accepts(MethodFilter.Role.TARGET, owner, name, desc)) {
            logger.trace("Processing method call instruction {}({})", name, desc);

            switch (opcode) {
                case Opcodes.INVOKEVIRTUAL:
                    invokeDynamicVirtualMethod(owner, name, desc);
                    break;
                case Opcodes.INVOKEINTERFACE:
                    invokeDynamicInterfaceMethod(owner, name, desc);
                    break;
                case Opcodes.INVOKESTATIC:
                    invokeDynamicStaticMethod(owner, name, desc);
                    break;
                case Opcodes.INVOKESPECIAL:
                    // replacement of a constructor
                    if (replaceNextConstr && "<init>".equals(name)) {
                        invokeDynamicConstructor(owner, desc);
                        replaceNextConstr = false;
                    } else if (!"<init>".equals(name)) {
                        // replacement of super methods
                        // they cannot be private ones as they would not pass the filter for "outside" only classes
                        invokeDynamicSpecialSuper(owner, name, desc);
                    } else {
                        proceedNoChange(opcode, owner, name, desc, itf);
                    }
                    break;
                default:
                    proceedNoChange(opcode, owner, name, desc, itf);
                    break;
            }
        } else {
            proceedNoChange(opcode, owner, name, desc, itf);
        }
    }

    /**
     * Proceed without change.
     * @param opcode opcode
     * @param owner owner
     * @param name name
     * @param desc desc
     * @param itf iff
     */
    private void proceedNoChange(
            final int opcode,
            final String owner,
            final String name,
            final String desc,
            final boolean itf) {

        logger.trace("Skipping method call instruction {}({})", name, desc);
        super.visitMethodInsn(opcode, owner, name, desc, itf);

    }


    /**
     * Create invoke dynamic instruction.
     *
     * @param owner original method owner
     * @param name original method name
     * @param desc original method desc
     */
    private void invokeDynamicStaticMethod(final String owner, final String name, final String desc) {

        logger.trace("{}#{}({}) changed to invokedynamic. ", new Object[] {owner, name, desc});

        invokedynamicTemplate(desc, name,
                BootstrapConstants.BOOTSTRAP_STATIC_METHOD,
                new Class<?>[] {String.class}, new Object[] {owner});
    }

    /**
     * Create invoke dynamic instruction.
     *
     * @param owner original method owner
     * @param name original method name
     * @param desc original method desc
     */
    private void invokeDynamicInterfaceMethod(final String owner, final String name, final String desc) {

        logger.trace("{}#{}({}) changed to invokedynamic. ", new Object[] {owner, name, desc});

        String newDesc = addOwnerAsFirstParam(owner, desc);
        invokedynamicTemplate(newDesc, name,
                BootstrapConstants.BOOTSTRAP_INTERFACE_METHOD,
                EMPTY_CLASSES, EMPTY_DATA);
    }


    /**
     * Create invoke dynamic instruction.
     *
     * @param owner original method owner
     * @param name original method name
     * @param desc original method desc
     */
    private void invokeDynamicVirtualMethod(final String owner, final String name, final String desc) {

        logger.trace("{}#{}({}) changed to invokedynamic. ", new Object[] {owner, name, desc});

        String newDesc = addOwnerAsFirstParam(owner, desc);
        invokedynamicTemplate(newDesc, name,
                BootstrapConstants.BOOTSTRAP_VIRTUAL_METHOD,
                EMPTY_CLASSES, EMPTY_DATA);
    }


    /**
     * Create invoke dynamic instruction.
     *
     * @param owner original method owner
     * @param name original method name
     * @param desc original method desc
     */
    private void invokeDynamicSpecialSuper(final String owner, final String name, final String desc) {

        logger.trace("{}#{}({}) changed to invokedynamic. ", new Object[] {owner, name, desc});

        String newDesc = addOwnerAsFirstParam(owner, desc);
        invokedynamicTemplate(newDesc, name,
                BootstrapConstants.BOOTSTRAP_SPECIAL_METHOD_SUPER,
                EMPTY_CLASSES, EMPTY_DATA);
    }


    /**
     * Create invoke dynamic instruction.
     *
     * @param owner original method owner
     * @param desc original method desc
     */
    private void invokeDynamicConstructor(final String owner, final String desc) {

        logger.trace("{}#<init>({}) changed to invokedynamic. ", new Object[] {owner, desc});

        // replace return type (Void) by owner class.
        // e.g. (xyz;abc)V  --> (xyz;abc)Ljava/util/ArrayList
        String newDesc = desc.substring(0, desc.length() - 1) + "L" + owner + ";";

        invokedynamicTemplate(newDesc,
                BootstrapConstants.CONSTRUCTOR_DYNAMIC_NAME,
                BootstrapConstants.BOOTSTRAP_SPECIAL_METHOD_NEW,
                EMPTY_CLASSES, EMPTY_DATA);
    }

    /**
     * Change the input description so that the first method parameter
     * will be the owner class.
     *
     * @param owner owner
     * @param desc original description
     * @return new description
     */
    public static String addOwnerAsFirstParam(final String owner, final String desc) {
        int index = desc.indexOf('(') + 1;
        // add owner to the first argument
        // e.g.   (Ljava/lang/String)V  becomes (Lxx/yy/MyClass;Ljava/lang/String)V
        return desc.substring(0, index) + "L" + owner + ";" + desc.substring(index);
    }

    /**
     * Common template for onvokedynamic.
     * @param desc description.
     * @param targetMethodName target method name
     * @param bootstrapMethodName Target bootstrap method name.
     * @param additionalParams additional parameters data.
     * @param  additionalParamsType additional parameters
     */
    private void invokedynamicTemplate(
            final String desc,
            final String targetMethodName,
            final String bootstrapMethodName,
            final Class<?>[] additionalParamsType,
            final Object[] additionalParams) {

        List<Class<?>> paramsTypes = new ArrayList<>(Arrays.asList(
                MethodHandles.Lookup.class,
                String.class,
                MethodType.class
        ));

        if (additionalParamsType != null) {
            paramsTypes.addAll(Arrays.asList(additionalParamsType));
        }

        MethodType mt = MethodType.methodType(CallSite.class, paramsTypes);

        Handle bootstrap = new Handle(
                Opcodes.H_INVOKESTATIC,
                BootstrapConstants.BOOTSTRAP_CLASS,
                bootstrapMethodName,
                mt.toMethodDescriptorString());

        mv.visitInvokeDynamicInsn(targetMethodName, desc, bootstrap, additionalParams);
    }

}
