/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.enhancer.transformer;

import org.dynamo.enhancer.filters.MethodFilter;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class ClassInvocationTransformer implements ClassTransformer {


    /** Logger. */
    private Logger logger = LoggerFactory.getLogger(getClass());


    /** Java 7 byte-code version. */
    private static final int JAVA7_BYTECODE_VERSION = 0x33; // 51 -> Java 7

    /** Mask to fetch two bytes. */
    private static final int TWO_BYTES_MASK = 0xff;


    @Override
    public byte[] transform(final String className, final byte[] bytecode, final MethodFilter methodFilter) {
        final ClassReader classReader = new ClassReader(bytecode);
        // TODO - Compute Frames invokes reflection so we cannot use it here
        // TODO - but we should not need to recompute frames at all as we only swap instructions, not variables,
        // TODO - is the solution with "0" right?
//        http://stackoverflow.com/questions/15122890/java-lang-verifyerror-expecting-a-stackmap-frame-at-branch-target-jdk-1-7
//        http://chrononsystems.com/blog/java-7-design-flaw-leads-to-huge-backward-step-for-the-jvm
//        http://stackoverflow.com/questions/19381417/compile-error-jsr-ret-are-not-supported-with-computeframes-option
//        final ClassWriter cw = new ClassWriter(classReader,  ClassWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_MAXS);
        final ClassWriter cw = new ClassWriter(classReader,  0);

        classReader.accept(new ClassVisitor(Opcodes.ASM5, cw) {
            @Override
            public MethodVisitor visitMethod(
                    final int access,
                    final String name,
                    final String desc,
                    final String signature,
                    final String[] exceptions) {

                MethodVisitor parentVisitor = super.visitMethod(access, name, desc, signature, exceptions);

                MethodVisitor resultVisitor = parentVisitor;
                if (methodFilter.accepts(MethodFilter.Role.CALLSITE, className, name, desc)) {
                    resultVisitor = new AsmMethodVisitor(parentVisitor, methodFilter);
                }

                return resultVisitor;
            }

            @Override
            public void visit(
                    final int version,
                    final int access,
                    final String name,
                    final String signature,
                    final String superName,
                    final String[] interfaces) {

                int updateVersion = version;

                logger.trace("Analysing class: {}", name);

                // TODO -- this is a very coarse solution to change only the byte-code version in the header
                // the output byte-code will not likely pass the verification phase
                // Workaround for now is to run the output byte-code with the '-noverify' JVM parameter.
                // TODO -- byte-code must be modified to properly fit the Java7 rules.

                // new byte-code must be at least Java 1.7
                if ((version & TWO_BYTES_MASK) < JAVA7_BYTECODE_VERSION) {
                    updateVersion = JAVA7_BYTECODE_VERSION;

                    logger.trace("Updating byte-code version. Final byte-code will not pass verification.");
                }

                super.visit(updateVersion, access, name, signature, superName, interfaces);
            }

        }, ClassReader.EXPAND_FRAMES);

        return cw.toByteArray();
    }
}
