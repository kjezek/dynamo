/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.enhancer.utils;


import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * A utility class allowing to load also JRE byte-code.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public final class JreLoader
{

    /** Private.  */
    private JreLoader() { }

    /**
     *
     * @return JAVA_HOME property value
     */
    public static String getJavaHome()
    {
        Map<String, String> env = System.getenv();
        String targetJDK = env.get("TARGET_JDK_HOME");

        if (targetJDK == null) {
            targetJDK = env.get("JAVA_HOME");
            if (targetJDK == null) {
                throw new RuntimeException("Either set TARGET_JDK_HOME or evaluation"
                        + " property.");
            }
        }

        return targetJDK;
    }

    /**
     * Get JRE jars.
     * @param javaHome java home
     * @return files.
     */
    public static File[] getJreJars(final String javaHome)
    {
        Set<File> jars = new HashSet<>();

        Iterator<File> it = FileUtils.iterateFiles(new File(javaHome), new String[] {"jar"}, true);
        while (it.hasNext()) {
            jars.add(it.next());
        }

        return jars.toArray(new File[jars.size()]);
    }

}
