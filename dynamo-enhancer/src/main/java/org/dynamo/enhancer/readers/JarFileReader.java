/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */
package org.dynamo.enhancer.readers;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;


/**
 * A loader loading Java classes from a JAR file.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public class JarFileReader implements DataSourceReader
{

    /** Logger. */
    private Logger logger = LoggerFactory.getLogger(getClass());

    /** A jar file to be opened. */
    private File jarFile;

    /**
     * @param jarFile the jar file to load.
     */
    public JarFileReader(final File jarFile)
    {
        this.jarFile = jarFile;
    }

    /** {@inheritDoc} */
    @Override
    public void loadResources(final ResourceStreamVisitor visitor) throws IOException
    {
        logger.debug("Loading resources. File: {}", jarFile);

        ZipFile zipFile = null;

        try {

            zipFile = new ZipFile(jarFile);
            ZipEntry zipEntry;

            Enumeration<? extends ZipEntry> entries = zipFile.entries();

            while (entries.hasMoreElements()) {
                zipEntry = entries.nextElement();

                String entryName = zipEntry.getName();
                File entryFile = new File(entryName);

                logger.trace("Found entry: {}", entryName);

                if (isClassFile(entryFile, entryName)) {
                    visitClassStream(zipEntry, entryName, zipFile, visitor);
                } else {
                    visitAnotherResource(zipEntry, entryName, zipFile, visitor);
                }

            }

        } finally {
            IOUtils.closeQuietly(zipFile);
        }
    }

    /**
     * Visit other than .class resources.
     * @param zipEntry zip entry
     * @param entryName entry name
     * @param zipFile zip file
     * @param visitor visitor
     * @throws IOException IO error.
     */
    private void visitAnotherResource(
            final ZipEntry zipEntry,
            final String entryName,
            final ZipFile zipFile,
            final ResourceStreamVisitor visitor) throws IOException {

        InputStream in = new BufferedInputStream(zipFile.getInputStream(zipEntry));
        try {
            visitor.visitAnotherResource(entryName, in);
        } finally {
            IOUtils.closeQuietly(in);
        }
    }

    /**
     * Return true if the given entry should be included.
     * @param entryFile file
     * @param entryName name
     * @return true to include
     */
    private boolean isClassFile(
            final File entryFile,
            final String entryName) {

        return !entryFile.isDirectory() && entryName.endsWith(".class");
    }

    /**
     * visit stream.
     * @param zipEntry zip entry
     * @param entryName entry name
     * @param zipFile zip file
     * @param visitor visitor
     * @throws IOException IO error.
     */
    private void visitClassStream(
            final ZipEntry zipEntry,
            final String entryName,
            final ZipFile zipFile,
            final ResourceStreamVisitor visitor) throws IOException {

        InputStream in = new BufferedInputStream(zipFile.getInputStream(zipEntry));
        try {
            visitor.visitClassStream(entryName, jarFile.getPath(), in);
        } finally {
            IOUtils.closeQuietly(in);
        }

    }

}
