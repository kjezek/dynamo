/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */
package org.dynamo.enhancer.readers;

import java.io.IOException;


/**
 * This loader is used for loading java classes from several
 * type of locations. Example locations may be a JAR file, a WAR file,
 * exploded JAR (directory).
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface DataSourceReader
{


    /**
     * This method loads all classes from a destination
     * invoking the input visitor for each class.
     *
     * The destination may be e.g. JAR file, a WAR file or a directory.
     *
     *
     * @param visitor the visitor invoked for each class.
     * @throws IOException error to read the data.
     */
    void loadResources(ResourceStreamVisitor visitor) throws IOException;


}
