/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */
package org.dynamo.enhancer.readers;

import java.io.IOException;
import java.io.InputStream;

/**
 * This visitor should be invoked every time a stream
 * containing a Java resource is opened.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 */
public interface ResourceStreamVisitor
{

    /**
     * This method is invoked every time a stream with java class
     * content is opened.
     *
     *
     * @param fullClassName a full class name
     * @param stream the stream
     * @param sourcePath a path where the byte-code class comes from
     * @exception IOException error to read the stream
     */
    void visitClassStream(String fullClassName, String sourcePath, InputStream stream) throws IOException;

    /**
     * This method is invoked for other resources than .class files.
     *
     * @param name resource ame
     * @param stream stream
     * @throws IOException IO error.
     */
    void visitAnotherResource(String name, InputStream stream) throws IOException;
}
