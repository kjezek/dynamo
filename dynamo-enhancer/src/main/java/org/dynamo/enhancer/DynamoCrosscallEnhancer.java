/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.enhancer;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FilenameUtils;
import org.dynamo.enhancer.filters.DefaultMethodFilter;
import org.dynamo.enhancer.filters.MethodFilter;
import org.dynamo.enhancer.filters.MethodFilterParser;
import org.dynamo.enhancer.services.DataTransformer;
import org.dynamo.enhancer.services.DataTransformerImpl;
import org.dynamo.enhancer.transformer.ClassInvocationTransformer;
import org.dynamo.enhancer.transformer.ClassTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.List;

/**
 * Main class to transform byte-code to use dynamic invocation.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public final class DynamoCrosscallEnhancer {

    /** Logger. */
    private static Logger logger = LoggerFactory.getLogger(DynamoCrosscallEnhancer.class);

    /** Transformer. */
    private static ClassTransformer transformer = new ClassInvocationTransformer();

    /** Transformer. */
    private static DataTransformer dataTransformer = new DataTransformerImpl(transformer);


    /**
     * private.
     */
    private DynamoCrosscallEnhancer() {
    }

    /**
     * Transform input byte-cod (usually stored in JAR files)
     * to replace invoke static, interface and virtual instructions
     * by invoke dynamic.
     *
     * @param args args.
     * @throws ParseException error to parse command line.
     * @throws IOException    IO error.
     */
    public static void main(final String[] args) throws ParseException, IOException {
        Options options = new Options();
        options.addOption("filter", true, "Specify filter of included/excluded method calls to enhance.");

        CommandLineParser cmdParser = new DefaultParser();
        CommandLine line = cmdParser.parse(options, args);

        List<String> inputOutput = line.getArgList();

        if (inputOutput.size() < 2) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("dynamo-enhancer [input file] [output file]", options);

            System.exit(-1);
        }

        MethodFilter userFilter;
        String filter = line.getOptionValue("filter");
        if (filter != null) {
            userFilter = MethodFilterParser.parse(filter);
        } else {
            userFilter = DefaultMethodFilter.INSTANCE;
        }

        String inputFile = inputOutput.get(0);
        String outputFile = inputOutput.get(1);

        boolean batch = new File(inputFile).isDirectory();

        logger.info("Processing file. {}", inputFile);

        File[] inputFiles;

        if (batch) {
            inputFiles = new File(inputFile).listFiles(new FilenameFilter() {
                @Override
                public boolean accept(final File dir, final String name) {
                    return name.toLowerCase().endsWith(".jar");
                }
            });
        } else {
            inputFiles = new File[] {new File(inputFile)};
        }

        for (File currentInputFile : inputFiles) {

            File currentOutputFile;
            if (batch) {
                currentOutputFile = new File(outputFile, currentInputFile.getName());
            } else {
                currentOutputFile = new File(addJarFileName(outputFile, inputFile));
            }

            dataTransformer.transformJarFile(currentInputFile, currentOutputFile, userFilter);
        }
    }

    /**
     * If the output path does not ends with the jar file name, treat it as a directory.
     *
     * @param outputFile output file
     * @param inputFile  input file.
     * @return output path with a file taken from the input path, if the output path is a directory
     */
    private static String addJarFileName(final String outputFile, final String inputFile) {

        String result = outputFile;

        if (!outputFile.toLowerCase().endsWith(".jar")) {
            String unixInputPath = FilenameUtils.separatorsToUnix(inputFile);

            result = new File(outputFile,
                    unixInputPath.substring(unixInputPath.lastIndexOf('/'), unixInputPath.length())
            ).getPath();

        }

        return result;
    }
}
