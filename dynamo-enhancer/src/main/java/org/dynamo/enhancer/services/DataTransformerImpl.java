/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.enhancer.services;

import org.apache.commons.io.IOUtils;
import org.dynamo.enhancer.filters.AndMethodFilter;
import org.dynamo.enhancer.filters.CrossJarMethodFilter;
import org.dynamo.enhancer.filters.MethodFilter;
import org.dynamo.enhancer.readers.DataSourceReader;
import org.dynamo.enhancer.readers.JarFileReader;
import org.dynamo.enhancer.readers.ResourceStreamVisitor;
import org.dynamo.enhancer.transformer.ClassTransformer;
import org.dynamo.enhancer.writers.DataSourceWriter;
import org.dynamo.enhancer.writers.JarFileWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class DataTransformerImpl implements DataTransformer {

    /** Logger. */
    private Logger logger = LoggerFactory.getLogger(getClass());

    /** Class transformer. */
    private ClassTransformer transformer;

    /**
     * Init.
     * @param classTransformer class transformer
     */
    public DataTransformerImpl(final ClassTransformer classTransformer) {

        this.transformer = classTransformer;
    }

    @Override
    public void transformJarFile(
            final File inputFile,
            final File outputFile,
            final MethodFilter userFilter) throws IOException {

        logger.info("Transforming file: {}", inputFile);

        final MethodFilter filter = new AndMethodFilter(new CrossJarMethodFilter(inputFile), userFilter);

        DataSourceReader reader = new JarFileReader(inputFile);
        try (final DataSourceWriter writer = new JarFileWriter(outputFile)) {

            reader.loadResources(new ResourceStreamVisitor() {
                @Override
                public void visitClassStream(
                        final String fullClassName,
                        final String sourcePath,
                        final InputStream stream) throws IOException {

                    byte[] inputData = IOUtils.toByteArray(stream);
                    byte[] outputData = transformer.transform(fullClassName, inputData, filter);

                    ByteArrayInputStream convertedStream = new ByteArrayInputStream(outputData);
                    String classFileName = fullClassName + ".class";
                    logger.trace("Adding converted class: {}", classFileName);
                    writer.addResource(classFileName, convertedStream);
                }

                @Override
                public void visitAnotherResource(final String name, final InputStream stream) throws IOException {
                    logger.trace("Adding another resource: {}", name);
                    // just copy input values
                    writer.addResource(name, stream);
                }
            });
        }

        logger.info("Finished, results stored into: {}", outputFile);
    }
}
