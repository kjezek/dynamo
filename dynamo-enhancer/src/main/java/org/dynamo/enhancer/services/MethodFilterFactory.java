/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.enhancer.services;

import org.dynamo.enhancer.filters.MethodFilter;

/**
 * Method filter factory.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 * @param <T> data to create the filter
 */
public interface MethodFilterFactory<T> {

    /**
     * Create a method filter for a JAR file.
     * @param data data for the filter
     * @param globalFilter a user, global, or default filter pattern
     * @return method filter instance.
     */
    MethodFilter create(T data, MethodFilter globalFilter);
}
