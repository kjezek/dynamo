/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.rt;

import org.dynamo.rt.finder.MemberFinder;
import org.dynamo.rt.finder.MemberFinderFactory;

import java.lang.invoke.CallSite;
import java.lang.invoke.ConstantCallSite;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.logging.Logger;

/**
 * Runtime resolution as described in the paper.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public final class DynamoBootstrap {

    /** Class name. */
    private static final String CLASS_NAME = DynamoBootstrap.class.getName();

    /** Logger. */
    private static Logger logger = Logger.getLogger(DynamoBootstrap.class.getName());

    /** Virtual methods finder. */
    private static MemberFinder<Method> virtualMethods =  MemberFinderFactory.createVirtualMethods();

    /** Static methods finder. */
    private static MemberFinder<Method> staticMethods =  MemberFinderFactory.createStaticMethods();

    /** Static methods finder. */
    private static MemberFinder<Method> specialMethods =  MemberFinderFactory.createSpecialMethods();

    /** Constructors finder. */
    private static MemberFinder<Constructor<?>> constructors =  MemberFinderFactory.createConstructors();


    /** No instance needed. */
    private DynamoBootstrap() { }

    /**
     * Bootstrap method invoked by invoke dynamic.
     *
     * It tries to findConversion exactly the same method as required by input first.
     * If the method is not found, try to findConversion a type conversion.
     *
     * @param caller caller.
     * @param name method name
     * @param type method type
     * @return call site
     * @throws ClassNotFoundException error
     * @throws IllegalAccessException error
     */
    public static CallSite bootstrapVirtual(
            final MethodHandles.Lookup caller,
            final String name,
            final MethodType type
    ) throws ClassNotFoundException, IllegalAccessException {

        logger.entering(CLASS_NAME, "bootstrapVirtual", new Object[] {name, type});

        Class<?> owner = type.parameterType(0);
        Class<?> callerClass = caller.lookupClass();
        Method method = virtualMethods.find(callerClass, owner, name, type);

        if (method == null) {
            String desc = type.dropParameterTypes(0, 1).toMethodDescriptorString();
            throw new NoSuchMethodError(owner.getName() + "#" + name + desc);
        }

        MethodHandle handle = virtualMethods.toHandle(caller, method);

        MethodHandle finalHandle = handle;

        // static methods are typed so that parameter types start from zero index
        //i.e. the first index is not the owner class type.
        if (Modifier.isStatic(method.getModifiers())) {
            // create a new Handler that just drops first (zero) argument typed as
            // the owner class, and then invokes the target handle
            finalHandle = MethodHandles.dropArguments(handle, 0, method.getDeclaringClass());
        }

        logger.exiting(CLASS_NAME, "bootstrapVirtual", handle);

        return new ConstantCallSite(finalHandle.asType(type));
    }

    /**
     * Bootstrap method invoked by invoke dynamic.
     *
     * It tries to findConversion exactly the same method as required by input first.
     * If the method is not found, try to findConversion a type conversion.
     *
     * @param caller caller.
     * @param name method name
     * @param type method type
     * @param owner owner
     * @return call site
     * @throws ClassNotFoundException error
     * @throws IllegalAccessException error
     */
    public static CallSite bootstrapStatic(
            final MethodHandles.Lookup caller,
            final String name,
            final MethodType type,
            final String owner
    ) throws ClassNotFoundException, IllegalAccessException {

        logger.entering(CLASS_NAME, "bootstrapStatic", new Object[] {name, type, owner});

        Class<?> ownerClass = Class.forName(owner.replace('/', '.'));
        Class<?> callerClass = caller.lookupClass();
        Method method = staticMethods.find(callerClass, ownerClass, name, type);

        // existing method is non-static
        if (method != null && !Modifier.isStatic(method.getModifiers())) {
            method = null;
        }

        if (method == null) {
            throw new NoSuchMethodError(ownerClass.getName() + "#" + name + type.toMethodDescriptorString());
        }

        MethodHandle handle = staticMethods.toHandle(caller, method);

        logger.exiting(CLASS_NAME, "bootstrapStatic", handle);

        return new ConstantCallSite(handle.asType(type));
    }


    /**
     * Bootstrap method invoked by invoke dynamic.
     *
     * It tries to findConversion exactly the same method as required by input first.
     * If the method is not found, try to findConversion a type conversion.
     *
     * @param caller caller.
     * @param name method name
     * @param type method type
     * @return call site
     * @throws ClassNotFoundException error
     * @throws IllegalAccessException error
     */
    public static CallSite bootstrapInterface(
            final MethodHandles.Lookup caller,
            final String name,
            final MethodType type
    ) throws ClassNotFoundException, IllegalAccessException {

        // invocation is the same as for virtual methods.
        return bootstrapVirtual(caller, name, type);
    }


    /**
     * Bootstrap method invoked by invoke dynamic.
     *
     * It tries to findConversion exactly the same method as required by input first.
     * If the method is not found, try to findConversion a type conversion.
     *
     * @param caller caller.
     * @param name method name
     * @param type method type
     * @return call site
     * @throws ClassNotFoundException error
     * @throws IllegalAccessException error
     */
    public static CallSite bootstrapSpecialNew(
            final MethodHandles.Lookup caller,
            final String name,
            final MethodType type
    ) throws ClassNotFoundException, IllegalAccessException {

        logger.entering(CLASS_NAME, "bootstrapSpecialNew", new Object[] {name, type});

        Class<?> owner = type.returnType();
        Class<?> callerClass = caller.lookupClass();
        Constructor<?> constructor = constructors.find(callerClass, owner, owner.getName(), type);

        if (constructor == null) {
            String desc = type.changeReturnType(void.class).toMethodDescriptorString();
            throw new NoSuchMethodError(owner.getName() + "#" + name + desc);
        }

        MethodHandle handle = constructors.toHandle(caller, constructor);

        logger.exiting(CLASS_NAME, "bootstrapSpecialNew", handle);

        return new ConstantCallSite(handle.asType(type));
    }



    /**
     * Bootstrap method invoked by invoke dynamic.
     *
     * It tries to findConversion exactly the same method as required by input first.
     * If the method is not found, try to findConversion a type conversion.
     *
     * @param caller caller.
     * @param name method name
     * @param type method type
     * @return call site
     * @throws ClassNotFoundException error
     * @throws IllegalAccessException error
     */
    public static CallSite bootstrapSpecialSuper(
            final MethodHandles.Lookup caller,
            final String name,
            final MethodType type
    ) throws ClassNotFoundException, IllegalAccessException {

        logger.entering(CLASS_NAME, "bootstrapSpecialSuper", new Object[] {name, type});

        Class<?> owner = type.parameterType(0);
        Class<?> callerClass = caller.lookupClass();
        Method method = specialMethods.find(callerClass, owner, name, type);

        if (method == null) {
            String desc = type.dropParameterTypes(0, 1).toMethodDescriptorString();
            throw new NoSuchMethodError(owner.getName() + "#" + name + desc);
        }

        MethodHandle handle = specialMethods.toHandle(caller, method);

        logger.exiting(CLASS_NAME, "bootstrapSpecialSuper", handle);

        return new ConstantCallSite(handle.asType(type));
    }

}
