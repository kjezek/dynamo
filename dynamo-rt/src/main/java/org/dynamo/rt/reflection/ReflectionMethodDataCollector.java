/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.rt.reflection;

import org.dynamo.rt.finder.MemberDataCollector;
import org.dynamo.rt.utils.Utils;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Convenient implementation for method finders.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class ReflectionMethodDataCollector implements MemberDataCollector<Method> {

    @Override
    public Collection<Method> getMembers(final Class<?> owner) {
        Collection<Method> methods = new ArrayList<>();
        Utils.collectMethodsHierarchy(owner, methods);
        return methods;
    }

    @Override
    public List<Class<?>> getParameterTypes(final Method member) {
        return Arrays.asList(member.getParameterTypes());
    }

    @Override
    public Class<?> getReturnType(final Method member) {
        return member.getReturnType();
    }

    @Override
    public MethodHandle toHandle(
            final MethodHandles.Lookup lookup,
            final Method member) throws IllegalAccessException {

        return lookup.unreflect(member);
    }
}
