package org.dynamo.rt.reflection;

import org.dynamo.rt.best.TypeConversionGraph;
import org.dynamo.rt.best.XD;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Graph storing inheritance information.
 * <p/>
 * The graph is initialized with implicit Java inheritance aka
 * any class inherits from Object.
 * <p/>
 * It also contains boxing and unboxing.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class ReflectionTypeConversionGraph implements TypeConversionGraph {


    /**
     * A flag saying if boxing is enabled or disabled.
     */
    private boolean boxingAllowed = true;


    @Override
    public TypeConversionGraph withBoxing(final boolean enabled) {
        this.boxingAllowed = enabled;
        return this;
    }

    @Override
    public boolean isMoreSpecificThan(final XD baseXD, final XD candidateXD) {

        boolean moreSpecific = isMoreSpecificThan(baseXD.getOwner(), false, candidateXD.getOwner());
//        moreSpecific &= isMoreSpecificThan(baseXD.getRetType(), true, candidateXD.getRetType());

        // both XDs have the same number of parameters.
        for (int i = 0; i < candidateXD.getParams().size(); i++) {
            moreSpecific &= isMoreSpecificThan(baseXD.getParams().get(i), false, candidateXD.getParams().get(i));
        }

        return moreSpecific;
    }

    @Override
    public boolean isMoreSpecificThan(final Class<?> type, final boolean rType, final Class<?> candidate) {

//        JSL: 15.12.2 Compile-Time Step 2: Determine Method Signature
        // First, boxing/unboxing is not permitted
        // Second, boxing/unboxing allowed
        // Third, variable arity allowed -- we ignore this, byte-code has not variable arity.

        boolean r;

        // general case, no wrapper, no primitive, only general Java types
        // example:  int -> double -> Double
        // example:  ArrayList -> List

        // for primitive  types: check without boxing.
        // when boxing is disabled, the only possible conversion narrowing or widening.
        // example:  int -> double  for parameter types.
        // This also covers covariant of return types
        // example: long -> int -> Integer

        // for wrapper types: check without boxing
        // we have no "unboxing edges" in the graph, so we can simply proceed

        // speed up -- we have the exact match.
        if (candidate.equals(type)) {
            r = true;
        } else {

            // true if type and candidate are reachable, ignoring boxing and unboxing
            r =  isReachablePrimitive(type, candidate, rType) || isReachable(type, candidate, rType);

            if (!r && boxingAllowed) {

                if (isPrimitive(type)) {

                    // if no result, enable boxing simply by boxing the input type
                    // amd finding a respective conversion
                    // example of possible change:  int -> Integer -> Number
                    // but NOT!! :  Integer -> int -> double -> Double
                    Class<?> box = box(type);
                    r = isReachable(box, candidate, rType);

                } else if (isWrapper(type)) {
                    // if no result, enable unboxing simply by unboxing the input type
                    // amd finding a respective primitive conversion
                    // example of possible change:  Integer -> int -> double
                    // but NOT!! :  Integer -> int -> double -> Double
                    Class<?> unbox = unbox(type);
                    r = isReachablePrimitive(unbox, candidate, rType);

                }
            }
        }

        return r;
    }

    /**
     * Compute path between two vertexes.
     *
     * @param type  type
     * @param cand  candidate
     * @param rType true for return types.
     * @return path or null, if the path does not exist
     */
    private boolean isReachable(final Class<?> type, final Class<?> cand, final boolean rType) {
        boolean r = false;

        if (type != null && cand != null) {
            if (rType) {
                r = type.isAssignableFrom(cand);
            } else {
                r = cand.isAssignableFrom(type);
            }
        }

        return r;
    }

    /**
     * Compute path between two vertexes.
     *
     * @param type  type
     * @param cand  candidate
     * @param rType true for return types.
     * @return path or null, if the path does not exist
     */
    private boolean isReachablePrimitive(final Class<?> type, final Class<?> cand, final boolean rType) {
        boolean r = false;

        if (type != null && cand != null) {
            int i = TYPES_HIERARCHY.indexOf(type);
            int j = TYPES_HIERARCHY.indexOf(cand);

            if (i >= 0 && j >= 0) {
                if (rType) {
                    r = i <= j;
                } else {
                    r = j >= i;
                }
            }
        }

        return r;
    }

    /**
     * Box input type.
     *
     * @param type input
     * @return wrapper type or EMPTY.
     */
    private static Class<?> box(final Class<?> type) {
        return PRIM_TO_WRAP.get(type);
    }


    /**
     * Unbox.
     *
     * @param type input
     * @return output
     */
    private static Class<?> unbox(final Class<?> type) {
        return WRAP_TO_PRIM.get(type);
    }

    /**
     * @param type input
     * @return true for primitive type.
     */
    private static boolean isPrimitive(final Class<?> type) {
        return PRIM_TO_WRAP.keySet().contains(type);
    }

    /**
     * @param type input
     * @return true for wrapper type.
     */
    private static boolean isWrapper(final Class<?> type) {
        return WRAP_TO_PRIM.keySet().contains(type);
    }


    /**
     * Hash table of values of primitive types.
     */
    private static final Map<Class<?>, Class<?>> TYPES_TABLE;

    /**
     * Implicit hierarchy chains for primitive types.
     */
    public static final List<Class<?>> TYPES_HIERARCHY;

    /** Table of boxing. */
    public static final Map<Class<?>, Class<?>> PRIM_TO_WRAP = new HashMap<>();

    /** Table of unboxing. */
    public static final Map<Class<?>, Class<?>> WRAP_TO_PRIM = new HashMap<>();



    /**
     * Makes a hash-table from primitive types and their hierarchy values.
     */
    static {
        TYPES_TABLE = new HashMap<>();

        TYPES_TABLE.put(boolean.class, Boolean.class);
        TYPES_TABLE.put(byte.class, Byte.class);
        TYPES_TABLE.put(short.class, Short.class);
        TYPES_TABLE.put(int.class, Integer.class);
        TYPES_TABLE.put(long.class, Long.class);
        TYPES_TABLE.put(float.class, Float.class);
        TYPES_TABLE.put(double.class, Double.class);
        TYPES_TABLE.put(char.class, Character.class);
        TYPES_TABLE.put(void.class, Void.class);

        TYPES_HIERARCHY = new ArrayList<>();
        TYPES_HIERARCHY.addAll(Arrays.asList(
                byte.class, short.class, int.class, long.class, float.class, double.class));

        for (Map.Entry<Class<?>, Class<?>> entry : TYPES_TABLE.entrySet()) {
            PRIM_TO_WRAP.put(entry.getKey(), entry.getValue());
            WRAP_TO_PRIM.put(entry.getValue(), entry.getKey());
        }

    }
    /**
     * Init.
     */
    public ReflectionTypeConversionGraph() {
    }


}
