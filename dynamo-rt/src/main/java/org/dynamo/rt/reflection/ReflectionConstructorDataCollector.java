/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.rt.reflection;

import org.dynamo.rt.finder.MemberDataCollector;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.dynamo.rt.utils.Utils.collectConstructorsHierarchy;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class ReflectionConstructorDataCollector implements MemberDataCollector<Constructor<?>> {


    @Override
    public Collection<Constructor<?>> getMembers(final Class<?> owner) {
        Collection<Constructor<?>> constructors = new ArrayList<>();
        collectConstructorsHierarchy(owner, constructors);
        return constructors;
    }

    @Override
    public List<Class<?>> getParameterTypes(final Constructor<?> member) {
        return Arrays.asList(member.getParameterTypes());
    }

    @Override
    public MethodHandle toHandle(
            final MethodHandles.Lookup lookup,
            final Constructor<?> member) throws IllegalAccessException {

        return lookup.unreflectConstructor(member);
    }

    @Override
    public Class<?> getReturnType(final Constructor member) {
        return member.getDeclaringClass();
    }
}
