/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.rt.utils;

import org.dynamo.rt.DynamoBootstrap;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.logging.Logger;

/**
 * Common utils.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public final class Utils {

    /**
     * Private.
     */
    private Utils() {

    }

    /**
     * Logger.
     */
    private static Logger logger = Logger.getLogger(DynamoBootstrap.class.getName());

    /**
     * Iterate type hierarchy and collect methods from super types.
     *
     * @param owner   owner.
     * @param methods a collection to store methods into.
     */
    public static void collectMethodsHierarchy(
            final Class<?> owner,
            final Collection<Method> methods) {

        if (owner != null) {

            List<Class<?>> classes = iterateHierarchyBFS(owner);
            for (Class<?> clazz : classes) {
                // when hierarchy analysis finds a missing class, do not stop evaluation
                // but continue with next methods.
                try {
                    methods.addAll(filterAvailable(clazz.getDeclaredMethods()));
                } catch (NoClassDefFoundError e) {
                    logger.warning("Skipping hierarchy analysis due to NoClassDefFoundError");
                }

            }
        }
    }

    /**
     * Filter input elements to contain only those generally available ones.
     * It filters methods such as bridge or synthetic.
     * @param members methods
     * @return filtered.
     */
    private static Collection<Method> filterAvailable(final Method[] members) {
        Collection<Method> result = new ArrayList<>();
        for (Method m : members) {
            if (!m.isBridge() && !m.isSynthetic()) {
                result.add(m);
            }
        }

        return result;
    }


    /**
     * Iterate type hierarchy and collect methods from super types.
     *
     * @param owner        owner.
     * @param constructors a collection to store methods into.
     */
    public static void collectConstructorsHierarchy(
            final Class<?> owner,
            final Collection<Constructor<?>> constructors) {

        if (owner != null) {

            List<Class<?>> classes = iterateHierarchyBFS(owner);
            for (Class<?> clazz : classes) {
                // when hierarchy analysis finds a missing class, do not stop evaluation
                // but continue with next methods.
                try {
                    constructors.addAll(Arrays.asList(clazz.getDeclaredConstructors()));
                } catch (NoClassDefFoundError e) {
                    logger.warning("Skipping hierarchy analysis due to NoClassDefFoundError");
                }
            }
        }
    }

    /**
     * Iterate class hierarchy and return all parents.
     * @param aClass input class
     * @return all parents
     */
    public static List<Class<?>> iterateHierarchyBFS(final Class<?> aClass) {

        List<Class<?>> result = new ArrayList<>();
        Class<?> current;
        Queue<Class<?>> queue = new LinkedList<>();
        queue.add(aClass);

        while (!queue.isEmpty()) {
            current = queue.poll();
            result.add(current);

            if (current.getSuperclass() != null) {
                queue.add(current.getSuperclass());
            }

            Collections.addAll(queue, current.getInterfaces());
        }

        return result;
    }


}
