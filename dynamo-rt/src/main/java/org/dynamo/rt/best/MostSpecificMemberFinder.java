/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.rt.best;

import org.dynamo.rt.finder.AbstractMemberFinder;
import org.dynamo.rt.finder.MemberDataCollector;

import java.lang.invoke.MethodType;
import java.lang.reflect.Member;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This finder will find the best matching candidate
 * by its closeness in the type hierarchy.
 *
 * @param <T> a member type -- constructor or method
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class MostSpecificMemberFinder<T extends Member> extends AbstractMemberFinder<T> {

    /**
     * Global inheritance graph.
     */
    private TypeConversionGraph graph;


    /**
     * Create instance.
     *
     * @param dataCollector data collector impl.
     * @param graph type coversion graph
     * @param indexShift    index shift
     */
    public MostSpecificMemberFinder(
            final MemberDataCollector<T> dataCollector,
            final TypeConversionGraph graph,
            final int indexShift) {
        super(dataCollector, indexShift);

        this.graph = graph;
    }

    @Override
    public T find(final Class<?> owner, final Collection<T> members, final MethodType type) {

        // compute matrix
        Class<?> pivotRtype = type.returnType();
        List<Class<?>> pivotParams = getParamTypes(type);

        // this is XD that we want to find a subtype for
        XD baseXD = new XD(owner, pivotRtype, pivotParams);

        // convert memebers to XDs.
        Map<T, XD> candidatesXD = collectCandidates(members);

//        JSL: 15.12.2 Compile-Time Step 2: Determine Method Signature
        // First, boxing/unboxing is not permitted
        // Second, boxing/unboxing allowed
        T result = resolve(baseXD, candidatesXD, graph.withBoxing(false));
        if (result == null) {
            result = resolve(baseXD, candidatesXD, graph.withBoxing(true));
        }

        return result;
    }

    /**
     * Resolve most specific method.
     * @param baseXD base extended descriptor.
     * @param candidatesXD candidate XDs
     * @param graph type reasoner.
     * @return result or null.
     */
    private T resolve(
            final XD baseXD,
            final Map<T, XD> candidatesXD,
            final TypeConversionGraph graph) {

        // resolve candidates that are subtype at all
        Map<T, XD> xdescriptors = resolveSubtypeCandidates(baseXD, candidatesXD, graph);
        // resolve candidates that are most specific.
        Map<T, XD> mostSpecificXDs = resolveMostSpecific(xdescriptors, graph);

        T result = null;

        // decision time !!

        if (mostSpecificXDs.size() == 0) {
            result = null;
        } else if (mostSpecificXDs.size() == 1) {
            result = mostSpecificXDs.keySet().iterator().next();
        } else {
            // if there are multiple most specific XDs, try to disambiguate by selecting the one with the
            // most specific return type
            // this is similar to how the compiler disambiguates (abstract) methods
            // https://docs.oracle.com/javase/specs/jls/se8/html/jls-15.html#jls-15.12.2.5
            List<Class<?>> paramTypes = null;
            Class<?> returnType = null;
            for (Map.Entry<T, XD> entry : mostSpecificXDs.entrySet()) {
                XD xd = entry.getValue();
                // start only with an XD that has a return type more specific than the expected one
                if (paramTypes == null && graph.isMoreSpecificThan(xd.getRetType(), true, baseXD.getRetType())) {
                    paramTypes = xd.getParams();
                    returnType = xd.getRetType();
                    result = entry.getKey();
                } else {
                    boolean sameParamTypes = paramTypes != null && paramTypes.equals(xd.getParams());
                    if (sameParamTypes) {
                        Class<?> returnType2 = xd.getRetType();
                        if (graph.isMoreSpecificThan(returnType2, true, returnType)) {
                            // update selection
                            returnType = returnType2;
                            result = entry.getKey();
                        }
                    } else {
                        result = null;
                    }
                }
            }
        }

        return result;
    }

    /**
     * Convert members to XDs.
     * @param members members
     * @return extended descriptors.
     */
    private Map<T, XD> collectCandidates(final Collection<T> members) {
        // all XD candidates added to the map
        // members are already filtered to contain only those
        // with the same number of parameters, excluding non-accessible ones,
        // and filtering out bridge and synthetic ones.
        // This filtering is implemented in the abstract parent class.
        Map<T, XD> candidatesXD = new HashMap<>();
        for (T m : members) {
            XD xd = new XD(
                    m.getDeclaringClass(),
                    getDataCollector().getReturnType(m),
                    getDataCollector().getParameterTypes(m)
            );

            candidatesXD.put(m, xd);
        }

        return candidatesXD;
    }

    /**
     * Resolve all members that are potential candidates, i.e. they all
     * have their XD subtype of base XD.
     * @param baseXD base extended descriptor.
     * @param candidatesXD candidates.
     * @param graph type conversion graph
     * @return map where key is a member and value its XD.
     */
    private Map<T, XD> resolveSubtypeCandidates(
            final XD baseXD,
            final Map<T, XD> candidatesXD,
            final TypeConversionGraph graph) {

        // find possible candidates = XDs that are subtypes.
        Map<T, XD> xdescriptors = new HashMap<>();
        for (T m : candidatesXD.keySet()) {
            XD xd = candidatesXD.get(m);
            if (graph.isMoreSpecificThan(baseXD, xd)) {
                xdescriptors.put(m, xd);
            }
        }

        return  xdescriptors;
    }

    /**
     * Resolve most specific XDs.
     * @param xdescriptors available descriptors that all are subtype,
     *                     but we do not know which one is the most specific.
     * @param graph type conversion graph
     * @return map where key is a member and value its XD.
     */
    private Map<T, XD> resolveMostSpecific(
            final Map<T, XD> xdescriptors,
            final TypeConversionGraph graph) {

        // now select most specific methods
        // this is potentially slow (quadratic), but will be fast in practise as the number of candidates is small
        // with a proper graph-based approach this could be done in linear time using DFS
        Map<T, XD> mostSpecificXDs = new HashMap<>();
        for (Map.Entry<T, XD> entry : xdescriptors.entrySet()) {
            XD xd = entry.getValue();
            boolean mostSpecific = true;
            for (XD otherXD : xdescriptors.values()) {
                mostSpecific = mostSpecific && graph.isMoreSpecificThan(xd, otherXD);
            }
            if (mostSpecific) {
                mostSpecificXDs.put(entry.getKey(), xd);
            }
        }

        return mostSpecificXDs;
    }


    /**
     * Fetch parameter types from the object of method type.
     *
     * @param type the method type
     * @return a list of param types
     */
    private List<Class<?>> getParamTypes(final MethodType type) {
        List<Class<?>> params = new ArrayList<>();
        for (int i = getIndexShift(); i < type.parameterCount(); i++) {
            params.add(type.parameterType(i));
        }
        return params;
    }
}
