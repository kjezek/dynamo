/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.rt.best;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public interface TypeConversionGraph {

    /**
     * Enable or disable boxing.
     *
     * @param enabled enabled or disabled.
     * @return this object.
     */
    TypeConversionGraph withBoxing(boolean enabled);

    /**
     * True if one descriptor is more specific that other one, or the same.
     *
     * @param baseXD      extended descriptor.
     * @param candidateXD extended descriptor.
     * @return true for more specific
     */
    boolean isMoreSpecificThan(XD baseXD, XD candidateXD);

    /**
     * Find most specific matching type from the list of candidates.
     *
     * @param type      pivot type.
     * @param candidate candidates.
     * @param rType     true for analysis of return types.
     * @return name of the type or empty string.
     */
    boolean isMoreSpecificThan(Class<?> type, boolean rType, Class<?> candidate);
}
