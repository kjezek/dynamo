/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.rt.best;

import java.util.List;

/**
 * This is extended descriptor.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class XD {


    /**
     * Pivotal return type.
     */
    private Class<?> retType;

    /**
     * Owner.
     */
    private Class<?> owner;


    /**
     * Pivotal parameter type.
     */
    private List<Class<?>> params;

    /**
     * Merge input to one array.
     *
     * @param owner  member owner
     * @param rType      return type.
     * @param paramTypes parameter types.
     */
    public XD(final Class<?> owner, final Class<?> rType, final List<Class<?>> paramTypes) {

        this.owner = owner;
        this.retType = rType;
        this.params = paramTypes;
    }

    public Class<?> getRetType() {
        return retType;
    }

    public Class<?> getOwner() {
        return owner;
    }

    public List<Class<?>> getParams() {
        return params;
    }
}
