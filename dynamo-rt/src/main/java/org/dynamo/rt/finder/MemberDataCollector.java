/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.rt.finder;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Member;
import java.util.Collection;
import java.util.List;

/**
 * Interface to collect data from a member (aka constructors, methods, etc).
 *
 * This interface may be replaced by java.lang.reflect.Executable,
 * which is available from Java 1.8.
 *
 * For compatibility with Java 1.7, we must use this interface.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 * @param <T> a member type (constructor, method, etc)
 */
public interface MemberDataCollector<T extends Member> {

    /**
     * Initial methods set.
     * @param owner owner.
     * @return either methods or constructors. Values are grouped by inheritance hierarchy, subclasses going first
     */
    Collection<T> getMembers(Class<?> owner);

    /**
     * Parameter types of current member.
     * @param member a member
     * @return parameter types
     */
    List<Class<?>> getParameterTypes(T member);

    /**
     * Return types of current member.
     * @param member a member
     * @return parameter type, or null for constructor.
     */
    Class<?> getReturnType(T member);

    /**
     * Convert the input member to the method handle.
     *
     * @param lookup a method lookup
     * @param member input member
     * @return method handle
     * @throws IllegalAccessException illegal access.
     */
    MethodHandle toHandle(MethodHandles.Lookup lookup, T member) throws IllegalAccessException;

}
