/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.rt.finder;

import org.dynamo.rt.best.MostSpecificMemberFinder;
import org.dynamo.rt.reflection.ReflectionTypeConversionGraph;
import org.dynamo.rt.best.TypeConversionGraph;
import org.dynamo.rt.reflection.ReflectionConstructorDataCollector;
import org.dynamo.rt.reflection.ReflectionMethodDataCollector;
import org.dynamo.rt.reflection.ReflectionSpecialMethodDataCollector;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/**
 * Factory to create member finders.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public final class MemberFinderFactory {

    /**
     * Global inheritance graph.
     */
    private static TypeConversionGraph graph = new ReflectionTypeConversionGraph();

    /** Service . */
    private static final MemberFinder<Constructor<?>> CONSTRUCTOR_FINDER
            = new MostSpecificMemberFinder<>(new ReflectionConstructorDataCollector(), graph, 0);

    /** Service . */
    private static final MemberFinder<Method> STATIC_METHODS_FINDER
            = new MostSpecificMemberFinder<>(new ReflectionMethodDataCollector(), graph, 0);

    /** Service . */
    private static final MemberFinder<Method> VIRTUAL_METHODS_FINDER
            = new MostSpecificMemberFinder<>(new ReflectionMethodDataCollector(), graph, 1);

    /** Service . */
    private static final MemberFinder<Method> SPECIAL_METHODS_FINDER
            = new MostSpecificMemberFinder<>(new ReflectionSpecialMethodDataCollector(), graph, 1);


    /** Static init. */
    private MemberFinderFactory() {

    }

    /**
     * Init for finding of virtual methods.
     * @return instance for virtual methods.
     */
    public static MemberFinder<Method> createVirtualMethods() {
        return VIRTUAL_METHODS_FINDER;

    }

    /**
     * Init for finding of other, i.e. non-virtual methods.
     * @return instance for other, i.e. non-virtual methods.
     */
    public static MemberFinder<Method> createStaticMethods() {
        return STATIC_METHODS_FINDER;
    }

    /**
     * Init for finding of other, i.e. non-virtual methods.
     * @return instance for other, i.e. non-virtual methods.
     */
    public static MemberFinder<Method> createSpecialMethods() {
        return SPECIAL_METHODS_FINDER;
    }

    /**
     * Init for finding of other, i.e. non-virtual methods.
     * @return instance for other, i.e. non-virtual methods.
     */
    public static MemberFinder<Constructor<?>> createConstructors() {
        return CONSTRUCTOR_FINDER;
    }


}
