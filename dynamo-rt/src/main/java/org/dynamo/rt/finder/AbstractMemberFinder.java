/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.rt.finder;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Member;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Convenient implementation of member finder.
 * This performs basic filtering of available members
 * to contain only those with required "name" and "parameter count"
 * as in the paper Section 5.2.
 *
 * @param <T> a member type -- constructor or method
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public abstract class AbstractMemberFinder<T extends Member> implements MemberFinder<T> {

    /**
     * Class name.
     */
    private final String className = getClass().getName();

    /**
     * Logger.
     */
    private Logger logger = Logger.getLogger(getClass().getName());

    /**
     * Data collector.
     */
    private MemberDataCollector<T> dataCollector;

    /**
     * Index shift.
     */
    private int indexShift;

    /**
     * Create instance.
     *
     * @param dataCollector data collector impl.
     * @param indexShift    index shift
     */
    public AbstractMemberFinder(final MemberDataCollector<T> dataCollector, final int indexShift) {
        this.dataCollector = dataCollector;
        this.indexShift = indexShift;
    }

    @Override
    public final T find(final Class<?> caller, final Class<?> owner, final String name, final MethodType type) {

        logger.entering(className, "find", new Object[]{owner, name});

        // collect all members
        Collection<T> allMembers = dataCollector.getMembers(owner);
        // filter out only members with the given name
        Collection<T> membersByName = findByName(allMembers, name);
        // filter out only members with the same parameter counts.
        Collection<T> membersByParamCount = findByParamCount(membersByName, type);
        // collect only accessible ones
        Collection<T> accessible = findAccessible(caller, membersByParamCount);

        T member = find(owner, accessible, type);

        logger.exiting(className, "find", member);
        return member;
    }

    /**
     * Find either an exact member or a type convertible member.
     *
     * @param members a set of all possible members.
     * @param type    expected type
     * @param owner member owner
     * @return a member  or null.
     */
    public abstract T find(final Class<?> owner, final Collection<T> members, final MethodType type);


    /**
     * Find all members by the given name.
     *
     * @param members all members
     * @param name    method name.
     * @return list
     */
    private Collection<T> findByName(final Collection<T> members, final String name) {
        logger.entering(className, "findByName", name);

        List<T> result = new LinkedList<>();
        for (T m : members) {

            logger.finest("Matching member " + m);

            if (name.equals(m.getName())) {
                result.add(m);
            }
        }

        logger.exiting(className, "findByName", result.size());

        return result;
    }


    /**
     * Find and get only members with required number of parameters.
     *
     * @param members members
     * @param type type with expected parameters.
     * @return result
     */
    private Collection<T> findByParamCount(final Collection<T> members, final MethodType type) {
        logger.entering(className, "findByParamCount", members);

        List<T> result = new LinkedList<>();

        for (T m : members) {

            logger.finest("Matching member " + m);

            if (dataCollector.getParameterTypes(m).size() == type.parameterCount() - getIndexShift()) {
                result.add(m);
            }
        }

        logger.exiting(className, "findByParamCount", result.size());

        return result;
    }


    /**
     * Find only accessible member. It allows only public elements, or
     * protected elements from super classes,
     * or package friend from the same packages.
     * @param caller calling class.
     * @param members all members
     * @return available members
     */
    private Collection<T> findAccessible(final Class<?> caller, final Collection<T> members) {
        logger.entering(className, "findAccessible", members.size());

        List<T> result = new ArrayList<>();
        for (T m : members) {

            if (Modifier.isPublic(m.getModifiers())) {
                // add all public
                result.add(m);
            } else if (Modifier.isProtected(m.getModifiers())) {

                processProtected(caller, m, result);

            } else if (!Modifier.isPrivate(m.getModifiers())
                    && !Modifier.isPublic(m.getModifiers())
                    && !Modifier.isProtected(m.getModifiers())) {

                processPackage(caller, m, result);
            }

        }

        logger.exiting(className, "findAccessible", result.size());

        return result;
    }

    /**
     * Process protected element.
     * @param caller caller class.
     * @param m current member
     * @param result list of result to extend
     */
    private void processProtected(
            final Class<?> caller,
            final T m,
            final List<T> result) {

        // access only in the same package
        // or inherited classes

        if (caller.getPackage() == null && m.getDeclaringClass().getPackage() == null) {
            result.add(m);
        }

        if (caller.getPackage() != null && m.getDeclaringClass().getPackage() != null) {
            if (caller.getPackage().equals(m.getDeclaringClass().getPackage())
                    || m.getDeclaringClass().isAssignableFrom(caller)) {

                result.add(m);
            }

        }
    }


    /**
     * Process package private  element.
     * @param caller caller class.
     * @param m current member
     * @param result list of result to extend
     */
    private void processPackage(
            final Class<?> caller,
            final T m,
            final List<T> result) {

        // access only in the same package
        if (caller.getPackage().equals(m.getDeclaringClass().getPackage())) {
            result.add(m);
        }

    }

    @Override
    public MethodHandle toHandle(
            final MethodHandles.Lookup lookup,
            final T member) throws IllegalAccessException {

        return dataCollector.toHandle(lookup, member);
    }

    /**
     * @return current data collector.
     */
    public final MemberDataCollector<T> getDataCollector() {
        return dataCollector;
    }

    /**
     * Index alignment.
     *
     * @return 1 - for virtual, interface and special methods, 0 - for static methods.
     */
    public final int getIndexShift() {
        return indexShift;
    }

}
