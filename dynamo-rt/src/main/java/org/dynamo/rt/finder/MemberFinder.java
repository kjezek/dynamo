/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.rt.finder;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Member;

/**
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 *
 * @param <T> a member type -- constructor or method
 */
public interface MemberFinder<T extends Member> {

    /**
     * Find a member first by exact matching.
     * Then try to find by a type conversion.
     *
     *
     *
     * @param caller calling class
     * @param owner member owner
     * @param name member name
     * @param type expected type.
     *
     * @return detected member or null.
     */
    T find(Class<?> caller, Class<?> owner, String name, MethodType type);

    /**
     * Convert the input member to the method handle.
     *
     * @param lookup a method lookup
     * @param member input member
     * @return method handle
     * @throws IllegalAccessException illegal access.
     */
    MethodHandle toHandle(MethodHandles.Lookup lookup, T member) throws IllegalAccessException;
}
