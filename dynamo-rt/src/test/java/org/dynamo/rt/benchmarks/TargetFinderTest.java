/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.rt.benchmarks;

import org.dynamo.rt.benchmarks.boxunbox.PrimParamType;
import org.dynamo.rt.benchmarks.boxunbox.WrapperRetType;
import org.dynamo.rt.benchmarks.disambiguatebyreturntype.DisamRetType;
import org.dynamo.rt.benchmarks.methodoverloading.MethodOverloadingAmbiguous;
import org.dynamo.rt.benchmarks.methodoverloading.MethodOverloadingCollections;
import org.dynamo.rt.benchmarks.methodoverloading.MethodOverloadingPrimWrap;
import org.dynamo.rt.benchmarks.overloadinghierarchy.ChildHierarchy;
import org.dynamo.rt.benchmarks.overloadinghierarchy.ParentHierarchy;
import org.dynamo.rt.benchmarks.specstaticreturntype.SpecRetType;
import org.dynamo.rt.finder.MemberFinder;
import org.dynamo.rt.finder.MemberFinderFactory;
import org.junit.Test;

import java.lang.invoke.MethodType;
import java.lang.reflect.Method;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Test case and executable for benchmarking.
 * @author jens dietrich
 */
public class TargetFinderTest {

    public static final String METHOD_NAME = "foo";

    /** Lookup for static methods.  */
    private MemberFinder<Method> staticMethods =  MemberFinderFactory.createStaticMethods();

    private Class<?> caller = TargetFinderTest.class;

    @Test
    public void testSpeStaticReturnType() throws Exception {
        Method target = findSpeStaticReturnType();
        assertMethod(target, SpecRetType.class, METHOD_NAME, List.class);
    }

    public Method findSpeStaticReturnType() throws Exception {
        return findTarget(caller, SpecRetType.class, METHOD_NAME, Collection.class);
    }

    @Test
    public void testDisambiguateReturnType() throws Exception {
        // should select B.foo#()List over A.foo#()Collection
        Method target = findDisambiguateReturnType();
        assertMethod(target, DisamRetType.class, METHOD_NAME, List.class);
    }

    public Method findDisambiguateReturnType() throws Exception {
        return findTarget(caller, DisamRetType.class, METHOD_NAME, Collection.class);
    }

    /** long -> Integer on return type. */
    @Test
    public void testUnboxWideRetType() throws Exception {
        Method target = findUnboxWideRetType();
        assertMethod(target, WrapperRetType.class, METHOD_NAME, Integer.class);
    }

    public Method findUnboxWideRetType() throws Exception {
        return findTarget(caller, WrapperRetType.class, METHOD_NAME, long.class);
    }

    /** int -> Integer on return type. */
    @Test
    public void testUnboxeRetType() throws Exception {
        Method target = findUnboxeRetType();
        assertMethod(target, WrapperRetType.class, METHOD_NAME, Integer.class);
    }

    public Method findUnboxeRetType() throws Exception {
        return findTarget(caller, WrapperRetType.class, METHOD_NAME, int.class);
    }

    /** Integer -> long on return type. */
    @Test
    public void testBoxNarrowParam() throws Exception {
        Method target = findBoxNarrowParam();
        assertMethod(target, PrimParamType.class, METHOD_NAME, void.class, long.class);
    }

    public Method findBoxNarrowParam() throws Exception {
        return findTarget(caller, PrimParamType.class, METHOD_NAME, void.class, Integer.class);
    }


    /** Long -> long on param type. */
    @Test
    public void testUnboxParam() throws Exception {
        Method target = findUnboxParam();
        assertMethod(target, PrimParamType.class, METHOD_NAME, void.class, long.class);
    }

    public Method findUnboxParam() throws Exception {
        return findTarget(caller, PrimParamType.class, METHOD_NAME, void.class, Long.class);
    }

    /** Integer -> Number */
    @Test
    public void testOverloadingGen() throws Exception {
        Method target = findOverloadingGen();
        assertMethod(target, MethodOverloadingPrimWrap.class, METHOD_NAME, void.class, Number.class);
    }

    public Method findOverloadingGen() throws Exception {
        return findTarget(caller, MethodOverloadingPrimWrap.class, METHOD_NAME, void.class, Integer.class);
    }

    /** int -> long. */
    @Test
    public void testOverloadingWide() throws Exception {
        Method target = findOverloadingWide();
        assertMethod(target, MethodOverloadingPrimWrap.class, METHOD_NAME, void.class, long.class);
    }

    public Method findOverloadingWide() throws Exception {
        return findTarget(caller, MethodOverloadingPrimWrap.class, METHOD_NAME, void.class, int.class);
    }

    /** ArrayList -> AbstractList selected over Collection, List, ...*/
    @Test
    public void testOverloadingArrayList() throws Exception {
        Method target = findOverloadingArrayList();
        assertMethod(target, MethodOverloadingCollections.class, METHOD_NAME, void.class, AbstractList.class);
    }

    public Method findOverloadingArrayList() throws Exception {
        return findTarget(caller, MethodOverloadingCollections.class, METHOD_NAME, void.class, ArrayList.class);
    }

    /** Set -> Collection over e.g. Iterable */
    @Test
    public void testOverloadingSet() throws Exception {
        Method target = findOverloadingSet();
        assertMethod(target, MethodOverloadingCollections.class, METHOD_NAME, void.class, Collection.class);
    }

    public Method findOverloadingSet() throws Exception {
        return findTarget(caller, MethodOverloadingCollections.class, METHOD_NAME, void.class, Set.class);
    }

    /** ArrayList ambiguous with AbstractList and Serialisable */
    @Test
    public void testOverloadingAmbiguous() throws Exception {
        Method target = findOverloadingAmbiguous();
        assertNull(target);
    }

    public Method findOverloadingAmbiguous() throws Exception {
        return findTarget(caller, MethodOverloadingAmbiguous.class, METHOD_NAME, void.class, ArrayList.class);
    }

    /** Complex overloading */
    @Test
    public void testComplexOverloadingGrandParentOverloading() throws Exception {
        Method target = findComplexOverloadingGrandParentOverloading();
        // r type overloading - we select the most specific one (List over Collection)
        assertMethod(target, ParentHierarchy.class, METHOD_NAME, List.class, Collection.class);
    }

    public Method findComplexOverloadingGrandParentOverloading() throws Exception {
        return findTarget(caller, ChildHierarchy.class, METHOD_NAME, Collection.class, HashSet.class);
    }

    /** Complex overloading */
    @Test
    public void testComplexOverloadingParent() throws Exception {
        Method target = findComplexOverloadingParent();
        assertMethod(target, ParentHierarchy.class, METHOD_NAME, List.class, Collection.class);
    }

    public Method findComplexOverloadingParent() throws Exception {
        return findTarget(caller, ChildHierarchy.class, METHOD_NAME, List.class, HashSet.class);
    }


    /** Complex overloading */
    @Test
    public void testComplexOverloadingChild() throws Exception {
        // ret type ArrayList does not exist, but it is convertible from List
        Method target = findComplexOverloadingChild();
        assertMethod(target, ChildHierarchy.class, METHOD_NAME, List.class, AbstractList.class);
    }

    public Method findComplexOverloadingChild() throws Exception {
        return findTarget(caller, ChildHierarchy.class, METHOD_NAME, ArrayList.class, ArrayList.class);
    }


    private void assertMethod(final Method method,
                              final Class<?> owner,
                              final String methodName,
                              final Class<?> rType,
                              final Class<?>... params) {

        assertNotNull(method);
        assertEquals(rType, method.getReturnType());
        assertEquals(methodName, method.getName());
        assertEquals(owner, method.getDeclaringClass());
        assertEquals(params.length, method.getParameterTypes().length);
        assertArrayEquals(params, method.getParameterTypes());
    }

    private Method findTarget(
            final Class<?> caller,
            final Class<?> owner,
            final String methodName,
            final Class<?> rType,
            final Class<?>... params) throws Exception {

        final MethodType type = MethodType.methodType(rType, params);

        return staticMethods.find(caller, owner, methodName, type);
    }


}
