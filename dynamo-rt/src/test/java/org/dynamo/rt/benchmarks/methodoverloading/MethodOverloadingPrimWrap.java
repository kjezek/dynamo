/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.rt.benchmarks.methodoverloading;

import java.util.List;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class MethodOverloadingPrimWrap {


    // this method must be selected when boxing is disabled
    public void foo(Number a) {
    }

    // this method must be selected when boxing is enabled
    public void foo(long a) {
    }

    // ------

    public void foo(String a) {

    }

    public void foo(List a) {

    }

    public void test() {
        foo(new Integer(10));
    }

}
