/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.rt.benchmarks.overloadinghierarchy;

import java.util.AbstractCollection;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class ChildHierarchy extends ParentHierarchy implements ParentInterface {

    @Override
    public List foo(AbstractList c) {
        return null;
    }

    @Override
    public AbstractCollection foo(HashMap c) {
        return null;
    }


    void test() {
        ArrayList input = new ArrayList();

        foo(input);
    }
}
