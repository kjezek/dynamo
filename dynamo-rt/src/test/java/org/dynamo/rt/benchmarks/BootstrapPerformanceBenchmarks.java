/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.rt.benchmarks;

import org.dynamo.rt.DynamoBootstrap;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.ChainedOptionsBuilder;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Run performance benchmarks via JMH.
 * Measures the time needed for compilation (classic and dynamo).
 * @author jens dietrich
 */
@Measurement(iterations=30,timeUnit= TimeUnit.MILLISECONDS )
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations=15)
@BenchmarkMode(Mode.AverageTime)
@State(Scope.Thread)
public class BootstrapPerformanceBenchmarks {

    private static final Logger LOGGER = Logger.getLogger(DynamoBootstrap.class.getName());


    @Setup(Level.Trial)
    public void beforeTrial() throws Exception {
        LOGGER.info("PREPARE TRIAL");
    }

    @TearDown(Level.Trial)
    public void afterTrial() throws Exception {
        LOGGER.info("CLEANUP AFTER TRIAL");
    }

    @Benchmark
    public Method findSpeStaticReturnType() throws Exception {
        return new TargetFinderTest().findSpeStaticReturnType();
    }

    @Benchmark
    public Method findDisambiguateReturnType() throws Exception {
        return new TargetFinderTest().findDisambiguateReturnType();
    }

    /** long -> Integer on return type. */
    @Benchmark
    public Method findUnboxWideRetType() throws Exception {
        return new TargetFinderTest().findUnboxWideRetType();
    }

    /** int -> Integer on return type. */
    @Benchmark
    public Method findUnboxeRetType() throws Exception {
        return new TargetFinderTest().findUnboxeRetType();
    }


    /** Integer -> long on return type. */
    @Benchmark
    public Method findBoxNarrowParam() throws Exception {
        return new TargetFinderTest().findBoxNarrowParam();
    }


    /** Long -> long on param type. */
    @Benchmark
    public Method findUnboxParam() throws Exception {
        return new TargetFinderTest().findUnboxParam();
    }

    /** Integer -> Number */
    @Benchmark
    public Method findOverloadingGen() throws Exception {
        return new TargetFinderTest().findOverloadingGen();
    }

    /** int -> long. */
    @Benchmark
    public Method findOverloadingWide() throws Exception {
        return new TargetFinderTest().findOverloadingWide();
    }

    /** ArrayList -> AbstractList selected over Collection, List, ...*/
    @Benchmark
    public Method findOverloadingArrayList() throws Exception {
        return new TargetFinderTest().findOverloadingArrayList();
    }

    /** Set -> Collection over e.g. Iterable */
    @Benchmark
    public Method findOverloadingSet() throws Exception {
        return new TargetFinderTest().findOverloadingSet();
    }

    /** ArrayList ambiguous with AbstractList and Serialisable */
    @Benchmark
    public Method findOverloadingAmbiguous() throws Exception {
        return new TargetFinderTest().findOverloadingAmbiguous();
    }

    /** Complex overloading */
    @Benchmark
    public Method findComplexOverloadingGrandParentOverloading() throws Exception {
        return new TargetFinderTest().findComplexOverloadingGrandParentOverloading();
    }

    /** Complex overloading */
    @Benchmark
    public Method findComplexOverloadingParent() throws Exception {
        return new TargetFinderTest().findComplexOverloadingParent();
    }


    /** Complex overloading */
    @Benchmark
    public Method findComplexOverloadingChild() throws Exception {
        return new TargetFinderTest().findComplexOverloadingChild();
    }


    @Benchmark
    public Method findAll() throws Exception {
        Method m = null;
        m = new TargetFinderTest().findBoxNarrowParam();
        m = new TargetFinderTest().findComplexOverloadingChild();
        m = new TargetFinderTest().findComplexOverloadingGrandParentOverloading();
        m = new TargetFinderTest().findComplexOverloadingParent();
        m = new TargetFinderTest().findDisambiguateReturnType();
        m = new TargetFinderTest().findOverloadingAmbiguous();
        m = new TargetFinderTest().findOverloadingArrayList();
        m = new TargetFinderTest().findOverloadingGen();
        m = new TargetFinderTest().findOverloadingSet();
        m = new TargetFinderTest().findOverloadingWide();
        m = new TargetFinderTest().findSpeStaticReturnType();
        m = new TargetFinderTest().findUnboxeRetType();
        m = new TargetFinderTest().findUnboxParam();
        m = new TargetFinderTest().findUnboxWideRetType();

        return m;


    }

    public static void main(String[] args) throws RunnerException {
        ChainedOptionsBuilder opt = new OptionsBuilder()
                .include(BootstrapPerformanceBenchmarks.class.getSimpleName() + ".findAll")
                .jvmArgs("-Xmx2g", "-Xms2g")
                .forks(2)
                .shouldDoGC(true);

        if (args.length > 0) {
            opt = opt.output(args[0]);
        }

        new Runner(opt.build()).run();
    }

}
