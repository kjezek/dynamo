/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.rt;

import org.dynamo.rt.testdata.TestMultipleMethods;
import org.junit.Before;
import org.junit.Test;

import java.lang.invoke.CallSite;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.invoke.WrongMethodTypeException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Test of method finder bootstrap.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class MethodFindingBootstrapTest {

    /** Tested method name. */
    private static final String STRING_METHOD = "stringMethod";

    /** Tested method name. */
    private static final String LIST_METHOD = "listMethod";

    /** Tested method name. */
    private static final String LIST_STATIC_METHOD = "staticListMethod";

    /** Lookup*/
    private MethodHandles.Lookup lookup;

    /** Init. */
    @Before
    public void setUp() {
        lookup = MethodHandles.lookup();
    }

    @Test
    public void testExactStringMatch() throws Throwable {

        // first method parameter is the owner class.
        MethodType exactType = MethodType.methodType(String.class, TestMultipleMethods.class, String.class);
        CallSite callSite = DynamoBootstrap.bootstrapVirtual(lookup, STRING_METHOD, exactType);
        MethodHandle methodHandle = callSite.dynamicInvoker();

        Object result = (String) methodHandle.invokeExact(new TestMultipleMethods(), "World");

        assertEquals(String.class.getName(), result.getClass().getName());
        assertEquals("Hello World", result);
    }

    @Test
    public void testExactListMatch() throws Throwable {

        // first method parameter is the owner class.
        MethodType exactType = MethodType.methodType(List.class, TestMultipleMethods.class, List.class);
        CallSite callSite = DynamoBootstrap.bootstrapVirtual(lookup, LIST_METHOD, exactType);
        MethodHandle methodHandle = callSite.dynamicInvoker();

        Object result = (List) methodHandle.invokeExact(new TestMultipleMethods(), createList());

        // returned is actually ArrayList
        assertEquals(ArrayList.class.getName(), result.getClass().getName());
    }

    @Test
    public void testListConversionOk() throws Throwable {

        MethodType convertibleType = MethodType.methodType(Collection.class, TestMultipleMethods.class, ArrayList.class);
        CallSite callSite = DynamoBootstrap.bootstrapVirtual(lookup, LIST_METHOD, convertibleType);
        MethodHandle methodHandle = callSite.dynamicInvoker();

        // invoke performs conversion
        Object result = (List) methodHandle.invoke(new TestMultipleMethods(), createList());

        // returned is actually ArrayList
        assertEquals(ArrayList.class.getName(), result.getClass().getName());
    }

    @Test(expected = WrongMethodTypeException.class)
    public void testListConversionFail() throws Throwable {

        MethodType convertibleType = MethodType.methodType(Collection.class, TestMultipleMethods.class, ArrayList.class);
        CallSite callSite = DynamoBootstrap.bootstrapVirtual(lookup, LIST_METHOD, convertibleType);
        MethodHandle methodHandle = callSite.dynamicInvoker();

        // invoke exact must fail
        methodHandle.invokeExact(new TestMultipleMethods(), createList());
    }

    @Test(expected = NoSuchMethodError.class)
    public void testListConversionNotPossible() throws Throwable {

        MethodType convertibleType = MethodType.methodType(ArrayList.class, TestMultipleMethods.class, Collection.class);
        DynamoBootstrap.bootstrapVirtual(lookup, LIST_METHOD, convertibleType);
    }


    /** static method invoked as virtual. */
    @Test
    public void testExactStaticListMatch() throws Throwable {

        // first method parameter is the owner class.
        MethodType exactType = MethodType.methodType(List.class, TestMultipleMethods.class, List.class);
        CallSite callSite = DynamoBootstrap.bootstrapVirtual(lookup, LIST_STATIC_METHOD, exactType);
        MethodHandle methodHandle = callSite.dynamicInvoker();

        Object result = (List) methodHandle.invokeExact(new TestMultipleMethods(), createList());

        // returned is actually ArrayList
        assertEquals(ArrayList.class.getName(), result.getClass().getName());
    }

    /** static method invoked as static. */
    @Test
    public void testStaticInvokedStaticExactMatch() throws Throwable {

        // first method parameter is the owner class.
        MethodType exactType = MethodType.methodType(List.class, List.class);
        CallSite callSite = DynamoBootstrap.bootstrapStatic(lookup, LIST_STATIC_METHOD, exactType, TestMultipleMethods.class.getName());
        MethodHandle methodHandle = callSite.dynamicInvoker();

        Object result = (List) methodHandle.invokeExact(createList());

        // returned is actually ArrayList
        assertEquals(ArrayList.class.getName(), result.getClass().getName());
    }

    /** static method invoked as static. */
    @Test(expected = NoSuchMethodError.class)
    public void testNonStaticInvokedStatic() throws Throwable {

        // first method parameter is the owner class.
        MethodType exactType = MethodType.methodType(List.class, ArrayList.class);
        CallSite callSite = DynamoBootstrap.bootstrapStatic(lookup, LIST_METHOD, exactType, TestMultipleMethods.class.getName());
        MethodHandle methodHandle = callSite.dynamicInvoker();

        Object result = (List) methodHandle.invokeExact(createList());

        // returned is actually ArrayList
        assertEquals(ArrayList.class.getName(), result.getClass().getName());
    }

    /** static method invoked as non static. */
    @Test
    public void testStaticInvokedAsNonStatic() throws Throwable {

        // first method parameter is the owner class.
        MethodType exactType = MethodType.methodType(Integer.class, Integer.class, int.class);
        CallSite callSite = DynamoBootstrap.bootstrapVirtual(lookup, "valueOf", exactType);
        MethodHandle methodHandle = callSite.dynamicInvoker();

        Integer owner = 1;
        Integer value = (Integer) methodHandle.invokeExact(owner, 10);

        // returned is actually ArrayList
        assertEquals(Integer.valueOf(10), value);
    }

    /** invoke interface. */
    @Test
    public void testInvokeInterfaceExactMatch() throws Throwable {

        // first method parameter is the owner class.
        MethodType exactType = MethodType.methodType(boolean.class, List.class, Object.class);
        CallSite callSite = DynamoBootstrap.bootstrapInterface(lookup, "add", exactType);
        MethodHandle methodHandle = callSite.dynamicInvoker();

        List instance = createList();
        boolean result = (boolean) methodHandle.invokeExact(instance, new Object());

        assertTrue(result);
    }


    /** invoke constructor. */
    @Test
    public void testInvokeSpecialConstructor() throws Throwable {

        // first method parameter is the owner class.
        MethodType exactType = MethodType.methodType(ArrayList.class);
        CallSite callSite = DynamoBootstrap.bootstrapSpecialNew(lookup, ArrayList.class.getName(), exactType);
        MethodHandle methodHandle = callSite.dynamicInvoker();

        ArrayList result = (ArrayList) methodHandle.invokeExact();

        assertNotNull(result);
    }


    /** invoke super method. */
    @Test
    public void testInvokeSpecialSuper() throws Throwable {

//        TODO test somehow super call (invokespecial super)

        // first method parameter is the owner class.
//        MethodType exactType = MethodType.methodType(boolean.class, ArrayList.class, Object.class);
//        CallSite callSite = DynamoBootstrap.bootstrapSpecialSuper(lookup, "add", exactType);
//        MethodHandle methodHandle = callSite.dynamicInvoker();
//
//        ArrayList result = (ArrayList) methodHandle.invokeExact();
//
//        assertNotNull(result);
    }

    @Test
    public void testMethodHandleCreation() throws Throwable {

        MethodType t = MethodType.methodType(int.class);
        // "size" is in Collection, but may be referred from Arraylist
        // -> "findVirtual" will iterate the hierarchy for us
        MethodHandle handle = lookup.findVirtual(ArrayList.class, "size", t);

        assertNotNull(handle);

        int i = (int) handle.invoke(new ArrayList());

        assertEquals(0, i);
    }

    private List createList() {
        return new ArrayList<>();
    }

}
