/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.rt.testdata.distance.invocation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class Callee {

    void foo(Iterable p1, Collection p2, List p3, ArrayList p4) {
        System.out.println("foo 1");
    }

    void foo(ArrayList p1, List p2, Collection p3, Iterable p4) {
        System.out.println("foo 2");
    }

    void foo(Collection p1, Collection p2, Collection p3, Collection p4) {
        System.out.println("foo 3");
    }

    void foo(List p1, List p2, List p3, List p4) {
        System.out.println("foo 4");
    }

    void simple(List p1) {
        System.out.println("simple 2");
    }

    void simple(Iterable p1) {
        System.out.println("simple 1");
    }



}
