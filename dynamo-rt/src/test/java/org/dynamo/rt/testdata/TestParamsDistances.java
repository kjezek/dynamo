/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.rt.testdata;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Test data containing a set of equal methods with various
 * inheritance distance of parameters
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public interface TestParamsDistances {

    void oneParam(ArrayList a);
    void oneParam(List a);
    void oneParam(Collection a);
    void oneParam(Iterable a);

    // 0 --> + 4
    void moreParams(Iterable    a, Collection b, List       c, ArrayList d);
    // +4 --> 0
    void moreParams(ArrayList   a, List       b, Collection c, Iterable d);

    // 0 --> +2 +2
    void moreParams(Iterator a, Iterator b, Collection c, Collection d);
    // +2 +2 --> O
    void moreParams(Collection a, Collection b, Iterator c, Iterator d);

}
