/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.rt.testdata.distance.invocation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class Caller {

    private static final Iterable I = null;
    private static final Collection C = null;
    private static final List L = null;
    private static final ArrayList A = null;


    public static void main(String[] args) {

        Callee c = new Callee();

        c.foo(I, C, L, A);
        c.foo(A, L, C, I);
        c.foo(C, C, C, C);
        c.foo(L, L, L, L);

        System.out.println("-----------");

//        c.foo(A, A, A, A);

        c.foo(A, A, A, I);

        System.out.println("A method with closest subtype is selected");
        // see  http://docs.oracle.com/javase/specs/jls/se7/html/jls-15.html#jls-15.12.2 !!
        c.simple(I);
        c.simple(C);
        c.simple(A);
    }
}
