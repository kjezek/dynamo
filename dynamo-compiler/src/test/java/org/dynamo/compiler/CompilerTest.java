/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.compiler;

import org.dynamo.compiler.testdata.TestClass;
import org.dynamo.tests.PublicClassLoader;
import org.dynamo.tests.TransformationTester;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.asm.Opcodes;

import java.io.File;
import java.io.IOException;
import java.lang.*;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.dynamo.tests.TransformationTester.assertTransformation;
import static org.junit.Assert.assertEquals;

/**
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class CompilerTest {


    /** Default filter allowing all on the callsite and blocking java packages on the target site. */
    public static final String DEFAULT_FILTER_PATTERN = ""
            + "+callsite *,"
            + "+target *"
            + "-target java.*,"
            + "-target javax.*,"
            + "-target sun.*,"
            + "-target com.sun.*,"
            + "-target com.oracle.*,"
            + "-target org.ietf.*,"
            + "-target org.omg.*,"
            + "-target org.w3c.*,"
            + "-target org.xml.*";

    /**
     * Tested class package.
     */
    private static final String TESTED_CLASS_NAME = TestClass.class.getName();

    public static final String OUT_DIR = "target/dynamo-compiled-classes";

    public static final String SRC_DIR = "src/test/java/org/dynamo/compiler/testdata/";

    @Before
    public void setUp() {
        // cleanup
        String classPath = TESTED_CLASS_NAME.replace('.', '/') + ".class";
        new File(new File(OUT_DIR), classPath).delete();
    }

    /**
     * Test compilation with standard filter.
     */
    @Test
    public void testCompileStandardFilter() throws Exception {
        compile(DEFAULT_FILTER_PATTERN);

        // only Java API used - no transformation at all
        TransformationTester.Rules rules = new TransformationTester.Rules() {
            @Override
            public void rules(TransformationTester.RulesUpdater updater) {
                updater.addMethod(List.class.getName(), "add", Opcodes.INVOKEINTERFACE);
                updater.addMethod(ArrayList.class.getName(), "add", Opcodes.INVOKEVIRTUAL);
                updater.addMethod(Integer.class.getName(), "parseInt", Opcodes.INVOKESTATIC);
                updater.addMethod(Integer.class.getName(), "bitCount", Opcodes.INVOKESTATIC);
                updater.addMethod(LinkedList.class.getName(), "<init>", Opcodes.INVOKESPECIAL);
                updater.addMethod(ArrayList.class.getName(), "<init>", Opcodes.INVOKESPECIAL);
                updater.addMethod(Date.class.getName(), "getTime", Opcodes.INVOKEVIRTUAL);
            }
        };

        byte[] bytecode = readCompiled();
        assertTransformation(rules, bytecode);
    }

    /**
     * Test compilation with user filter on target.
     */
    @Test
    public void testCompileUserFilterTarget() throws Exception {
        compile("+target java.util.ArrayList, +target java.lang.Integer#bitCount, +callsite *");

        // only selected API transformed
        TransformationTester.Rules rules = new TransformationTester.Rules() {
            @Override
            public void rules(TransformationTester.RulesUpdater updater) {
                updater.addMethod(List.class.getName(), "add", Opcodes.INVOKEINTERFACE);
                updater.addMethod(ArrayList.class.getName(), "add", Opcodes.INVOKEDYNAMIC);
                updater.addMethod(Integer.class.getName(), "parseInt", Opcodes.INVOKESTATIC);
                updater.addMethod(Integer.class.getName(), "bitCount", Opcodes.INVOKEDYNAMIC);
                updater.addMethod(LinkedList.class.getName(), "<init>", Opcodes.INVOKESPECIAL);
                updater.addMethod(ArrayList.class.getName(), "<init>", Opcodes.INVOKEDYNAMIC);
                updater.addMethod(Date.class.getName(), "getTime", Opcodes.INVOKEVIRTUAL);
            }
        };

        byte[] bytecode = readCompiled();
        assertTransformation(rules, bytecode);
    }

    /**
     * Test compilation with user callsite filter.
     */
    @Test
    public void testCompileUserFilterCallsite() throws Exception {
        compile("+target *, +callsite *#two");

        // only selected API transformed
        TransformationTester.Rules rules = new TransformationTester.Rules() {
            @Override
            public void rules(TransformationTester.RulesUpdater updater) {
                updater.addMethod(List.class.getName(), "add", Opcodes.INVOKEINTERFACE);
                updater.addMethod(ArrayList.class.getName(), "add", Opcodes.INVOKEVIRTUAL);
                updater.addMethod(Integer.class.getName(), "parseInt", Opcodes.INVOKESTATIC);
                updater.addMethod(Integer.class.getName(), "bitCount", Opcodes.INVOKESTATIC);
                updater.addMethod(LinkedList.class.getName(), "<init>", Opcodes.INVOKESPECIAL);
                updater.addMethod(ArrayList.class.getName(), "<init>", Opcodes.INVOKESPECIAL);
                updater.addMethod(Date.class.getName(), "getTime", Opcodes.INVOKEDYNAMIC);
            }
        };

        byte[] bytecode = readCompiled();
        assertTransformation(rules, bytecode);
    }

    /**
     * Invoke compiled class.
     */
    @Test
    public void testInvoke() throws Exception {

        compile("+target *, +callsite *");

        byte[] bytecode = readCompiled();
        PublicClassLoader classLoader = new PublicClassLoader(getClass().getClassLoader());
        Class clazz = classLoader.defineClass(TESTED_CLASS_NAME.replace('/', '.'), bytecode);

        @SuppressWarnings("unchecked")
        Method method1 = clazz.getMethod("one", String[].class);
        int r1 = (int) method1.invoke(clazz, new String[][]{new String[]{}});

        assertEquals(1, r1);

        @SuppressWarnings("unchecked")
        Method method2 = clazz.getMethod("two", String[].class);
        int r2 = (int) method2.invoke(clazz, new String[][]{new String[]{}});

        assertEquals(2, r2);
    }

    /**
     * Invoke compilation by invoking the main method of the compiler
     * by Java reflection.
     *
     * @param filter user filter.
     * @throws Exception
     */
    private void compile(String filter) throws Exception {

        String[] params = new String[]{
                        "-sourcepath", SRC_DIR,
                        "-d", OUT_DIR,
                        "-filter", filter};

        Compiler.main(params);

    }

    /**
     * Read compiled byte-code.
     * @return byte code
     */
    private byte[] readCompiled() throws IOException {
        String classPath = TESTED_CLASS_NAME.replace('.', '/') + ".class";
        return FileUtils.readFileToByteArray(new File(new File(OUT_DIR), classPath));
    }
}
