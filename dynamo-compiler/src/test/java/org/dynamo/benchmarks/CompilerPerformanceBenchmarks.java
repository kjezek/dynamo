/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.benchmarks;

import org.apache.commons.io.FileUtils;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.ChainedOptionsBuilder;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.dynamo.benchmarks.Commons.*;

/**
 * Run performance benchmarks via JMH.
 * Measures the time needed for compilation (classic and dynamo).
 * @author jens dietrich
 */
@Measurement(iterations=30,timeUnit= TimeUnit.MILLISECONDS )
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations=15)
@BenchmarkMode(Mode.AverageTime)
@State(Scope.Thread)
public class CompilerPerformanceBenchmarks {

    private static final Logger LOGGER = LoggerFactory.getLogger("dynamo-benchmark-findTarget");
    
    private Map<String,File> libV1s = new HashMap<>();
    private Map<String,File> libV2s = new HashMap<>();

    @Setup(Level.Trial)
    public void beforeTrial() throws Exception {
        LOGGER.info("PREPARE TRIAL");
        SOURCE_ROOT = "src/test/resources/"; // JMH locates relative folders diff to junit
        LOGGER.debug(new File(SOURCE_ROOT).getAbsolutePath());

        assert new File(SOURCE_ROOT).exists();

        System.out.println(COMPILER_PERFORMANCE_TEST_NAMES.length + " scenarios will be invoked");

        for (String name : COMPILER_PERFORMANCE_TEST_NAMES) {
            File BIN_DIR = new File(BIN_ROOT,name);
            FileUtils.deleteDirectory(BIN_DIR);
            BIN_DIR.mkdirs(); // reset
            File SRC_DIR = new File(SOURCE_ROOT,name);

            // compile jars
            File lib1 = javacAndJar(BIN_DIR, new File(SRC_DIR, "version1/lib/Foo.java"), "v1",LOGGER);
            assert lib1.exists();
            libV1s.put(name,lib1);

            File lib2 = javacAndJar(BIN_DIR, new File(SRC_DIR, "version2/lib/Foo.java"), "v2",LOGGER);
            assert lib2.exists();
            libV2s.put(name, lib2);
        }
    }

    @TearDown(Level.Trial)
    public void afterTrial() throws Exception {
        System.out.println("CLEANUP AFTER TRIAL");
        for (String name:Commons.SUCCESS_TEST_NAMES) {
            File bin = new File(BIN_ROOT,name);
            FileUtils.deleteDirectory(bin);
        }
    }

    private boolean compileWithDynamo(String[] benchmarkNames) throws Exception {

        boolean result = true;
        for (String name:benchmarkNames) {
            File bin = new File(BIN_ROOT, name);
            File src = new File(SOURCE_ROOT,name);
            File libV1 = this.libV1s.get(name);
            File binDynamo = dynamo(bin, new File(src, "client/Main.java"),libV1,LOGGER);
            result = result && binDynamo!=null;
        }
        return result;  // to prevent JVM from optimising this
    }

    /**
     * Note that we are also using dynamo, but skip instrumentation !
     * @param benchmarkNames
     * @return
     * @throws Exception
     */
    private boolean compileClassic(String[] benchmarkNames) throws Exception {

        boolean result = true;
        for (String name:benchmarkNames) {
            File bin = new File(BIN_ROOT, name);
            File src = new File(SOURCE_ROOT,name);
            File libV1 = this.libV1s.get(name);
            File binDynamo = dynamo(bin, new File(src, "client/Main.java"), libV1, LOGGER, "-classic");
            result = result && binDynamo!=null;
        }
        return result;  // to prevent JVM from optimising this
    }

    @Benchmark
    // @OperationsPerInvocation(49)
    public boolean compileAllWithDynamo() throws Exception {
        return compileWithDynamo(COMPILER_PERFORMANCE_TEST_NAMES);
    }

    @Benchmark
    // @OperationsPerInvocation(49)
    public boolean compileAllWithJavac() throws Exception {
        return compileClassic(COMPILER_PERFORMANCE_TEST_NAMES);
    }

//    @Benchmark
//    public boolean compileInvokeSpecialNewWithDynamo() throws Exception {
//        return compileWithDynamo(Commons.INVOKESPECIAL_NEW_TEST_NAMES);
//    }
//
//    @Benchmark
//    public boolean compileInvokeSpecialNewWithJavac() throws Exception {
//        return compileClassic(Commons.INVOKESPECIAL_NEW_TEST_NAMES);
//    }


    public static void main(String[] args) throws RunnerException {

        ChainedOptionsBuilder opt = new OptionsBuilder()
                .include(CompilerPerformanceBenchmarks.class.getSimpleName())
                .jvmArgs("-Xmx2g", "-Xms2g")
                .forks(2)
                .shouldDoGC(true);
//                .output("benchmark.tex");

        if (args.length > 0) {
            opt = opt.output(args[0]);
        }

        new Runner(opt.build()).run();
    }

}
