/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.benchmarks;

import org.apache.commons.io.FileUtils;
import org.dynamo.compiler.Compiler;
import org.slf4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import static junit.framework.TestCase.fail;

/**
 * Constant definitions used by several classes in this package.
 *
 * @author jens dietrich
 */
public class Commons {


    public static String SOURCE_ROOT = "src/test/resources/"; // not final, benchmarks use different context root folder !
    public static final String BIN_ROOT = ".benchmarks/";
    public static final String PATH_SEPARATOR = System.getProperty("path.separator");

    private static String DYNAMO_VERSION;

    static {
        try {
            DYNAMO_VERSION = new Commons().getProjectVersion();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static final String DYNAMO_RT_PATH = System.getProperty("user.home") + "/.m2/repository/org/dynamo/dynamo-rt/" + DYNAMO_VERSION + "/dynamo-rt-" + DYNAMO_VERSION + ".jar";
    /**
     * Path to local repo where compiled dynamo is. The path is valid on Linux and Mac.
     */
    public static final File DYNAMO_RT = new File(DYNAMO_RT_PATH);


    public static final String[] SUCCESS_TEST_NAMES = new String[]{
            "invokeinterface_box_gen_param",
            "invokeinterface_box_narrow_ret",
            "invokeinterface_box_param",
            "invokeinterface_box_ret",
            "invokeinterface_gen_array_cloneable_param",
            "invokeinterface_gen_array_object_param",
            "invokeinterface_gen_array_param",
            "invokeinterface_gen_array_serialis_param",
            "invokeinterface_gen_param",
            "invokeinterface_invokestatic",
            "invokeinterface_invokevirtual",
//            "invokeinterface_method_pull_up",      // this is compatible anyway
            "invokeinterface_narrow_ret",
            "invokeinterface_spec_array_ret",
            "invokeinterface_spec_ret",
            "invokeinterface_unbox_param",
            "invokeinterface_unbox_ret",
            "invokeinterface_unbox_widen_param",
            "invokeinterface_widen_param",
            "invokespecial_new_box_gen_param",
            "invokespecial_new_box_param",
            "invokespecial_new_gen_param",
            "invokespecial_new_unbox_param",
            "invokespecial_new_unbox_widen_param",
            "invokespecial_new_widen_param",
            "invokespecial_super_narrow_ret",
            "invokespecial_super_box_gen_param",
            "invokespecial_super_box_param",
            "invokespecial_super_box_ret",
            "invokespecial_super_gen_param",
            "invokespecial_super_spec_array_ret",
            "invokespecial_super_spec_ret",
            "invokespecial_super_unbox_param",
            "invokespecial_super_unbox_ret",
            "invokespecial_super_widen_param",
            "invokestatic_box_gen_param",
            "invokestatic_box_narrow_ret",
            "invokestatic_box_param",
            "invokestatic_box_ret",
            "invokestatic_gen_array_param",
            "invokestatic_gen_param",
            "invokestatic_multiple_gen_param",
            "invokestatic_narrow_ret",
            "invokestatic_spec_array_ret",
            "invokestatic_spec_ret",
            "invokestatic_unbox_param",
            "invokestatic_unbox_ret",
            "invokestatic_unbox_widen_param",
            "invokestatic_widen_param",
//            "invokevirtual_ambig_param",
//            "invokevirtual_ambig_two_params",
            "invokevirtual_box_gen_param",
            "invokevirtual_box_narrow_ret",
            "invokevirtual_box_param",
            "invokevirtual_box_ret",
            "invokevirtual_gen_array_param",
            "invokevirtual_gen_param",
            "invokevirtual_invokeinterface",
            "invokevirtual_invokestatic",
            "invokevirtual_narrow_ret",
            "invokevirtual_spec_array_ret",
            "invokevirtual_spec_ret",
//            "invokevirtual_spec_void_ret",    // not mentioned in the paper
            "invokevirtual_unbox_param",
            "invokevirtual_unbox_ret",
            "invokevirtual_unbox_widen_param",
            "invokevirtual_widen_param"

    };

    public static final String[] FAILURE_TESTS = new String[]{
            "invokevirtual_ambig_param",
            "invokevirtual_ambig_two_params",
    };

    public static final String[] INVOKESPECIAL_NEW_TEST_NAMES = new String[]{
            "invokespecial_new_gen_param",
            "invokespecial_new_box_param",
            "invokespecial_new_widen_param",
            "invokespecial_new_unbox_widen_param"
    };

    public static String[] COMPILER_PERFORMANCE_TEST_NAMES;

    static {
        // merge all names
        List<String> tmpList = new ArrayList<>();
        tmpList.addAll(Arrays.asList(SUCCESS_TEST_NAMES));
        tmpList.addAll(Arrays.asList(FAILURE_TESTS));

        // remove duplications
        // duplications are combination of tests that have the same client class
        // i.e. some clients do not change for multiple library evolutions.
        tmpList.removeAll(Arrays.asList(
                "invokevirtual_widen_param", //  invokevirtual_box_param
                "invokevirtual_widen_param", //  invokevirtual_box_gen_param
                "invokespecial_new_unbox_param", //  invokespecial_new_unbox_widen_param
                "invokeinterface_invokestatic", //  invokeinterface_invokevirtual
                "invokeinterface_box_ret", //  invokeinterface_narrow_ret
                "invokevirtual_ambig_param", //  invokevirtual_gen_param
                "invokespecial_new_box_gen_param", //  invokespecial_new_widen_param
                "invokespecial_new_box_gen_param", //  invokespecial_new_box_param
                "invokevirtual_box_param", //  invokevirtual_box_gen_param
                "invokespecial_super_box_gen_param", //  invokespecial_super_box_param
                "invokeinterface_widen_param", //  invokeinterface_box_param
                "invokeinterface_widen_param", //  invokeinterface_box_gen_param
                "invokeinterface_gen_array_object_param", //  invokeinterface_gen_array_serialis_param
                "invokeinterface_gen_array_object_param", //  invokeinterface_gen_array_cloneable_param
                "invokeinterface_gen_array_object_param", //  invokeinterface_gen_array_param
                "invokeinterface_box_param", //  invokeinterface_box_gen_param
                "invokestatic_widen_param", //  invokestatic_box_param
                "invokestatic_widen_param", //  invokestatic_box_gen_param
                "invokeinterface_gen_array_serialis_param", //  invokeinterface_gen_array_cloneable_param
                "invokeinterface_gen_array_serialis_param", //  invokeinterface_gen_array_param
                "invokeinterface_gen_array_cloneable_param", //  invokeinterface_gen_array_param
                "invokestatic_box_param", //  invokestatic_box_gen_param
                "invokespecial_new_widen_param" //  invokespecial_new_box_param

        ));

        COMPILER_PERFORMANCE_TEST_NAMES = tmpList.toArray(new String[tmpList.size()]);
    }


    public static void redirectStreams(Process p, Logger LOGGER) throws Exception {

        if (LOGGER.isDebugEnabled()) {
            String line = "";
            String OFFSET = "   ";
            LOGGER.debug(OFFSET + "=== external tool output follows: ===");

            BufferedReader error = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            while ((line = error.readLine()) != null) {
                LOGGER.debug(OFFSET + line);
            }
            error.close();


            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line = input.readLine()) != null) {
                LOGGER.debug(OFFSET + line);
            }
            input.close();
        }
    }

    /**
     * Compile a single source code file and jar it. Return the jar file.
     * Assume that javac and jar are in the path !!
     *
     * @param bin     the bin folder to store classes and jars
     * @param src     the source code file
     * @param version the version string to append to the jar file
     * @return the jar file
     */
    public static File javacAndJar(File bin, File src, String version, Logger LOGGER) throws Exception {
        LOGGER.debug("Compile and jar " + src.getAbsolutePath() + " NOTE: javac and jar must be in path");
        File CLASSES = new File(bin, "classes");
        FileUtils.deleteDirectory(CLASSES);
        CLASSES.mkdirs();
        assert CLASSES.exists();

        // javac
        try {
            String cmd = "javac " + src.getAbsolutePath() + " -d " + CLASSES.getAbsolutePath();
            LOGGER.debug("Exe: " + cmd);
            Process process = Runtime.getRuntime().exec(cmd);
            process.waitFor();
            redirectStreams(process, LOGGER);
            assert 0 == process.exitValue();
        } catch (IOException e) {
            LOGGER.error("Error compiling benchmark library from source file " + src.getAbsolutePath(), e);
            fail();
        }

        // jar
        String jarName = "lib-" + version + ".zip";
        try {
            String cmd = "jar cvf ../" + jarName + " .";
            LOGGER.debug("Exe: " + cmd);
            Process process = Runtime.getRuntime().exec(cmd, new String[]{}, CLASSES);
            process.waitFor();
            redirectStreams(process, LOGGER);
            assert 0 == process.exitValue();
        } catch (IOException e) {
            LOGGER.error("Error jaring benchmark library from source file " + src.getAbsolutePath(), e);
            fail();
        } finally {
            FileUtils.deleteDirectory(CLASSES);
        }

        return new File(bin, jarName);
    }

    /**
     * Compile a single source code file into a folder.
     * Assume that javac is in the path !!
     *
     * @param bin    the bin folder to store classes and jars
     * @param src    the source code file
     * @param jar4cp a single jar that forms the classpath
     * @return the folder containing the compiled class
     */
    public static File javac(File bin, File src, File jar4cp, Logger LOGGER) throws Exception {
        LOGGER.debug("Compile " + src.getAbsolutePath() + " NOTE: javac and jar must be in path");
        File CLASSES = new File(bin, "classes");
        CLASSES.delete();
        CLASSES.mkdirs();
        assert CLASSES.exists();

        // javac
        try {
            String cmd = "javac " + src.getAbsolutePath() + " -d " + CLASSES.getAbsolutePath() + (jar4cp == null ? "" : (" -cp " + jar4cp.getAbsolutePath()));
            LOGGER.debug("Exe: " + cmd);
            Process process = Runtime.getRuntime().exec(cmd);
            process.waitFor();
            redirectStreams(process, LOGGER);
            assert 0 == process.exitValue();
        } catch (IOException e) {
            LOGGER.error("Error compiling benchmark library from source file " + src.getAbsolutePath(), e);
            fail();
        }

        return CLASSES;
    }


    /**
     * Compile a single source code file into a folder using the dynamo compiler.
     *
     * @param bin    the bin folder to store classes and jars
     * @param src    the source code file
     * @param jar4cp a single jar that forms the classpath
     * @param params additional runtimeparameters
     * @return the folder containing the compiled class
     */
    public static File dynamo(File bin, File src, File jar4cp, Logger LOGGER, String... params) throws Exception {
        LOGGER.debug("Compile with dynamo " + src.getAbsolutePath());
        File CLASSES = new File(bin, "dynamo");
        CLASSES.delete();
        CLASSES.mkdirs();
        assert CLASSES.exists();

        String[] args = {
                "-" + Compiler.CLASSPATH, jar4cp.getAbsolutePath(),
                "-" + Compiler.DESTINATION, CLASSES.getAbsolutePath(),
                "-" + Compiler.SOURCE_PATH, src.getParentFile().getAbsolutePath()
        };
        String[] args2 = new String[args.length + params.length];
        System.arraycopy(args, 0, args2, 0, args.length);
        for (int i = 0; i < params.length; i++) {
            args2[i + args.length] = params[i];
        }

        // javac
        try {
            Compiler.main(args2);
        } catch (IOException e) {
            LOGGER.error("Error compiling benchmark library from source file " + src.getAbsolutePath(), e);
            fail();
        }

        return CLASSES;
    }

    /**
     * Execute a java class with a single library classpath.
     *
     * @param bin    the folder where the lib is
     * @param jar4cp the class path multiple jars
     * @param main   the main class
     * @return
     */
    public static int java(File bin, String main, Logger LOGGER, File... jar4cp) throws Exception {

        String CP = bin.getAbsolutePath();
        for (File jar : jar4cp) {
            CP = CP + PATH_SEPARATOR + jar.getAbsolutePath();
        }

        LOGGER.debug("Run " + main + " with classpath " + CP);

        // java
        String cmd = "java -cp " + CP + " " + main;
        LOGGER.debug("Exe: " + cmd);
        try {
            Process process = Runtime.getRuntime().exec(cmd);
            process.waitFor();
            redirectStreams(process, LOGGER);
            return process.exitValue();
        } catch (IOException e) {
            LOGGER.error("Error running " + cmd, e);
            fail();
            return -1;
        }
    }

    public String getProjectVersion() throws IOException {
        String path = "/project.properties";
        InputStream stream = getClass().getResourceAsStream(path);
        Properties props = new Properties();
        props.load(stream);
        stream.close();
        return (String) props.get("version");
    }
}
