/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.benchmarks;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Run all benchmark tests.
 * @author jens dietrich
 */

@RunWith(Parameterized.class)
public class BenchmarkTest {

    private static final Logger LOGGER = LoggerFactory.getLogger("dynamo-benchmark-tests");

    @BeforeClass
    public static void staticInit() throws Exception {
        File SRC_DIR = new File(Commons.SOURCE_ROOT);
        File BIN_DIR = new File(Commons.BIN_ROOT);

        assert new File(Commons.SOURCE_ROOT).exists();
        LOGGER.debug("Running benchmarks.benchmarks from source folder: " + SRC_DIR.getAbsolutePath());
        LOGGER.debug("Binaries will be created in: " + BIN_DIR.getAbsolutePath());

        FileUtils.deleteDirectory(BIN_DIR);
        BIN_DIR.mkdirs();

        System.out.println("Dynamo-rt path: " + Commons.DYNAMO_RT_PATH);
        LOGGER.debug("Dynamo-rt path: " + Commons.DYNAMO_RT_PATH);
        assertTrue("Compiled dynamo-rt must exist in local Maven repo.", Commons.DYNAMO_RT.exists());
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Set<String> names = new HashSet<>();

        Collections.addAll(names, Commons.SUCCESS_TEST_NAMES);
        Collections.addAll(names, Commons.FAILURE_TESTS);

        Collection<Object[]> coll = new HashSet<>();
        for (String testName:names) {
            coll.add(new String[]{testName});
        }

        return coll;
    }


    private String testName = null;

    public BenchmarkTest(String testName) {
        super();
        this.testName = testName;
    }

    /**
     * Run the benchmark with the given name.
     * @param name
     */
    private void runBenchmark(String name) throws Exception {

        LOGGER.debug("Running benchmark: " + name);
        File SRC_DIR = new File(Commons.SOURCE_ROOT,name);
        File BIN_DIR = new File(Commons.BIN_ROOT,name);

        assert new File(Commons.SOURCE_ROOT).exists();
        BIN_DIR.mkdirs();

        // step 1 - javac application and both versions of library
        File lib1 = Commons.javacAndJar(BIN_DIR, new File(SRC_DIR, "version1/lib/Foo.java"), "v1", LOGGER);
        assertTrue(lib1.exists());

        File lib2 = Commons.javacAndJar(BIN_DIR, new File(SRC_DIR, "version2/lib/Foo.java"), "v2", LOGGER);
        assertTrue(lib2.exists());

        File bin = Commons.javac(BIN_DIR, new File(SRC_DIR, "client/Main.java"), lib1, LOGGER); // compiled with lib1 !!
        assertTrue(bin.exists());
        assertTrue(bin.isDirectory());

        // step 2 - findTarget program with lib1 - this should work
        int status = Commons.java(bin, "Main", LOGGER, lib1);
        assertEquals("Running Main compiled with javac against version1 of lib failed for benchmark " + name, 0, status);

        // step 3 - findTarget program with lib2 - this should not work
        status = Commons.java(bin, "Main", LOGGER, lib2);
        assertNotEquals("Running Main compiled with javac against version2 of lib for benchmark " + name + " succeeded but should fail with a linkage error", 0, status);

        // step 4 - compile program with dynamo against lib1
        File binDynamo = Commons.dynamo(BIN_DIR, new File(SRC_DIR, "client/Main.java"), lib1, LOGGER); // compiled with lib1 !!
        assertTrue(binDynamo.exists());
        assertTrue(binDynamo.isDirectory());

        // step 5 findTarget program compiled with dynamo against lib1
        status = Commons.java(binDynamo, "Main", LOGGER, lib1, Commons.DYNAMO_RT);
        assertEquals("Running main compiled with dynamo against version1 of lib failed for benchmark " + name, 0, status);

        // step 6 the interesting part -  findTarget program compiled with dynamo against lib2
        status = Commons.java(binDynamo, "Main", LOGGER, lib2, Commons.DYNAMO_RT);

        if (Arrays.asList(Commons.FAILURE_TESTS).contains(name)) {
            assertEquals("Running main compiled with dynamo against version2 of lib did not fail for benchmark " + name, 1, status);
        } else {
            assertEquals("Running main compiled with dynamo against version2 of lib failed for benchmark " + name, 0, status);
        }

    }

    @Test
    public void test() throws Exception {
        runBenchmark(testName);
    }

    /*
    @Test
    public void test_invokestatic_spec_ret() throws Exception {
        runBenchmark("invokestatic_gen_param");
    }

    @Test
    public void test_invokestatic_gen_param() throws Exception {
        runBenchmark("invokestatic_gen_param");
    }

    @Test
    public void test_invokestatic_box_narrow_ret() throws Exception {
        runBenchmark("invokestatic_box_narrow_ret");
    }

    @Test
    public void test_invokestatic_box_param() throws Exception {
        runBenchmark("invokestatic_box_param");
    }

    @Test
    public void test_invokestatic_box_ret() throws Exception {
        runBenchmark("invokestatic_box_ret");
    }

    @Test
    public void test_invokestatic_narrow_ret() throws Exception {
        runBenchmark("invokestatic_narrow_ret");
    }

    @Test
    public void test_invokestatic_unbox_param() throws Exception {
        runBenchmark("invokestatic_unbox_param");
    }

    @Test
    public void test_invokestatic_unbox_ret() throws Exception {
        runBenchmark("invokestatic_unbox_ret");
    }

    @Test
    public void test_invokestatic_unbox_widen_param() throws Exception {
        runBenchmark("invokestatic_unbox_widen_param");
    }

    @Test
    public void test_invokestatic_widen_param() throws Exception {
        runBenchmark("invokestatic_widen_param");
    }

    @Test
    public void test_invokeinterface_gen_param() throws Exception {
        runBenchmark("invokeinterface_gen_param");
    }

    @Test
    public void test_invokeinterface_spec_ret() throws Exception {
        runBenchmark("invokeinterface_spec_ret");
    }

    @Test
    public void test_invokevirtual_spec_ret() throws Exception {
        runBenchmark("invokevirtual_spec_ret");
    }

    @Test
    public void test_invokevirtual_gen_param() throws Exception {
        runBenchmark("invokevirtual_gen_param");
    }

//    @Test
//    public void test_invokevirtual_invokestatic() throws Exception {
//        runBenchmark("invokevirtual_invokestatic");
//    }


    @Test
    public void test_invokevirtual_invokeinterface() throws Exception {
        runBenchmark("invokevirtual_invokeinterface");
    }

    @Test
    public void test_invokeinterface_invokevirtual() throws Exception {
        runBenchmark("invokeinterface_invokevirtual");
    }

    @Test
    public void test_invokeinterface_box_param() throws Exception {
        runBenchmark("invokeinterface_box_param");
    }

    @Test
    public void test_invokeinterface_unbox_param() throws Exception {
        runBenchmark("invokeinterface_unbox_param");
    }

    @Test
    public void test_invokeinterface_box_ret() throws Exception {
        runBenchmark("invokeinterface_box_ret");
    }

    @Test
    public void test_invokeinterface_unbox_ret() throws Exception {
        runBenchmark("invokeinterface_unbox_ret");
    }

    @Test
    public void test_invokeinterface_widen_param() throws Exception {
        runBenchmark("invokeinterface_widen_param");
    }

    @Test
    public void test_invokeinterface_narrow_ret() throws Exception {
        runBenchmark("invokeinterface_narrow_ret");
    }

    @Test
    public void test_invokeinterface_unbox_widen_param() throws Exception {
        runBenchmark("invokeinterface_unbox_widen_param");
    }

    @Test
    public void test_invokeinterface_box_narrow_ret() throws Exception {
        runBenchmark("invokeinterface_box_narrow_ret");
    }

    @Test
    public void test_invokespecial_new_gen_param() throws Exception {
        runBenchmark("invokespecial_new_gen_param");
    }

    @Test
    public void test_invokespecial_new_box_param() throws Exception {
        runBenchmark("invokespecial_new_box_param");
    }

    @Test
    public void test_invokespecial_new_widen_param() throws Exception {
        runBenchmark("invokespecial_new_widen_param");
    }

    @Test
    public void test_invokespecial_new_unbox_widen_param() throws Exception {
        runBenchmark("invokespecial_new_unbox_widen_param");
    }


    @Test
    public void test_invokespecial_super_gen_param() throws Exception {
        runBenchmark("invokespecial_super_gen_param");
    }

    @Test
    public void test_invokespecial_super_spec_ret() throws Exception {
        runBenchmark("invokespecial_super_spec_ret");
    }

    @Test
    public void test_invokespecial_super_box_ret() throws Exception {
        runBenchmark("invokespecial_super_box_ret");
    }

    @Test
    public void test_invokespecial_super_unbox_ret() throws Exception {
        runBenchmark("invokespecial_super_unbox_ret");
    }

    @Test
    public void test_invokespecial_super_box_param() throws Exception {
        runBenchmark("invokespecial_super_box_param");
    }

    @Test
    public void test_invokespecial_super_unbox_param() throws Exception {
        runBenchmark("invokespecial_super_unbox_param");
    }
    */
}



