package lib;

import java.util.Collection;

public interface Foo {

    Foo INSTANCE = new Foo() {
        @Override
        public Collection getColl() {
            return new java.util.ArrayList();
        }
    };

    java.util.Collection getColl();

};