package lib;

import java.io.Serializable;

public interface Foo {

    Foo INSTANCE = new Foo() {
        @Override
        public void setValue(Serializable v) {

        }
    };

    void setValue(Serializable v);
}