package lib;

public interface Foo {

    interface ApiValue {
        void setValue(int v);
    }

    class ValueImpl implements ApiValue {
        @Override
        public void setValue(int v) {

        }
    }

}