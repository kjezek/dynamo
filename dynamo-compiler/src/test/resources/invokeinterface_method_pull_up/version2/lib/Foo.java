package lib;

public interface Foo {

    interface SuperApiValue {
        void setValue(int v);
    }

    class SuperValueImpl implements SuperApiValue {
        @Override
        public void setValue(int v) {

        }
    }

    interface ApiValue extends SuperApiValue {
    }


    class ValueImpl extends SuperValueImpl implements ApiValue {
    }

}




