package lib;

public interface Foo {

    Foo INSTANCE = new Foo() {
        @Override
        public Integer getValue() {
            return new Integer(10);
        }
    };

    Integer getValue();
};