package lib;

public interface Foo {

    Foo INSTANCE = new Foo() {
        @Override
        public int getValue() {
            return 10;
        }
    };

    int getValue();
};