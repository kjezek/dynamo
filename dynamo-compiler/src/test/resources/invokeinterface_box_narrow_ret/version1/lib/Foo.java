package lib;

public interface Foo {

    Foo INSTANCE = new Foo() {
        @Override
        public long getValue() {
            return 10l;
        }
    };

    long getValue();
};