package lib;

public interface Foo {

    Foo INSTANCE = new Foo() {
        @Override
        public Integer getValue() {
            return 10;
        }
    };

    Integer getValue();
};