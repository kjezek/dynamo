package lib;

public interface Foo {

    Foo INSTANCE = new Foo() {
        @Override
        public short getValue() {
            return 10;
        }
    };

    short getValue();
};