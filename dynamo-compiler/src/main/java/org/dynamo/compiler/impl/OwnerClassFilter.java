/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.compiler.impl;

import org.dynamo.enhancer.filters.AbstractTargetFilter;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * This method filter includes methods that are not
 * owned by the set of input classes.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public class OwnerClassFilter extends AbstractTargetFilter {

    /**
     * Owner that will not be included.
     */
    private Set<String> excludedOwners = new HashSet<>();

    /**
     * Init.
     *
     * @param excludedOwners classes representing owners that will not be included.
     */
    public OwnerClassFilter(final Collection<String> excludedOwners) {

        for (String owner : excludedOwners) {
            this.excludedOwners.add(owner.replace('.', '/'));
        }
    }

    @Override
    public boolean accepts(
            final String className,
            final String methodName,
            final String descriptor) {

        return !excludedOwners.contains(className);
    }

}
