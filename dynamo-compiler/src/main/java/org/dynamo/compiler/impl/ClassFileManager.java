/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.compiler.impl;

import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaFileObject;
import javax.tools.JavaFileObject.Kind;
import javax.tools.StandardJavaFileManager;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * File manager that will store compiled classes in memory.
 * see also http://www.javablogging.com/dynamic-in-memory-compilation/
 * @author jens dietrich
 */
public class ClassFileManager extends ForwardingJavaFileManager<StandardJavaFileManager> {

	private Map<String, ByteCodeFileObject> clazzes = new HashMap<>();

	public ClassFileManager(StandardJavaFileManager standardManager) {
		super(standardManager);
	}

	@Override
	public JavaFileObject getJavaFileForOutput(Location location, String className, Kind kind, FileObject sibling) throws IOException {
		if (!clazzes.containsKey(className)) {
			clazzes.put(className, new ByteCodeFileObject(className, kind));
		}

		return clazzes.get(className);
	}

	public Map<String, byte[]> getByteCodes() {
		Map<String, byte[]> byteCodes = new HashMap<String, byte[]>();
		for(String clazz : clazzes.keySet()) {
			byteCodes.put(clazz, clazzes.get(clazz).getBytes());
		}
		return byteCodes;
	}
}
