/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.compiler;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.dynamo.compiler.impl.ClassFileManager;
import org.dynamo.compiler.impl.OwnerClassFilter;
import org.dynamo.enhancer.filters.AndMethodFilter;
import org.dynamo.enhancer.filters.DefaultMethodFilter;
import org.dynamo.enhancer.filters.MethodFilter;
import org.dynamo.enhancer.filters.MethodFilterParser;
import org.dynamo.enhancer.transformer.ClassInvocationTransformer;
import org.dynamo.enhancer.transformer.ClassTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.dynamo.enhancer.utils.JreLoader.getJavaHome;
import static org.dynamo.enhancer.utils.JreLoader.getJreJars;

/**
 * Compiler executable.
 *
 * @author jens dietrich
 */
public final class Compiler {

    /**
     * Logger.
     */
    private static final Logger LOGGER_DYNAMO = LoggerFactory.getLogger("dynamo-compiler");

    /**
     * Logger.
     */
    private static final Logger LOGGER_JAVAC = LoggerFactory.getLogger("dynamo-compiler/javac");

    /**
     * CLI parameter.
     */
    public static final String SOURCE_PATH = "sourcepath";

    /**
     * CLI parameter.
     */
    public static final String HELP = "help";

    /**
     * CLI parameter.
     */
    public static final String DESTINATION = "d";

    /**
     * CLI parameter.
     */
    public static final String CLASSPATH = "cp";

    /**
     * CLI parameter.
     */
    public static final String ENCODING = "encoding";

    /**
     * CLI parameter.
     */
    public static final String FILTER = "filter";

    /**
     * CLI parameter.
     */
    public static final String CLASSIC = "classic";

    /** Factory. */
    private static ClassTransformer transformer = new ClassInvocationTransformer();

    /**
     * No instance needed.
     */
    private Compiler() {
        super();
    }

    /**
     * Compile!
     *
     * @param args compiler arguments.
     * @throws Exception error
     */
    public static void main(final String[] args) throws Exception {

        // check prereqs
        if (!isJava7OrBetter()) {
            LOGGER_DYNAMO.warn("Java 7 or better is required to use the dynamo compiler, the current version is "
                    + System.getProperty("java.specification.version"));

            System.exit(1);
        }

        // step 1 - prepare, parse parameters
        Options options = new Options()
            .addOption(SOURCE_PATH, true, "Specify where to find input source files (required)")
            .addOption(DESTINATION, true, "Specify where to place generated class files (required)")
            .addOption(HELP, false, "print instructions")
            .addOption(CLASSPATH, true, "Specify where to find user class files and annotation processors (required)")
            .addOption(ENCODING, true, "Specify character encoding used by source files")
            .addOption(FILTER, true, "Specify which methods to enhance.")
            .addOption(CLASSIC, false, "Classic compilation without enhancement - same as javac.");

        options.getOption(SOURCE_PATH).setRequired(true);
        options.getOption(DESTINATION).setRequired(true);

        final CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);

        boolean printInstructions = false;
        if (cmd.hasOption(HELP)) {
            printInstructions = true;
        } else {
            for (Option option : options.getOptions()) {
                if (option.isRequired() && !cmd.hasOption(option.getOpt())) {
                    printInstructions = true;
                    break;
                }
            }
        }

        boolean classicMode = cmd.hasOption(CLASSIC);

        if (printInstructions) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("dynamo-compiler <arguments>", options);
            System.exit(1);
        }

        String sourcePath = cmd.getOptionValue(SOURCE_PATH);
        String destination = cmd.getOptionValue(DESTINATION);

        // Step 2 - compile classes with standard Java compiler in memory

        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        //final ClassFileManager fileManager = new ClassFileManager(compiler.getStandardFileManager(null, null, null));
        Collection<File> sources = FileUtils.listFiles(
                new File(sourcePath),
                new SuffixFileFilter(".java"),
                TrueFileFilter.INSTANCE);

        String classpath = cmd.getOptionValue(CLASSPATH);

        LOGGER_DYNAMO.info("Compiling " + sources.size() + " with standard Java compiler");
        LOGGER_DYNAMO.debug("Classpath is " + (classpath == null ? "<empty>" : classpath));

        // build parameters for JVM - pass through
        List<String> optionList = new ArrayList<>();

        String encoding = cmd.getOptionValue(ENCODING);

        DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();

        StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, getCharset(encoding));
        StandardJavaFileManager fileManager1 = compiler.getStandardFileManager(null, null, null);
        final ClassFileManager fileManager2 = new ClassFileManager(fileManager1);
        setupClasspath(fileManager1, classpath);
        Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjectsFromFiles(sources);
        Boolean compilationResult = compiler.getTask(null, fileManager2, diagnostics, optionList, null, compilationUnits).call();

        // print compiler output
        for (Diagnostic<? extends JavaFileObject> dia : diagnostics.getDiagnostics()) {
            switch (dia.getKind()) {
                case ERROR:
                    LOGGER_JAVAC.error("" + dia);
                    break;
                case WARNING:
                    LOGGER_JAVAC.warn("" + dia);
                    break;
                default:
                    LOGGER_JAVAC.info("" + dia);
                    break;
            }

        }

        LOGGER_JAVAC.debug("Compilation result " + compilationResult);

        Map<String, byte[]> compiledClasses = fileManager2.getByteCodes();

        // Step 3 - swap invoke instructions

        Map<String, byte[]> enhancedClasses = fileManager2.getByteCodes();
        if (!classicMode) {
            LOGGER_DYNAMO.info("Enhancing " + compiledClasses.size() + " compiled classes ");

            MethodFilter userFilter;
            String filter = cmd.getOptionValue("filter");
            if (filter != null) {
                userFilter = MethodFilterParser.parse(filter);
            } else {
                userFilter = DefaultMethodFilter.INSTANCE;
            }

            MethodFilter compoundFilter = new AndMethodFilter(
                    new OwnerClassFilter(compiledClasses.keySet()), userFilter
            );

            for (String className : compiledClasses.keySet()) {
                byte[] bytecode = compiledClasses.get(className);
                byte[] enhancedBytecode = transformer.transform(className, bytecode, compoundFilter);
                enhancedClasses.put(className, enhancedBytecode);
            }
        } else {
            LOGGER_DYNAMO.info("Skip enhancement - compiler started with flag " + CLASSIC);
        }

        // Step 4 - export compiled and modified classes
        File dest = new File(destination);
        if (dest.exists() && !dest.isDirectory()) {
            LOGGER_DYNAMO.error("Destination folder " + dest.getAbsolutePath() + " exists but is not a folder");
            System.exit(1);
        }
        if (!dest.exists()) {
            LOGGER_DYNAMO.info("Destination folder " + dest.getAbsolutePath()
                    + " does not exist, will try to create it");
            FileUtils.forceMkdir(dest);
        }

        assert dest.isDirectory();
        assert dest.exists();

        LOGGER_DYNAMO.info("Exporting enhanced classes to  " + dest.getAbsolutePath());

        // store compiled classes
        for (Map.Entry<String, byte[]> entry : enhancedClasses.entrySet()) {
            String relClassFileName = entry.getKey().replace('.', '/') + ".class";
            File classFile = new File(dest, relClassFileName);
            FileUtils.writeByteArrayToFile(classFile, entry.getValue());
        }

        // store original non-java files
        FileUtils.copyDirectory(new File(sourcePath), dest, new FileFilter() {
            @Override
            public boolean accept(final File pathname) {
                return !pathname.getName().endsWith(".java");
            }
        });

        LOGGER_DYNAMO.info("Compilation done");
//        System.exit(0);

    }

    /**
     * Convert encoding to charset.
     *
     * @param encoding encoding
     * @return charset object
     */
    private static Charset getCharset(final String encoding) {
        Charset charset = null;
        if (encoding != null) {
            charset = Charset.forName(encoding);
        }

        return charset;
    }

    /**
     * Setup classpath for the bootstrap and user classpath.
     * <p/>
     * Bootstrap classpath must be set, otherwise private Oracle/Sun packages cannot be compiled against.
     * Well, it is not a good practise to compile against private packages, but such programs exist
     * and we want to support all.
     *
     * @param fileManager1 current file manager.
     * @param classpath    user classpath
     * @throws IOException IO exception
     */
    private static void setupClasspath(
            final StandardJavaFileManager fileManager1,
            final String classpath) throws IOException {

        // set bootstrap classpath
        File[] jreFiles = getJreJars(getJavaHome());
        fileManager1.setLocation(StandardLocation.PLATFORM_CLASS_PATH, Arrays.asList(jreFiles));

        // set user classpath
        List<File> paths = new ArrayList<>();
        if (classpath != null) {
            for (String item : classpath.split(File.pathSeparator)) {
                paths.add(new File(item));
            }
        }

        fileManager1.setLocation(StandardLocation.CLASS_PATH, paths);
    }


    /**
     * Check Java version by loading one of the classes that has been added in Java 1.7.
     *
     * @return true if the Java version constrains passes.
     */
    private static boolean isJava7OrBetter() {
        try {
            Class.forName("java.lang.invoke.MethodHandles");
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }
}
