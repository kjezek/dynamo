#!/bin/sh

FILES=$(find src/test/resources/ -name "Main.java")


for first in $FILES
do
    for second in $FILES
    do

        if  diff -w $first $second >/dev/null 2>&1 && [ "$first" != "$second" ]
         then
                 IFS='/' read -r -a first_array <<< "$first"
                 IFS='/' read -r -a second_array <<< "$second"
                 first_part="${first_array[3]}"
                 second_part="${second_array[3]}"

         echo "\""$first_part"\", // " $second_part

         #delete element from array
         FILES=("${FILES[@]/$first}")
        fi
    done
done