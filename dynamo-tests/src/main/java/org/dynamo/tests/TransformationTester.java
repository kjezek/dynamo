/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.tests;

import org.junit.Assert;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import java.util.HashMap;
import java.util.Map;

/**
 * This class tests transformation.
 *
 * @author Kamil Jezek [kjezek@kiv.zcu.cz]
 */
public final class TransformationTester {

    /** Private. */
    private TransformationTester() {
    }

    /**
     * Assert that the input byte code has expected instructions.
     * @param bytecode bytes with transformed bytecode.
     * @param ruleUpdater an object accessing the rules to check for.
     */
    public static void assertTransformation(final Rules ruleUpdater, final byte[] bytecode) {
        /** Rules. */
        final Map<String, Integer> rules = new HashMap<>();

        ruleUpdater.rules(new RulesUpdater() {
            @Override
            public void addMethod(
                    final String owner,
                    final String name,
                    final Integer opcode) {

                rules.put(owner + "#" + name, opcode);
            }
        });

        final ClassReader classReader = new ClassReader(bytecode);
        classReader.accept(new ClassVisitor(Opcodes.ASM5, null) {
            @Override
            public MethodVisitor visitMethod(
                    final int access,
                    final String name,
                    final String desc,
                    final String signature,
                    final String[] exceptions) {

                return new MethodVisitor(Opcodes.ASM5, null) {
                    @Override
                    public void visitMethodInsn(
                            final int opcode,
                            final String owner,
                            final String name,
                            final String desc,
                            final boolean itf) {

                        Integer expectedOpcode = rules.get(owner.replace('/', '.') + "#" + name);

                        if (expectedOpcode != null) {
                            Assert.assertEquals("Wrong opcode for "
                                    + owner + "#" + name,
                                    expectedOpcode.intValue(), opcode);
                        }

                        super.visitMethodInsn(opcode, owner, name, desc, itf);
                    }
                };
            }
        }, 0);

    }

    /**
     * A set of rules.
     */
    public interface RulesUpdater {

        /**
         * Add a rule. The rule says which opcode should be set
         * for a method given by its owner and name.
         * @param owner method owner
         * @param name method name
         * @param opcode expected opcode
         */
        void addMethod(String owner, String name, Integer opcode);
    }

    /**
     * Interface for accessing the rules object.
     */
    public interface Rules {

        /**
         * Method accessing the rules.
         * @param updater rules.
         */
        void rules(RulesUpdater updater);
    }
}
