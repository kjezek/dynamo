/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Kamil Jezek, Jens Dietrich
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package org.dynamo.tests;

/**
 * This instance allows for accessing methods from class loader.
 */
public class PublicClassLoader extends ClassLoader {

    /**
     * Init.
     * @param classLoader parent loader
     */
    public PublicClassLoader(final ClassLoader classLoader) {
        super(classLoader);
    }

    /**
     * Define a new class from the byte-code.
     * @param name class name.
     * @param b byte-code of the class.
     * @return a new class.
     */
    public final Class<?> defineClass(final String name, final byte[] b) {
        return super.defineClass(name, b, 0, b.length, null);
    }
}
