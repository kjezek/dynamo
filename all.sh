#!/bin/bash

function user_input {
         printf  "\n\n"
         printf '\e[1;92m%-6s\e[m \n\n'  " Please pay attention at the following lines ..  when you are ready to continue press ENTER "
         printf " $1 \n\n"
#         printf  '\e[1;92m%-6s\e[m \n\n'  "  ... Please check the above output and press ENTER to continue "
         read ;
}


# We skip this steps to allow for offline work
# Environment will be set in VM

#    sudo apt-get install git
#    sudo apt-get install openjdk-7-jdk
#    sudo apt-get install maven
#    sudo apt-get install ant

    cd ~/
#    git clone git@bitbucket.org:kjezek/dynamo.git
    cd dynamo
#    git checkout tags/dynamo-1.0
#    git checkout master

user_input "Next steps will \n\t 1. Build Dynamo from sources \n\t 2. Run JUnit tests";

    mvn clean install

user_input "The output above lines should contain BUILD SUCCESS";

    ./deploy.sh

    cd ~/dynamo/case-study/jasper-reports-recompilable-problem/

user_input "Case study from section '7.1 Solving Compatibility Issues between Jasperreports and Jfreechart ' \n\t \
The next steps will show a report generated with \
\n\t\t 1. JasperReports is compiled and executed with JFreeChart 1.0.0  - no error, a report is printed \
\n\t\t 2. JasperReports is compiled with javac against JFreeChart 1.0.0 and executed with JFreeChart 1.0.12 - this will FAIL as new library cannnot be linked \
\n\t\t 3. JasperReports is compiled with dynamo against JFreeChart 1.0.0 and executed with JFreeChart 1.0.12 - no error, a report is printed";

user_input "When the graphical window is opened, CLOSE it to continue."

    ant run.original

user_input "Step 1: You should now see a window with a plain report."

    ant run.update

user_input "Step 2: Failure is expected, you should see NoSuchMethodError as the library cannot be linked"

    ant run.dynamic

user_input "Step 3: You should see a window with a plain report. \
\n\t the report is generated with the new DYNAMICALLY linked JFreeChart 1.0.12 library."

    cd ~/dynamo/case-study/covariant-returntypes-and-bridgemethods/

user_input "Case study from section: '7.2 Avoiding the Hazards of Covariant Return Types and Bridge Methods'
\t You will see: \
\n\t\t 1. Stack Overflow -- expected failure \
\n\t\t 2. Correct output 'Hello' when Dynamo is used"

    ant

user_input "You should see the following:  \
\n\t 1. Stack Overflow -- expected failure \
\n\t 2. Correct output 'Hello' when Dynamo is used"

    cd ~/dynamo/dynamo-compiler/

user_input "Section 6: 'Benchmarks' \
Next steps will invoke all benchmarks -- each step takes several minutes"

    ./benchmark.sh

user_input "The lines above show the results of running the benchmarks from '6.1 Compiler Benchmarks'"

    cd ~/dynamo/dynamo-rt/

    ./benchmark.sh

user_input "The lines above show the results of running the benchmarks from '6.2 Runtime Benchmarks'"

    cd ~/dynamo/case-study/jasper-reports-recompilable-problem

    ant benchmark.compile

user_input "The lines above show the results of running the compilation benchmarks from case study 1 (section 7.1)"

    ant benchmark.run.original

user_input "The lines above show the results of running the runtime benchmarks from case study 1 compiled with the classic Java compiler (sect 7.1)"

    ant benchmark.run.dynamic

user_input "The lines above show the results of running the runtime benchmarks from case study 1 compiled with dynamo (sect 7.1)"

user_input "END - we thank reviewer for his or her patience"
